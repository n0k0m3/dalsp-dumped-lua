return {
    [20302] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500069,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200318,
        head = "icon/role/mood/110/4.png",
        id = 20302,
        datingId = 0,
        weight = 100,
        des = 13200294,
    },
    [20304] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500069,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200320,
        head = "icon/role/mood/112/4.png",
        id = 20304,
        datingId = 0,
        weight = 100,
        des = 13200296,
    },
    [20306] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500069,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200322,
        head = "icon/role/mood/101/4.png",
        id = 20306,
        datingId = 0,
        weight = 100,
        des = 13200298,
    },
    [30601] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580155,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580156,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580157,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311538,
        head = "icon/role/mood/103/4.png",
        id = 30601,
        datingId = 0,
        weight = 100,
        des = 13311562,
    },
    [30603] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580155,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580156,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580157,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311540,
        head = "icon/role/mood/112/4.png",
        id = 30603,
        datingId = 0,
        weight = 100,
        des = 13311564,
    },
    [30101] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500081,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311517,
        head = "icon/role/mood/101/4.png",
        id = 30101,
        datingId = 0,
        weight = 100,
        des = 13311541,
    },
    [30103] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500081,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311519,
        head = "icon/role/mood/103/4.png",
        id = 30103,
        datingId = 0,
        weight = 100,
        des = 13311543,
    },
    [20201] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500068,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200314,
        head = "icon/role/mood/112/4.png",
        id = 20201,
        datingId = 0,
        weight = 100,
        des = 13200290,
    },
    [20203] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500068,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200316,
        head = "icon/role/mood/105/4.png",
        id = 20203,
        datingId = 0,
        weight = 100,
        des = 13200292,
    },
    [30502] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580155,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580156,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580157,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311536,
        head = "icon/role/mood/104/4.png",
        id = 30502,
        datingId = 0,
        weight = 100,
        des = 13311560,
    },
    [20602] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580098,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580099,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580100,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200330,
        head = "icon/role/mood/104/4.png",
        id = 20602,
        datingId = 0,
        weight = 100,
        des = 13200306,
    },
    [20604] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580098,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580099,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580100,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200332,
        head = "icon/role/mood/103/4.png",
        id = 20604,
        datingId = 0,
        weight = 100,
        des = 13200308,
    },
    [20606] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580098,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580099,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580100,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200334,
        head = "icon/role/mood/112/4.png",
        id = 20606,
        datingId = 0,
        weight = 100,
        des = 13200310,
    },
    [20102] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500068,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200312,
        head = "icon/role/mood/102/4.png",
        id = 20102,
        datingId = 0,
        weight = 100,
        des = 13200288,
    },
    [30401] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311529,
        head = "icon/role/mood/101/4.png",
        id = 30401,
        datingId = 0,
        weight = 100,
        des = 13311553,
    },
    [30403] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311531,
        head = "icon/role/mood/104/4.png",
        id = 30403,
        datingId = 0,
        weight = 100,
        des = 13311555,
    },
    [30405] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311533,
        head = "icon/role/mood/102/4.png",
        id = 30405,
        datingId = 0,
        weight = 100,
        des = 13311557,
    },
    [20501] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200326,
        head = "icon/role/mood/11303/4.png",
        id = 20501,
        datingId = 0,
        weight = 100,
        des = 13200302,
    },
    [20503] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200328,
        head = "icon/role/mood/103/4.png",
        id = 20503,
        datingId = 0,
        weight = 100,
        des = 13200304,
    },
    [30302] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500082,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311524,
        head = "icon/role/mood/110/4.png",
        id = 30302,
        datingId = 0,
        weight = 100,
        des = 13311548,
    },
    [30304] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500082,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311526,
        head = "icon/role/mood/112/4.png",
        id = 30304,
        datingId = 0,
        weight = 100,
        des = 13311550,
    },
    [30306] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500082,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311528,
        head = "icon/role/mood/101/4.png",
        id = 30306,
        datingId = 0,
        weight = 100,
        des = 13311552,
    },
    [20402] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200324,
        head = "icon/role/mood/105/4.png",
        id = 20402,
        datingId = 0,
        weight = 100,
        des = 13200300,
    },
    [10012] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500046,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200019,
        head = "icon/role/mood/101/4.png",
        id = 10012,
        datingId = 0,
        weight = 100,
        des = 13200051,
    },
    [10013] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200020,
        head = "icon/role/mood/101/4.png",
        id = 10013,
        datingId = 0,
        weight = 100,
        des = 13200052,
    },
    [10014] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500051,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 500052,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500053,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200021,
        head = "icon/role/mood/101/4.png",
        id = 10014,
        datingId = 0,
        weight = 100,
        des = 13200053,
    },
    [10021] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500045,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200022,
        head = "icon/role/mood/102/4.png",
        id = 10021,
        datingId = 0,
        weight = 100,
        des = 13200054,
    },
    [10022] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500046,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200023,
        head = "icon/role/mood/102/4.png",
        id = 10022,
        datingId = 0,
        weight = 100,
        des = 13200055,
    },
    [10023] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200024,
        head = "icon/role/mood/102/4.png",
        id = 10023,
        datingId = 0,
        weight = 100,
        des = 13200056,
    },
    [10024] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500051,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 500052,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500053,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200025,
        head = "icon/role/mood/102/4.png",
        id = 10024,
        datingId = 0,
        weight = 100,
        des = 13200057,
    },
    [20303] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500069,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200319,
        head = "icon/role/mood/11303/4.png",
        id = 20303,
        datingId = 0,
        weight = 100,
        des = 13200295,
    },
    [20305] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500069,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200321,
        head = "icon/role/mood/105/4.png",
        id = 20305,
        datingId = 0,
        weight = 100,
        des = 13200297,
    },
    [10031] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500045,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200026,
        head = "icon/role/mood/103/4.png",
        id = 10031,
        datingId = 0,
        weight = 100,
        des = 13200058,
    },
    [10032] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500046,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200027,
        head = "icon/role/mood/103/4.png",
        id = 10032,
        datingId = 0,
        weight = 100,
        des = 13200059,
    },
    [10033] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200028,
        head = "icon/role/mood/103/4.png",
        id = 10033,
        datingId = 0,
        weight = 100,
        des = 13200060,
    },
    [10034] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500051,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 500052,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500053,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200029,
        head = "icon/role/mood/103/4.png",
        id = 10034,
        datingId = 0,
        weight = 100,
        des = 13200061,
    },
    [30102] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500081,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311518,
        head = "icon/role/mood/102/4.png",
        id = 30102,
        datingId = 0,
        weight = 100,
        des = 13311542,
    },
    [20202] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500068,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200315,
        head = "icon/role/mood/110/4.png",
        id = 20202,
        datingId = 0,
        weight = 100,
        des = 13200291,
    },
    [10041] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500045,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200030,
        head = "icon/role/mood/104/4.png",
        id = 10041,
        datingId = 0,
        weight = 100,
        des = 13200062,
    },
    [10042] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500046,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200031,
        head = "icon/role/mood/104/4.png",
        id = 10042,
        datingId = 0,
        weight = 100,
        des = 13200063,
    },
    [10043] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200032,
        head = "icon/role/mood/104/4.png",
        id = 10043,
        datingId = 0,
        weight = 100,
        des = 13200064,
    },
    [10044] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500051,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 500052,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500053,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200033,
        head = "icon/role/mood/104/4.png",
        id = 10044,
        datingId = 0,
        weight = 100,
        des = 13200065,
    },
    [30503] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580155,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580156,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580157,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311537,
        head = "icon/role/mood/102/4.png",
        id = 30503,
        datingId = 0,
        weight = 100,
        des = 13311561,
    },
    [20601] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580098,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580099,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580100,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200329,
        head = "icon/role/mood/110/4.png",
        id = 20601,
        datingId = 0,
        weight = 100,
        des = 13200305,
    },
    [20603] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580098,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580099,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580100,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200331,
        head = "icon/role/mood/102/4.png",
        id = 20603,
        datingId = 0,
        weight = 100,
        des = 13200307,
    },
    [20605] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580098,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580099,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580100,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200333,
        head = "icon/role/mood/11303/4.png",
        id = 20605,
        datingId = 0,
        weight = 100,
        des = 13200309,
    },
    [20101] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500068,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200311,
        head = "icon/role/mood/101/4.png",
        id = 20101,
        datingId = 0,
        weight = 100,
        des = 13200287,
    },
    [20103] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500068,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200313,
        head = "icon/role/mood/103/4.png",
        id = 20103,
        datingId = 0,
        weight = 100,
        des = 13200289,
    },
    [10053] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200036,
        head = "icon/role/mood/105/4.png",
        id = 10053,
        datingId = 0,
        weight = 100,
        des = 13200068,
    },
    [10054] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500051,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 500052,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500053,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200037,
        head = "icon/role/mood/105/4.png",
        id = 10054,
        datingId = 0,
        weight = 100,
        des = 13200069,
    },
    [30402] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311530,
        head = "icon/role/mood/105/4.png",
        id = 30402,
        datingId = 0,
        weight = 100,
        des = 13311554,
    },
    [30404] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311532,
        head = "icon/role/mood/11303/4.png",
        id = 30404,
        datingId = 0,
        weight = 100,
        des = 13311556,
    },
    [30406] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311534,
        head = "icon/role/mood/103/4.png",
        id = 30406,
        datingId = 0,
        weight = 100,
        des = 13311558,
    },
    [20502] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200327,
        head = "icon/role/mood/102/4.png",
        id = 20502,
        datingId = 0,
        weight = 100,
        des = 13200303,
    },
    [10062] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500046,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200039,
        head = "icon/role/mood/112/4.png",
        id = 10062,
        datingId = 0,
        weight = 100,
        des = 13200071,
    },
    [10063] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200040,
        head = "icon/role/mood/112/4.png",
        id = 10063,
        datingId = 0,
        weight = 100,
        des = 13200072,
    },
    [10064] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500051,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 500052,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500053,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200041,
        head = "icon/role/mood/112/4.png",
        id = 10064,
        datingId = 0,
        weight = 100,
        des = 13200073,
    },
    [30602] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580155,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580156,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580157,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311539,
        head = "icon/role/mood/11303/4.png",
        id = 30602,
        datingId = 0,
        weight = 100,
        des = 13311563,
    },
    [30501] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 580155,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 580156,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 580157,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311535,
        head = "icon/role/mood/110/4.png",
        id = 30501,
        datingId = 0,
        weight = 100,
        des = 13311559,
    },
    [30305] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500082,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311527,
        head = "icon/role/mood/105/4.png",
        id = 30305,
        datingId = 0,
        weight = 100,
        des = 13311551,
    },
    [30303] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500082,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311525,
        head = "icon/role/mood/11303/4.png",
        id = 30303,
        datingId = 0,
        weight = 100,
        des = 13311549,
    },
    [20301] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500069,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200317,
        head = "icon/role/mood/104/4.png",
        id = 20301,
        datingId = 0,
        weight = 100,
        des = 13200293,
    },
    [10061] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500045,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200038,
        head = "icon/role/mood/112/4.png",
        id = 10061,
        datingId = 0,
        weight = 100,
        des = 13200070,
    },
    [10071] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500045,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200042,
        head = "icon/role/mood/11303/4.png",
        id = 10071,
        datingId = 0,
        weight = 100,
        des = 13200074,
    },
    [10072] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500046,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200043,
        head = "icon/role/mood/11303/4.png",
        id = 10072,
        datingId = 0,
        weight = 100,
        des = 13200075,
    },
    [10073] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200044,
        head = "icon/role/mood/11303/4.png",
        id = 10073,
        datingId = 0,
        weight = 100,
        des = 13200076,
    },
    [10074] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500051,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 500052,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500053,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200045,
        head = "icon/role/mood/11303/4.png",
        id = 10074,
        datingId = 0,
        weight = 100,
        des = 13200077,
    },
    [20403] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200325,
        head = "icon/role/mood/104/4.png",
        id = 20403,
        datingId = 0,
        weight = 100,
        des = 13200301,
    },
    [30301] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500082,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311523,
        head = "icon/role/mood/104/4.png",
        id = 30301,
        datingId = 0,
        weight = 100,
        des = 13311547,
    },
    [30203] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500081,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311522,
        head = "icon/role/mood/105/4.png",
        id = 30203,
        datingId = 0,
        weight = 100,
        des = 13311546,
    },
    [20401] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200323,
        head = "icon/role/mood/101/4.png",
        id = 20401,
        datingId = 0,
        weight = 100,
        des = 13200299,
    },
    [30201] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500081,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311520,
        head = "icon/role/mood/112/4.png",
        id = 30201,
        datingId = 0,
        weight = 100,
        des = 13311544,
    },
    [10011] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500045,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200018,
        head = "icon/role/mood/101/4.png",
        id = 10011,
        datingId = 0,
        weight = 100,
        des = 13200050,
    },
    [10081] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500045,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200046,
        head = "icon/role/mood/110/4.png",
        id = 10081,
        datingId = 0,
        weight = 100,
        des = 13200078,
    },
    [10082] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500046,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200047,
        head = "icon/role/mood/110/4.png",
        id = 10082,
        datingId = 0,
        weight = 100,
        des = 13200079,
    },
    [10083] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500047,
                        max = 1,
                        weight = 3000,
                    },
                    [2] = {
                        min = 1,
                        id = 500048,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500049,
                        max = 1,
                        weight = 2500,
                    },
                    [4] = {
                        min = 1,
                        id = 500050,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200048,
        head = "icon/role/mood/110/4.png",
        id = 10083,
        datingId = 0,
        weight = 100,
        des = 13200080,
    },
    [10084] = {
        reward = {
            roll = {
                maxWeight = 10000,
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 500051,
                        max = 1,
                        weight = 4000,
                    },
                    [2] = {
                        min = 1,
                        id = 500052,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 500053,
                        max = 1,
                        weight = 4000,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200049,
        head = "icon/role/mood/110/4.png",
        id = 10084,
        datingId = 0,
        weight = 100,
        des = 13200081,
    },
    [30202] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500081,
                    },
                },
            },
        },
        isOpen = true,
        name = 13311521,
        head = "icon/role/mood/110/4.png",
        id = 30202,
        datingId = 0,
        weight = 100,
        des = 13311545,
    },
    [10051] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 10,
                        max = 10,
                        id = 500045,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200034,
        head = "icon/role/mood/105/4.png",
        id = 10051,
        datingId = 0,
        weight = 100,
        des = 13200066,
    },
    [10052] = {
        reward = {
            fix = {
                items = {
                    [1] = {
                        min = 2,
                        max = 2,
                        id = 500046,
                    },
                },
            },
        },
        isOpen = true,
        name = 13200035,
        head = "icon/role/mood/105/4.png",
        id = 10052,
        datingId = 0,
        weight = 100,
        des = 13200067,
    },
}