return {
    [2] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s2.png",
        id = 2,
        probability = 1250,
    },
    [3] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s3.png",
        id = 3,
        probability = 1250,
    },
    [4] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s4.png",
        id = 4,
        probability = 1250,
    },
    [5] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s5.png",
        id = 5,
        probability = 1250,
    },
    [6] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s6.png",
        id = 6,
        probability = 1250,
    },
    [7] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s7.png",
        id = 7,
        probability = 1250,
    },
    [8] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s8.png",
        id = 8,
        probability = 1250,
    },
    [9] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s9.png",
        id = 9,
        probability = 1250,
    },
    [10] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s10.png",
        id = 10,
        probability = 0,
    },
    [11] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s11.png",
        id = 11,
        probability = 0,
    },
    [12] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s12.png",
        id = 12,
        probability = 0,
    },
    [13] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s13.png",
        id = 13,
        probability = 1250,
    },
    [14] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s15.png",
        id = 14,
        probability = 0,
    },
    [15] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s57.png",
        id = 15,
        probability = 0,
    },
    [16] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s24.png",
        id = 16,
        probability = 0,
    },
    [17] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s25.png",
        id = 17,
        probability = 0,
    },
    [18] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s14.png",
        id = 18,
        probability = 1250,
    },
    [19] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s53.png",
        id = 19,
        probability = 0,
    },
    [20] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s54.png",
        id = 20,
        probability = 0,
    },
    [21] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s55.png",
        id = 21,
        probability = 0,
    },
    [22] = {
        descID = {
        },
        showType = 1,
        res = "ui/update/s56.png",
        id = 22,
        probability = 0,
    },
    [23] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s1.png",
        id = 23,
        probability = 1250,
    },
    [24] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s2.png",
        id = 24,
        probability = 1250,
    },
    [25] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s3.png",
        id = 25,
        probability = 1250,
    },
    [26] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s4.png",
        id = 26,
        probability = 1250,
    },
    [27] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s5.png",
        id = 27,
        probability = 1250,
    },
    [28] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s6.png",
        id = 28,
        probability = 1250,
    },
    [29] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s7.png",
        id = 29,
        probability = 1250,
    },
    [30] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s8.png",
        id = 30,
        probability = 1250,
    },
    [31] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s9.png",
        id = 31,
        probability = 1250,
    },
    [32] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s10.png",
        id = 32,
        probability = 0,
    },
    [33] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s11.png",
        id = 33,
        probability = 0,
    },
    [34] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s12.png",
        id = 34,
        probability = 0,
    },
    [35] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s13.png",
        id = 35,
        probability = 1250,
    },
    [36] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s15.png",
        id = 36,
        probability = 0,
    },
    [37] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s57.png",
        id = 37,
        probability = 0,
    },
    [38] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s24.png",
        id = 38,
        probability = 0,
    },
    [39] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s25.png",
        id = 39,
        probability = 0,
    },
    [40] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s14.png",
        id = 40,
        probability = 1250,
    },
    [41] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s53.png",
        id = 41,
        probability = 0,
    },
    [42] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s54.png",
        id = 42,
        probability = 0,
    },
    [43] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s55.png",
        id = 43,
        probability = 0,
    },
    [44] = {
        descID = {
            [1] = 28001,
            [2] = 28037,
        },
        showType = 2,
        res = "ui/update/s56.png",
        id = 44,
        probability = 0,
    },
    [45] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s1.png",
        id = 45,
        probability = 1250,
    },
    [46] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s2.png",
        id = 46,
        probability = 1250,
    },
    [47] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s3.png",
        id = 47,
        probability = 1250,
    },
    [48] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s4.png",
        id = 48,
        probability = 1250,
    },
    [49] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s5.png",
        id = 49,
        probability = 1250,
    },
    [50] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s6.png",
        id = 50,
        probability = 1250,
    },
    [51] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s7.png",
        id = 51,
        probability = 1250,
    },
    [52] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s8.png",
        id = 52,
        probability = 1250,
    },
    [53] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s9.png",
        id = 53,
        probability = 1250,
    },
    [54] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s10.png",
        id = 54,
        probability = 0,
    },
    [55] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s11.png",
        id = 55,
        probability = 0,
    },
    [56] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s12.png",
        id = 56,
        probability = 0,
    },
    [57] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s13.png",
        id = 57,
        probability = 1250,
    },
    [58] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s15.png",
        id = 58,
        probability = 0,
    },
    [59] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s57.png",
        id = 59,
        probability = 0,
    },
    [60] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s24.png",
        id = 60,
        probability = 0,
    },
    [61] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s25.png",
        id = 61,
        probability = 0,
    },
    [62] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s14.png",
        id = 62,
        probability = 1250,
    },
    [63] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s53.png",
        id = 63,
        probability = 0,
    },
    [64] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s54.png",
        id = 64,
        probability = 0,
    },
    [65] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s55.png",
        id = 65,
        probability = 0,
    },
    [66] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 3,
        res = "ui/update/s56.png",
        id = 66,
        probability = 0,
    },
    [67] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s1.png",
        id = 67,
        probability = 1250,
    },
    [68] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s2.png",
        id = 68,
        probability = 1250,
    },
    [69] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s3.png",
        id = 69,
        probability = 1250,
    },
    [70] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s4.png",
        id = 70,
        probability = 1250,
    },
    [71] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s5.png",
        id = 71,
        probability = 1250,
    },
    [72] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s6.png",
        id = 72,
        probability = 1250,
    },
    [73] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s7.png",
        id = 73,
        probability = 1250,
    },
    [74] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s8.png",
        id = 74,
        probability = 1250,
    },
    [75] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s9.png",
        id = 75,
        probability = 1250,
    },
    [76] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s10.png",
        id = 76,
        probability = 0,
    },
    [77] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s11.png",
        id = 77,
        probability = 0,
    },
    [78] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s12.png",
        id = 78,
        probability = 0,
    },
    [79] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s13.png",
        id = 79,
        probability = 1250,
    },
    [80] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s15.png",
        id = 80,
        probability = 0,
    },
    [81] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s57.png",
        id = 81,
        probability = 0,
    },
    [82] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s24.png",
        id = 82,
        probability = 0,
    },
    [83] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s25.png",
        id = 83,
        probability = 0,
    },
    [84] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s14.png",
        id = 84,
        probability = 1250,
    },
    [85] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s53.png",
        id = 85,
        probability = 0,
    },
    [86] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s54.png",
        id = 86,
        probability = 0,
    },
    [87] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s55.png",
        id = 87,
        probability = 0,
    },
    [88] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 31,
        res = "ui/update/s56.png",
        id = 88,
        probability = 0,
    },
    [89] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s1.png",
        id = 89,
        probability = 1250,
    },
    [90] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s2.png",
        id = 90,
        probability = 1250,
    },
    [91] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s3.png",
        id = 91,
        probability = 1250,
    },
    [92] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s4.png",
        id = 92,
        probability = 1250,
    },
    [93] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s5.png",
        id = 93,
        probability = 1250,
    },
    [94] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s6.png",
        id = 94,
        probability = 1250,
    },
    [95] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s7.png",
        id = 95,
        probability = 1250,
    },
    [96] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s8.png",
        id = 96,
        probability = 1250,
    },
    [97] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s9.png",
        id = 97,
        probability = 1250,
    },
    [98] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s10.png",
        id = 98,
        probability = 0,
    },
    [99] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s11.png",
        id = 99,
        probability = 0,
    },
    [100] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s12.png",
        id = 100,
        probability = 0,
    },
    [101] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s13.png",
        id = 101,
        probability = 1250,
    },
    [102] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s15.png",
        id = 102,
        probability = 0,
    },
    [103] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s57.png",
        id = 103,
        probability = 0,
    },
    [104] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s24.png",
        id = 104,
        probability = 0,
    },
    [105] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s25.png",
        id = 105,
        probability = 0,
    },
    [106] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s14.png",
        id = 106,
        probability = 1250,
    },
    [107] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s53.png",
        id = 107,
        probability = 0,
    },
    [108] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s54.png",
        id = 108,
        probability = 0,
    },
    [109] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s55.png",
        id = 109,
        probability = 0,
    },
    [110] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 32,
        res = "ui/update/s56.png",
        id = 110,
        probability = 0,
    },
    [111] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s1.png",
        id = 111,
        probability = 1250,
    },
    [112] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s2.png",
        id = 112,
        probability = 1250,
    },
    [113] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s3.png",
        id = 113,
        probability = 1250,
    },
    [114] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s4.png",
        id = 114,
        probability = 1250,
    },
    [115] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s5.png",
        id = 115,
        probability = 1250,
    },
    [116] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s6.png",
        id = 116,
        probability = 1250,
    },
    [117] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s7.png",
        id = 117,
        probability = 1250,
    },
    [118] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s8.png",
        id = 118,
        probability = 1250,
    },
    [119] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s9.png",
        id = 119,
        probability = 1250,
    },
    [120] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s10.png",
        id = 120,
        probability = 0,
    },
    [121] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s11.png",
        id = 121,
        probability = 0,
    },
    [122] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s12.png",
        id = 122,
        probability = 0,
    },
    [123] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s13.png",
        id = 123,
        probability = 1250,
    },
    [124] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s15.png",
        id = 124,
        probability = 0,
    },
    [125] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s57.png",
        id = 125,
        probability = 0,
    },
    [126] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s24.png",
        id = 126,
        probability = 0,
    },
    [127] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s25.png",
        id = 127,
        probability = 0,
    },
    [128] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s14.png",
        id = 128,
        probability = 1250,
    },
    [129] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s53.png",
        id = 129,
        probability = 0,
    },
    [130] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s54.png",
        id = 130,
        probability = 0,
    },
    [131] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s55.png",
        id = 131,
        probability = 0,
    },
    [132] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 33,
        res = "ui/update/s56.png",
        id = 132,
        probability = 0,
    },
    [133] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s1.png",
        id = 133,
        probability = 1250,
    },
    [134] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s2.png",
        id = 134,
        probability = 1250,
    },
    [135] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s3.png",
        id = 135,
        probability = 1250,
    },
    [136] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s4.png",
        id = 136,
        probability = 1250,
    },
    [137] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s5.png",
        id = 137,
        probability = 1250,
    },
    [138] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s6.png",
        id = 138,
        probability = 1250,
    },
    [139] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s7.png",
        id = 139,
        probability = 1250,
    },
    [140] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s8.png",
        id = 140,
        probability = 1250,
    },
    [141] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s9.png",
        id = 141,
        probability = 1250,
    },
    [142] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s10.png",
        id = 142,
        probability = 0,
    },
    [143] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s11.png",
        id = 143,
        probability = 0,
    },
    [144] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s12.png",
        id = 144,
        probability = 0,
    },
    [145] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s13.png",
        id = 145,
        probability = 1250,
    },
    [146] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s15.png",
        id = 146,
        probability = 0,
    },
    [147] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s57.png",
        id = 147,
        probability = 0,
    },
    [148] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s24.png",
        id = 148,
        probability = 0,
    },
    [149] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s25.png",
        id = 149,
        probability = 0,
    },
    [150] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s14.png",
        id = 150,
        probability = 1250,
    },
    [151] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s53.png",
        id = 151,
        probability = 0,
    },
    [152] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s54.png",
        id = 152,
        probability = 0,
    },
    [153] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s55.png",
        id = 153,
        probability = 0,
    },
    [154] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 34,
        res = "ui/update/s56.png",
        id = 154,
        probability = 0,
    },
    [155] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s1.png",
        id = 155,
        probability = 1250,
    },
    [156] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s2.png",
        id = 156,
        probability = 1250,
    },
    [157] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s3.png",
        id = 157,
        probability = 1250,
    },
    [158] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s4.png",
        id = 158,
        probability = 1250,
    },
    [159] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s5.png",
        id = 159,
        probability = 1250,
    },
    [160] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s6.png",
        id = 160,
        probability = 1250,
    },
    [161] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s7.png",
        id = 161,
        probability = 1250,
    },
    [162] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s8.png",
        id = 162,
        probability = 1250,
    },
    [163] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s9.png",
        id = 163,
        probability = 1250,
    },
    [164] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s10.png",
        id = 164,
        probability = 0,
    },
    [165] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s11.png",
        id = 165,
        probability = 0,
    },
    [166] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s12.png",
        id = 166,
        probability = 0,
    },
    [167] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s13.png",
        id = 167,
        probability = 1250,
    },
    [168] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s15.png",
        id = 168,
        probability = 0,
    },
    [169] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s57.png",
        id = 169,
        probability = 0,
    },
    [170] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s24.png",
        id = 170,
        probability = 0,
    },
    [171] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s25.png",
        id = 171,
        probability = 0,
    },
    [172] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s14.png",
        id = 172,
        probability = 1250,
    },
    [173] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s53.png",
        id = 173,
        probability = 0,
    },
    [174] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s54.png",
        id = 174,
        probability = 0,
    },
    [175] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s55.png",
        id = 175,
        probability = 0,
    },
    [176] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 35,
        res = "ui/update/s56.png",
        id = 176,
        probability = 0,
    },
    [177] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s1.png",
        id = 177,
        probability = 1250,
    },
    [178] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s2.png",
        id = 178,
        probability = 1250,
    },
    [179] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s3.png",
        id = 179,
        probability = 1250,
    },
    [180] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s4.png",
        id = 180,
        probability = 1250,
    },
    [181] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s5.png",
        id = 181,
        probability = 1250,
    },
    [182] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s6.png",
        id = 182,
        probability = 1250,
    },
    [183] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s7.png",
        id = 183,
        probability = 1250,
    },
    [184] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s8.png",
        id = 184,
        probability = 1250,
    },
    [185] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s9.png",
        id = 185,
        probability = 1250,
    },
    [186] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s10.png",
        id = 186,
        probability = 0,
    },
    [187] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s11.png",
        id = 187,
        probability = 0,
    },
    [188] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s12.png",
        id = 188,
        probability = 0,
    },
    [189] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s13.png",
        id = 189,
        probability = 1250,
    },
    [190] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s15.png",
        id = 190,
        probability = 0,
    },
    [191] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s57.png",
        id = 191,
        probability = 0,
    },
    [192] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s24.png",
        id = 192,
        probability = 0,
    },
    [193] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s25.png",
        id = 193,
        probability = 0,
    },
    [194] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s14.png",
        id = 194,
        probability = 1250,
    },
    [195] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s53.png",
        id = 195,
        probability = 0,
    },
    [196] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s54.png",
        id = 196,
        probability = 0,
    },
    [197] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s55.png",
        id = 197,
        probability = 0,
    },
    [198] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 36,
        res = "ui/update/s56.png",
        id = 198,
        probability = 0,
    },
    [199] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s1.png",
        id = 199,
        probability = 1250,
    },
    [200] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s2.png",
        id = 200,
        probability = 1250,
    },
    [201] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s3.png",
        id = 201,
        probability = 1250,
    },
    [202] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s4.png",
        id = 202,
        probability = 1250,
    },
    [203] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s5.png",
        id = 203,
        probability = 1250,
    },
    [204] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s6.png",
        id = 204,
        probability = 1250,
    },
    [205] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s7.png",
        id = 205,
        probability = 1250,
    },
    [206] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s8.png",
        id = 206,
        probability = 1250,
    },
    [207] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s9.png",
        id = 207,
        probability = 1250,
    },
    [208] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s10.png",
        id = 208,
        probability = 1250,
    },
    [209] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s11.png",
        id = 209,
        probability = 0,
    },
    [210] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s12.png",
        id = 210,
        probability = 0,
    },
    [211] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s13.png",
        id = 211,
        probability = 1250,
    },
    [212] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s15.png",
        id = 212,
        probability = 0,
    },
    [213] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s57.png",
        id = 213,
        probability = 0,
    },
    [214] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s24.png",
        id = 214,
        probability = 0,
    },
    [215] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s25.png",
        id = 215,
        probability = 0,
    },
    [216] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s14.png",
        id = 216,
        probability = 1250,
    },
    [217] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s53.png",
        id = 217,
        probability = 0,
    },
    [218] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s54.png",
        id = 218,
        probability = 0,
    },
    [219] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s55.png",
        id = 219,
        probability = 0,
    },
    [220] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 37,
        res = "ui/update/s56.png",
        id = 220,
        probability = 0,
    },
    [221] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s1.png",
        id = 221,
        probability = 1250,
    },
    [222] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s2.png",
        id = 222,
        probability = 1250,
    },
    [223] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s3.png",
        id = 223,
        probability = 1250,
    },
    [224] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s4.png",
        id = 224,
        probability = 1250,
    },
    [225] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s5.png",
        id = 225,
        probability = 1250,
    },
    [226] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s6.png",
        id = 226,
        probability = 1250,
    },
    [227] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s7.png",
        id = 227,
        probability = 1250,
    },
    [228] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s8.png",
        id = 228,
        probability = 1250,
    },
    [229] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s9.png",
        id = 229,
        probability = 1250,
    },
    [230] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s10.png",
        id = 230,
        probability = 1250,
    },
    [231] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s11.png",
        id = 231,
        probability = 0,
    },
    [232] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s12.png",
        id = 232,
        probability = 0,
    },
    [233] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s13.png",
        id = 233,
        probability = 1250,
    },
    [234] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s15.png",
        id = 234,
        probability = 0,
    },
    [235] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s57.png",
        id = 235,
        probability = 0,
    },
    [236] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s24.png",
        id = 236,
        probability = 0,
    },
    [237] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s25.png",
        id = 237,
        probability = 0,
    },
    [238] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s14.png",
        id = 238,
        probability = 1250,
    },
    [239] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s53.png",
        id = 239,
        probability = 0,
    },
    [240] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s54.png",
        id = 240,
        probability = 0,
    },
    [241] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s55.png",
        id = 241,
        probability = 0,
    },
    [242] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 38,
        res = "ui/update/s56.png",
        id = 242,
        probability = 0,
    },
    [243] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s1.png",
        id = 243,
        probability = 1250,
    },
    [244] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s2.png",
        id = 244,
        probability = 1250,
    },
    [245] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s3.png",
        id = 245,
        probability = 1250,
    },
    [246] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s4.png",
        id = 246,
        probability = 1250,
    },
    [247] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s5.png",
        id = 247,
        probability = 1250,
    },
    [248] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s6.png",
        id = 248,
        probability = 1250,
    },
    [249] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s7.png",
        id = 249,
        probability = 1250,
    },
    [250] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s8.png",
        id = 250,
        probability = 1250,
    },
    [251] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s9.png",
        id = 251,
        probability = 1250,
    },
    [252] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s10.png",
        id = 252,
        probability = 1250,
    },
    [253] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s11.png",
        id = 253,
        probability = 0,
    },
    [254] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s12.png",
        id = 254,
        probability = 0,
    },
    [255] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s13.png",
        id = 255,
        probability = 1250,
    },
    [256] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s15.png",
        id = 256,
        probability = 0,
    },
    [257] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s57.png",
        id = 257,
        probability = 0,
    },
    [258] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s24.png",
        id = 258,
        probability = 0,
    },
    [259] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s25.png",
        id = 259,
        probability = 0,
    },
    [260] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s14.png",
        id = 260,
        probability = 1250,
    },
    [261] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s53.png",
        id = 261,
        probability = 0,
    },
    [262] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s54.png",
        id = 262,
        probability = 0,
    },
    [263] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s55.png",
        id = 263,
        probability = 0,
    },
    [264] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 39,
        res = "ui/update/s56.png",
        id = 264,
        probability = 0,
    },
    [265] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s1.png",
        id = 265,
        probability = 1250,
    },
    [266] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s2.png",
        id = 266,
        probability = 1250,
    },
    [267] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s3.png",
        id = 267,
        probability = 1250,
    },
    [268] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s4.png",
        id = 268,
        probability = 1250,
    },
    [269] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s5.png",
        id = 269,
        probability = 1250,
    },
    [270] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s6.png",
        id = 270,
        probability = 1250,
    },
    [271] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s7.png",
        id = 271,
        probability = 1250,
    },
    [272] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s8.png",
        id = 272,
        probability = 1250,
    },
    [273] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s9.png",
        id = 273,
        probability = 1250,
    },
    [274] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s10.png",
        id = 274,
        probability = 1250,
    },
    [275] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s11.png",
        id = 275,
        probability = 0,
    },
    [276] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s12.png",
        id = 276,
        probability = 0,
    },
    [277] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s13.png",
        id = 277,
        probability = 1250,
    },
    [278] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s15.png",
        id = 278,
        probability = 0,
    },
    [279] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s57.png",
        id = 279,
        probability = 0,
    },
    [280] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s24.png",
        id = 280,
        probability = 0,
    },
    [281] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s25.png",
        id = 281,
        probability = 0,
    },
    [282] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s14.png",
        id = 282,
        probability = 1250,
    },
    [283] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s53.png",
        id = 283,
        probability = 0,
    },
    [284] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s54.png",
        id = 284,
        probability = 0,
    },
    [285] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s55.png",
        id = 285,
        probability = 0,
    },
    [286] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 40,
        res = "ui/update/s56.png",
        id = 286,
        probability = 0,
    },
    [287] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s1.png",
        id = 287,
        probability = 1250,
    },
    [288] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s2.png",
        id = 288,
        probability = 1250,
    },
    [289] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s3.png",
        id = 289,
        probability = 1250,
    },
    [290] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s4.png",
        id = 290,
        probability = 1250,
    },
    [291] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s5.png",
        id = 291,
        probability = 1250,
    },
    [292] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s6.png",
        id = 292,
        probability = 1250,
    },
    [293] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s7.png",
        id = 293,
        probability = 1250,
    },
    [294] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s8.png",
        id = 294,
        probability = 1250,
    },
    [295] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s9.png",
        id = 295,
        probability = 1250,
    },
    [296] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s10.png",
        id = 296,
        probability = 1250,
    },
    [297] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s11.png",
        id = 297,
        probability = 0,
    },
    [298] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s12.png",
        id = 298,
        probability = 0,
    },
    [299] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s13.png",
        id = 299,
        probability = 1250,
    },
    [300] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s15.png",
        id = 300,
        probability = 0,
    },
    [301] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s57.png",
        id = 301,
        probability = 0,
    },
    [302] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s24.png",
        id = 302,
        probability = 0,
    },
    [303] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s25.png",
        id = 303,
        probability = 0,
    },
    [304] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s14.png",
        id = 304,
        probability = 1250,
    },
    [305] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s53.png",
        id = 305,
        probability = 0,
    },
    [306] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s54.png",
        id = 306,
        probability = 0,
    },
    [307] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s55.png",
        id = 307,
        probability = 0,
    },
    [308] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 41,
        res = "ui/update/s56.png",
        id = 308,
        probability = 0,
    },
    [309] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s1.png",
        id = 309,
        probability = 1250,
    },
    [310] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s2.png",
        id = 310,
        probability = 1250,
    },
    [311] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s3.png",
        id = 311,
        probability = 1250,
    },
    [312] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s4.png",
        id = 312,
        probability = 1250,
    },
    [313] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s5.png",
        id = 313,
        probability = 1250,
    },
    [314] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s6.png",
        id = 314,
        probability = 1250,
    },
    [315] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s7.png",
        id = 315,
        probability = 1250,
    },
    [316] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s8.png",
        id = 316,
        probability = 1250,
    },
    [317] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s9.png",
        id = 317,
        probability = 1250,
    },
    [318] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s10.png",
        id = 318,
        probability = 1250,
    },
    [319] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s11.png",
        id = 319,
        probability = 0,
    },
    [320] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s12.png",
        id = 320,
        probability = 0,
    },
    [321] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s13.png",
        id = 321,
        probability = 1250,
    },
    [322] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s15.png",
        id = 322,
        probability = 0,
    },
    [323] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s57.png",
        id = 323,
        probability = 0,
    },
    [324] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s24.png",
        id = 324,
        probability = 0,
    },
    [325] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s25.png",
        id = 325,
        probability = 0,
    },
    [326] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s14.png",
        id = 326,
        probability = 1250,
    },
    [327] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s53.png",
        id = 327,
        probability = 0,
    },
    [328] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s54.png",
        id = 328,
        probability = 0,
    },
    [329] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s55.png",
        id = 329,
        probability = 0,
    },
    [330] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 101,
        res = "ui/update/s56.png",
        id = 330,
        probability = 0,
    },
    [331] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s1.png",
        id = 331,
        probability = 1250,
    },
    [332] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s2.png",
        id = 332,
        probability = 1250,
    },
    [333] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s3.png",
        id = 333,
        probability = 1250,
    },
    [334] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s4.png",
        id = 334,
        probability = 1250,
    },
    [335] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s5.png",
        id = 335,
        probability = 1250,
    },
    [336] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s6.png",
        id = 336,
        probability = 1250,
    },
    [337] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s7.png",
        id = 337,
        probability = 1250,
    },
    [338] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s8.png",
        id = 338,
        probability = 1250,
    },
    [339] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s9.png",
        id = 339,
        probability = 1250,
    },
    [340] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s10.png",
        id = 340,
        probability = 1250,
    },
    [341] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s11.png",
        id = 341,
        probability = 0,
    },
    [342] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s12.png",
        id = 342,
        probability = 0,
    },
    [343] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s13.png",
        id = 343,
        probability = 1250,
    },
    [344] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s15.png",
        id = 344,
        probability = 0,
    },
    [345] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s57.png",
        id = 345,
        probability = 0,
    },
    [346] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s24.png",
        id = 346,
        probability = 0,
    },
    [347] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s25.png",
        id = 347,
        probability = 0,
    },
    [348] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s14.png",
        id = 348,
        probability = 1250,
    },
    [349] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s53.png",
        id = 349,
        probability = 0,
    },
    [350] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s54.png",
        id = 350,
        probability = 0,
    },
    [351] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s55.png",
        id = 351,
        probability = 0,
    },
    [352] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 102,
        res = "ui/update/s56.png",
        id = 352,
        probability = 0,
    },
    [353] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s1.png",
        id = 353,
        probability = 1250,
    },
    [354] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s2.png",
        id = 354,
        probability = 1250,
    },
    [355] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s3.png",
        id = 355,
        probability = 1250,
    },
    [356] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s4.png",
        id = 356,
        probability = 1250,
    },
    [357] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s5.png",
        id = 357,
        probability = 1250,
    },
    [358] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s6.png",
        id = 358,
        probability = 1250,
    },
    [359] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s7.png",
        id = 359,
        probability = 1250,
    },
    [360] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s8.png",
        id = 360,
        probability = 1250,
    },
    [361] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s9.png",
        id = 361,
        probability = 1250,
    },
    [362] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s10.png",
        id = 362,
        probability = 1250,
    },
    [363] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s11.png",
        id = 363,
        probability = 0,
    },
    [364] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s12.png",
        id = 364,
        probability = 0,
    },
    [365] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s13.png",
        id = 365,
        probability = 1250,
    },
    [366] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s15.png",
        id = 366,
        probability = 0,
    },
    [367] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s57.png",
        id = 367,
        probability = 0,
    },
    [368] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s24.png",
        id = 368,
        probability = 0,
    },
    [369] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s25.png",
        id = 369,
        probability = 0,
    },
    [370] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s14.png",
        id = 370,
        probability = 1250,
    },
    [371] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s53.png",
        id = 371,
        probability = 0,
    },
    [372] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s54.png",
        id = 372,
        probability = 0,
    },
    [373] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s55.png",
        id = 373,
        probability = 0,
    },
    [374] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 103,
        res = "ui/update/s56.png",
        id = 374,
        probability = 0,
    },
    [375] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s1.png",
        id = 375,
        probability = 1250,
    },
    [376] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s2.png",
        id = 376,
        probability = 1250,
    },
    [377] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s3.png",
        id = 377,
        probability = 1250,
    },
    [378] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s4.png",
        id = 378,
        probability = 1250,
    },
    [379] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s5.png",
        id = 379,
        probability = 1250,
    },
    [380] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s6.png",
        id = 380,
        probability = 1250,
    },
    [381] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s7.png",
        id = 381,
        probability = 1250,
    },
    [382] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s8.png",
        id = 382,
        probability = 1250,
    },
    [383] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s9.png",
        id = 383,
        probability = 1250,
    },
    [384] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s10.png",
        id = 384,
        probability = 1250,
    },
    [385] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s11.png",
        id = 385,
        probability = 0,
    },
    [386] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s12.png",
        id = 386,
        probability = 0,
    },
    [387] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s13.png",
        id = 387,
        probability = 1250,
    },
    [388] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s15.png",
        id = 388,
        probability = 0,
    },
    [389] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s57.png",
        id = 389,
        probability = 0,
    },
    [390] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s24.png",
        id = 390,
        probability = 0,
    },
    [391] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s25.png",
        id = 391,
        probability = 0,
    },
    [392] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s14.png",
        id = 392,
        probability = 1250,
    },
    [393] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s53.png",
        id = 393,
        probability = 0,
    },
    [394] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s54.png",
        id = 394,
        probability = 0,
    },
    [395] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s55.png",
        id = 395,
        probability = 0,
    },
    [396] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 104,
        res = "ui/update/s56.png",
        id = 396,
        probability = 0,
    },
    [397] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s1.png",
        id = 397,
        probability = 1250,
    },
    [398] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s2.png",
        id = 398,
        probability = 1250,
    },
    [399] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s3.png",
        id = 399,
        probability = 1250,
    },
    [400] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s4.png",
        id = 400,
        probability = 1250,
    },
    [401] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s5.png",
        id = 401,
        probability = 1250,
    },
    [402] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s6.png",
        id = 402,
        probability = 1250,
    },
    [403] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s7.png",
        id = 403,
        probability = 1250,
    },
    [404] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s8.png",
        id = 404,
        probability = 1250,
    },
    [405] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s9.png",
        id = 405,
        probability = 1250,
    },
    [406] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s10.png",
        id = 406,
        probability = 1250,
    },
    [407] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s11.png",
        id = 407,
        probability = 0,
    },
    [408] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s12.png",
        id = 408,
        probability = 0,
    },
    [409] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s13.png",
        id = 409,
        probability = 1250,
    },
    [410] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s15.png",
        id = 410,
        probability = 0,
    },
    [411] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s57.png",
        id = 411,
        probability = 0,
    },
    [412] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s24.png",
        id = 412,
        probability = 0,
    },
    [413] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s25.png",
        id = 413,
        probability = 0,
    },
    [414] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s14.png",
        id = 414,
        probability = 1250,
    },
    [415] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s53.png",
        id = 415,
        probability = 0,
    },
    [416] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s54.png",
        id = 416,
        probability = 0,
    },
    [417] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s55.png",
        id = 417,
        probability = 0,
    },
    [418] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 105,
        res = "ui/update/s56.png",
        id = 418,
        probability = 0,
    },
    [419] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s1.png",
        id = 419,
        probability = 1250,
    },
    [420] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s2.png",
        id = 420,
        probability = 1250,
    },
    [421] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s3.png",
        id = 421,
        probability = 1250,
    },
    [422] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s4.png",
        id = 422,
        probability = 1250,
    },
    [423] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s5.png",
        id = 423,
        probability = 1250,
    },
    [424] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s6.png",
        id = 424,
        probability = 1250,
    },
    [425] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s7.png",
        id = 425,
        probability = 1250,
    },
    [426] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s8.png",
        id = 426,
        probability = 1250,
    },
    [427] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s9.png",
        id = 427,
        probability = 1250,
    },
    [428] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s10.png",
        id = 428,
        probability = 1250,
    },
    [429] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s11.png",
        id = 429,
        probability = 0,
    },
    [430] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s12.png",
        id = 430,
        probability = 0,
    },
    [431] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s13.png",
        id = 431,
        probability = 1250,
    },
    [432] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s15.png",
        id = 432,
        probability = 0,
    },
    [433] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s57.png",
        id = 433,
        probability = 0,
    },
    [434] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s24.png",
        id = 434,
        probability = 0,
    },
    [435] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s25.png",
        id = 435,
        probability = 0,
    },
    [436] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s14.png",
        id = 436,
        probability = 1250,
    },
    [437] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s53.png",
        id = 437,
        probability = 0,
    },
    [438] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s54.png",
        id = 438,
        probability = 0,
    },
    [439] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s55.png",
        id = 439,
        probability = 0,
    },
    [440] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 110,
        res = "ui/update/s56.png",
        id = 440,
        probability = 0,
    },
    [441] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s1.png",
        id = 441,
        probability = 1250,
    },
    [442] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s2.png",
        id = 442,
        probability = 1250,
    },
    [443] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s3.png",
        id = 443,
        probability = 1250,
    },
    [444] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s4.png",
        id = 444,
        probability = 1250,
    },
    [445] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s5.png",
        id = 445,
        probability = 1250,
    },
    [446] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s6.png",
        id = 446,
        probability = 1250,
    },
    [447] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s7.png",
        id = 447,
        probability = 1250,
    },
    [448] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s8.png",
        id = 448,
        probability = 1250,
    },
    [449] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s9.png",
        id = 449,
        probability = 1250,
    },
    [450] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s10.png",
        id = 450,
        probability = 1250,
    },
    [451] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s11.png",
        id = 451,
        probability = 0,
    },
    [452] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s12.png",
        id = 452,
        probability = 0,
    },
    [453] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s13.png",
        id = 453,
        probability = 1250,
    },
    [454] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s15.png",
        id = 454,
        probability = 0,
    },
    [455] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s57.png",
        id = 455,
        probability = 0,
    },
    [456] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s24.png",
        id = 456,
        probability = 0,
    },
    [457] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s25.png",
        id = 457,
        probability = 0,
    },
    [458] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s14.png",
        id = 458,
        probability = 1250,
    },
    [459] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s53.png",
        id = 459,
        probability = 0,
    },
    [460] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s54.png",
        id = 460,
        probability = 0,
    },
    [461] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s55.png",
        id = 461,
        probability = 0,
    },
    [462] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 112,
        res = "ui/update/s56.png",
        id = 462,
        probability = 0,
    },
    [463] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s1.png",
        id = 463,
        probability = 1250,
    },
    [464] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s2.png",
        id = 464,
        probability = 1250,
    },
    [465] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s3.png",
        id = 465,
        probability = 1250,
    },
    [466] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s4.png",
        id = 466,
        probability = 1250,
    },
    [467] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s5.png",
        id = 467,
        probability = 1250,
    },
    [468] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s6.png",
        id = 468,
        probability = 1250,
    },
    [469] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s7.png",
        id = 469,
        probability = 1250,
    },
    [470] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s8.png",
        id = 470,
        probability = 1250,
    },
    [471] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s9.png",
        id = 471,
        probability = 1250,
    },
    [472] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s10.png",
        id = 472,
        probability = 1250,
    },
    [473] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s11.png",
        id = 473,
        probability = 0,
    },
    [474] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s12.png",
        id = 474,
        probability = 0,
    },
    [475] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s13.png",
        id = 475,
        probability = 1250,
    },
    [476] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s15.png",
        id = 476,
        probability = 0,
    },
    [477] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s57.png",
        id = 477,
        probability = 0,
    },
    [478] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s24.png",
        id = 478,
        probability = 0,
    },
    [479] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s25.png",
        id = 479,
        probability = 0,
    },
    [480] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s14.png",
        id = 480,
        probability = 1250,
    },
    [481] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s53.png",
        id = 481,
        probability = 0,
    },
    [482] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s54.png",
        id = 482,
        probability = 0,
    },
    [483] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s55.png",
        id = 483,
        probability = 0,
    },
    [484] = {
        descID = {
            [1] = 28101,
            [2] = 28187,
        },
        showType = 113,
        res = "ui/update/s56.png",
        id = 484,
        probability = 0,
    },
}