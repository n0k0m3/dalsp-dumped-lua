return {
    [300003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 0,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 300003,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 1202,
        timeFrame = {
        },
        name = "",
        pictureIcon = "",
        dungeonType = 21,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [100004] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 4,
        activityDsc = "",
        preDungeonId = {
            [1] = 110323,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 100004,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 10,
        timeFrame = {
        },
        name = "300514",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [9201] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 20,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 9201,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 901,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 11,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [800001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 0,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 800001,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 1101,
        timeFrame = {
        },
        name = "",
        pictureIcon = "",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [90003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 3,
        activityDsc = "",
        preDungeonId = {
            [1] = 109214,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 90003,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 9,
        timeFrame = {
        },
        name = "300513",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [8001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 3,
                        [2] = 0,
                    },
                    tag = "c1_3_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c2_6_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 9,
                        [2] = 0,
                    },
                    tag = "c3_9_0",
                    reward = {
                        [500002] = 30,
                        [510302] = 1,
                        [500001] = 5000,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c4_12_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 15,
                        [2] = 0,
                    },
                    tag = "c5_15_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c6_18_0",
                    reward = {
                        [500002] = 30,
                        [510302] = 1,
                        [500001] = 5000,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 21,
                        [2] = 0,
                    },
                    tag = "c7_21_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c8_24_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 5000,
                        [570033] = 1,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 8001,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 501,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 9,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [80002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 108107,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 80002,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 8,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [3001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 11,
        order = 1,
        activityDsc = "",
        preDungeonId = {
            [1] = 102314,
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 2000,
                        [570033] = 1,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 30,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 51,
                        [2] = 0,
                    },
                    tag = "c9_51_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 2000,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 15,
                        [500001] = 3000,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 40,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 20,
                        [500001] = 3000,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 40,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 45,
                        [2] = 0,
                    },
                    tag = "c8_45_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 3000,
                        [510307] = 2,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 25,
                        [500001] = 4000,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 27,
                        [2] = 0,
                    },
                    tag = "c5_27_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 4000,
                        [510307] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 3001,
        layout = {
            [1] = 513,
            [2] = 513,
            [3] = 513,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 3,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [8003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 3,
                        [2] = 0,
                    },
                    tag = "c1_3_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c2_6_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 9,
                        [2] = 0,
                    },
                    tag = "c3_9_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c4_12_0",
                    reward = {
                        [500002] = 30,
                        [510302] = 1,
                        [500001] = 5000,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 15,
                        [2] = 0,
                    },
                    tag = "c5_15_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c6_18_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 21,
                        [2] = 0,
                    },
                    tag = "c7_21_0",
                    reward = {
                        [500002] = 30,
                        [510302] = 1,
                        [500001] = 5000,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c8_24_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 27,
                        [2] = 0,
                    },
                    tag = "c9_27_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c10_30_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 5000,
                        [570033] = 1,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 8003,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 503,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 9,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [110006] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 6,
        activityDsc = "",
        preDungeonId = {
            [1] = 111536,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 110006,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 11,
        timeFrame = {
        },
        name = "300516",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [3002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 103106,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 3002,
        layout = {
            [1] = 513,
            [2] = 513,
            [3] = 513,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 3,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [8005] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 5,
                        [2] = 0,
                    },
                    tag = "c1_5_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 10,
                        [2] = 0,
                    },
                    tag = "c2_10_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 15,
                        [2] = 0,
                    },
                    tag = "c3_15_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 20,
                        [2] = 0,
                    },
                    tag = "c4_20_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 25,
                        [2] = 0,
                    },
                    tag = "c5_25_0",
                    reward = {
                        [500002] = 30,
                        [510317] = 1,
                        [500001] = 5000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c6_30_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 35,
                        [2] = 0,
                    },
                    tag = "c7_35_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 40,
                        [2] = 0,
                    },
                    tag = "c8_40_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 45,
                        [2] = 0,
                    },
                    tag = "c9_45_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 50,
                        [2] = 0,
                    },
                    tag = "c10_50_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 55,
                        [2] = 0,
                    },
                    tag = "c11_55_0",
                    reward = {
                        [500002] = 30,
                        [510317] = 1,
                        [500001] = 5000,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c12_60_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 65,
                        [2] = 0,
                    },
                    tag = "c13_65_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 70,
                        [2] = 0,
                    },
                    tag = "c14_70_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 75,
                        [2] = 0,
                    },
                    tag = "c15_75_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [16] = {
                    cond = {
                        [1] = 80,
                        [2] = 0,
                    },
                    tag = "c16_80_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [21] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c21_90_0",
                    reward = {
                        [500002] = 30,
                        [510317] = 1,
                        [500001] = 5000,
                    },
                },
                [17] = {
                    cond = {
                        [1] = 85,
                        [2] = 0,
                    },
                    tag = "c17_85_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 8005,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 2001,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 17,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [200002] = {
        resIcon = "icon/fuben/005.png",
        heroChapter = 0,
        unlockLevel = 10,
        order = 2,
        activityDsc = "300132",
        preDungeonId = {
        },
        reward = {
        },
        price = {
            [500002] = 50,
        },
        buyCountLimit = 2,
        openTimeType = 4,
        cycleParam = 0,
        cycleType = 1,
        countLimit = 1,
        id = 200002,
        layout = {
            [1] = 0,
        },
        dropShow = {
        },
        desc = "300422",
        dungeonChapterId = 101,
        timeFrame = {
            [1] = 4,
            [2] = 5,
            [3] = 7,
            [4] = 1,
        },
        name = "300521",
        pictureIcon = "icon/fuben/Dungeon_Coin.png",
        dungeonType = 3,
        pictureText = "",
        buyCountType = 2,
        titleName = "300550",
    },
    [3003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 3,
        activityDsc = "",
        preDungeonId = {
            [1] = 103211,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 3003,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 3,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [500001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 1000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 1000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 1000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 21,
                        [2] = 0,
                    },
                    tag = "c4_21_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 1000,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 10,
                        [500001] = 2000,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 2000,
                        [510301] = 2,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 15,
                        [500001] = 3000,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 3000,
                        [510301] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500001,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 405,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 7,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [2002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 102104,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 2002,
        layout = {
            [1] = 512,
            [2] = 512,
            [3] = 512,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 2,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [500002] = {
        resIcon = "",
        heroChapter = 1,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500002,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 411,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 30,
        pictureText = "",
        buyCountType = 0,
        titleName = "Chapter 1 ",
    },
    [200003] = {
        resIcon = "icon/fuben/007.png",
        heroChapter = 0,
        unlockLevel = 16,
        order = 4,
        activityDsc = "300133",
        preDungeonId = {
        },
        reward = {
        },
        price = {
            [500002] = 50,
        },
        buyCountLimit = 2,
        openTimeType = 4,
        cycleParam = 0,
        cycleType = 1,
        countLimit = 1,
        id = 200003,
        layout = {
            [1] = 0,
        },
        dropShow = {
        },
        desc = "300423",
        dungeonChapterId = 101,
        timeFrame = {
            [1] = 3,
            [2] = 5,
            [3] = 7,
            [4] = 1,
        },
        name = "300522",
        pictureIcon = "icon/fuben/Dungeon_Pray.png",
        dungeonType = 3,
        pictureText = "",
        buyCountType = 2,
        titleName = "300551",
    },
    [500003] = {
        resIcon = "",
        heroChapter = 2,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 9,
                        [2] = 0,
                    },
                    tag = "c1_9_0",
                    reward = {
                        [570002] = 10,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c2_18_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 10,
                        [570033] = 1,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 27,
                        [2] = 0,
                    },
                    tag = "c3_27_0",
                    reward = {
                        [570002] = 10,
                        [500002] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c4_36_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 10,
                        [570035] = 1,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500003,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 411,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 30,
        pictureText = "",
        buyCountType = 0,
        titleName = "Chapter 2",
    },
    [2003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 3,
        activityDsc = "",
        preDungeonId = {
            [1] = 102208,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 2003,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 2,
        timeFrame = {
        },
        name = "300513",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [500004] = {
        resIcon = "",
        heroChapter = 1,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500004,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 412,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 30,
        pictureText = "",
        buyCountType = 0,
        titleName = "Chapter 1 ",
    },
    [200004] = {
        resIcon = "icon/fuben/009.png",
        heroChapter = 0,
        unlockLevel = 10,
        order = 3,
        activityDsc = "300134",
        preDungeonId = {
        },
        reward = {
        },
        price = {
            [500002] = 50,
        },
        buyCountLimit = 2,
        openTimeType = 4,
        cycleParam = 0,
        cycleType = 1,
        countLimit = 1,
        id = 200004,
        layout = {
            [1] = 0,
        },
        dropShow = {
        },
        desc = "300424",
        dungeonChapterId = 101,
        timeFrame = {
            [1] = 2,
            [2] = 3,
            [3] = 7,
            [4] = 1,
        },
        name = "300530",
        pictureIcon = "icon/fuben/Dungeon_Sephiroth.png",
        dungeonType = 3,
        pictureText = "",
        buyCountType = 2,
        titleName = "300552",
    },
    [500005] = {
        resIcon = "",
        heroChapter = 2,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 9,
                        [2] = 0,
                    },
                    tag = "c1_9_0",
                    reward = {
                        [500002] = 50,
                        [500077] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c2_18_0",
                    reward = {
                        [500077] = 40,
                        [570033] = 1,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 27,
                        [2] = 0,
                    },
                    tag = "c3_27_0",
                    reward = {
                        [500002] = 50,
                        [500077] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c4_36_0",
                    reward = {
                        [500077] = 40,
                        [570035] = 1,
                        [500002] = 50,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500005,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 412,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 30,
        pictureText = "",
        buyCountType = 0,
        titleName = "Chapter 2",
    },
    [90005] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 5,
        activityDsc = "",
        preDungeonId = {
            [1] = 109428,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 90005,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 9,
        timeFrame = {
        },
        name = "300515",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [500006] = {
        resIcon = "",
        heroChapter = 1,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500006,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 413,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 30,
        pictureText = "",
        buyCountType = 0,
        titleName = "Chapter 1 ",
    },
    [7001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 47,
        order = 1,
        activityDsc = "",
        preDungeonId = {
            [1] = 106432,
            [2] = 106433,
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 4000,
                        [570033] = 1,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c15_90_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [16] = {
                    cond = {
                        [1] = 99,
                        [2] = 0,
                    },
                    tag = "c16_99_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 4000,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 15,
                        [500001] = 5000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 20,
                        [500001] = 5000,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 75,
                        [2] = 0,
                    },
                    tag = "c13_75_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 5000,
                        [510313] = 2,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 30,
                        [500001] = 6000,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 45,
                        [2] = 0,
                    },
                    tag = "c8_45_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 6000,
                        [510313] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 7001,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 7,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [80004] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 4,
        activityDsc = "",
        preDungeonId = {
            [1] = 108323,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 80004,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 8,
        timeFrame = {
        },
        name = "300514",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [7002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 107108,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 7002,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 7,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [500007] = {
        resIcon = "",
        heroChapter = 2,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 9,
                        [2] = 0,
                    },
                    tag = "c1_9_0",
                    reward = {
                        [500002] = 50,
                        [500077] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c2_18_0",
                    reward = {
                        [500077] = 40,
                        [570033] = 1,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 27,
                        [2] = 0,
                    },
                    tag = "c3_27_0",
                    reward = {
                        [500002] = 50,
                        [500077] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c4_36_0",
                    reward = {
                        [500077] = 40,
                        [570035] = 1,
                        [500002] = 50,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500007,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 413,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 30,
        pictureText = "",
        buyCountType = 0,
        titleName = "Chapter 2",
    },
    [7003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 3,
        activityDsc = "",
        preDungeonId = {
            [1] = 107216,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 7003,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 7,
        timeFrame = {
        },
        name = "300513",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [7004] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 4,
        activityDsc = "",
        preDungeonId = {
            [1] = 107324,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 7004,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 7,
        timeFrame = {
        },
        name = "300514",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [500008] = {
        resIcon = "",
        heroChapter = 1,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500008,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 414,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 30,
        pictureText = "",
        buyCountType = 0,
        titleName = "Chapter 1 ",
    },
    [7005] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 5,
        activityDsc = "",
        preDungeonId = {
            [1] = 107431,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 7005,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 7,
        timeFrame = {
        },
        name = "300515",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [9502] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 9502,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 3102,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 102,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [500009] = {
        resIcon = "",
        heroChapter = 2,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 9,
                        [2] = 0,
                    },
                    tag = "c1_9_0",
                    reward = {
                        [570002] = 10,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c2_18_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 10,
                        [570033] = 1,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 27,
                        [2] = 0,
                    },
                    tag = "c3_27_0",
                    reward = {
                        [570002] = 10,
                        [500002] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c4_36_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 10,
                        [570035] = 1,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500009,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 414,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 30,
        pictureText = "",
        buyCountType = 0,
        titleName = "Chapter 2",
    },
    [90006] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 6,
        activityDsc = "",
        preDungeonId = {
            [1] = 109534,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 90006,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 9,
        timeFrame = {
        },
        name = "300516",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [600001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 600001,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 406,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 8,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [500010] = {
        resIcon = "",
        heroChapter = 1,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500010,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 416,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 30,
        pictureText = "",
        buyCountType = 0,
        titleName = "Chapter 1 ",
    },
    [80005] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 5,
        activityDsc = "",
        preDungeonId = {
            [1] = 108431,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 80005,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 8,
        timeFrame = {
        },
        name = "300515",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [500011] = {
        resIcon = "",
        heroChapter = 2,
        unlockLevel = 15,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 9,
                        [2] = 0,
                    },
                    tag = "c1_9_0",
                    reward = {
                        [501006] = 50,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c2_18_0",
                    reward = {
                        [500002] = 50,
                        [501006] = 50,
                        [570033] = 1,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 27,
                        [2] = 0,
                    },
                    tag = "c3_27_0",
                    reward = {
                        [501006] = 50,
                        [500002] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c4_36_0",
                    reward = {
                        [500002] = 50,
                        [570035] = 1,
                        [501006] = 50,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 500011,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 416,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 30,
        pictureText = "",
        buyCountType = 0,
        titleName = "Chapter 2",
    },
    [110001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 60,
        order = 1,
        activityDsc = "",
        preDungeonId = {
            [1] = 110644,
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 4000,
                        [570033] = 1,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c15_90_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [16] = {
                    cond = {
                        [1] = 96,
                        [2] = 0,
                    },
                    tag = "c16_96_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [17] = {
                    cond = {
                        [1] = 102,
                        [2] = 0,
                    },
                    tag = "c17_102_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [18] = {
                    cond = {
                        [1] = 108,
                        [2] = 0,
                    },
                    tag = "c18_108_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 4000,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 15,
                        [500001] = 5000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 20,
                        [500001] = 5000,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c15_90_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 5000,
                        [510319] = 2,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 30,
                        [500001] = 6000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 6000,
                        [510319] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 110001,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 11,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [400001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 23,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 1000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 1000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 1000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 21,
                        [2] = 0,
                    },
                    tag = "c4_21_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 1000,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 10,
                        [500001] = 2000,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 2000,
                        [510301] = 2,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 15,
                        [500001] = 3000,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 3000,
                        [510301] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 400001,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 404,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 5,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [6001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 38,
        order = 1,
        activityDsc = "",
        preDungeonId = {
            [1] = 105412,
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 3500,
                        [570033] = 1,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 30,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 87,
                        [2] = 0,
                    },
                    tag = "c15_87_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 3500,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 4500,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 4500,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 4500,
                        [500002] = 40,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 15,
                        [500001] = 4500,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 4500,
                        [500002] = 40,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 4500,
                        [500002] = 40,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 20,
                        [500001] = 4500,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 4500,
                        [500002] = 40,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 4500,
                        [500002] = 40,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500002] = 40,
                        [510314] = 2,
                        [500001] = 4500,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 5500,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 5500,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 30,
                        [500001] = 5500,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 5500,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 5500,
                        [500002] = 50,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500002] = 50,
                        [510314] = 3,
                        [500001] = 5500,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 6001,
        layout = {
            [1] = 513,
            [2] = 513,
            [3] = 513,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 6,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [80006] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 6,
        activityDsc = "",
        preDungeonId = {
            [1] = 108539,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 80006,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 8,
        timeFrame = {
        },
        name = "300516",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [6002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 106106,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 6002,
        layout = {
            [1] = 513,
            [2] = 513,
            [3] = 513,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 6,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [9401] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500077] = 5,
                        [510323] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500077] = 10,
                        [510323] = 1,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500077] = 10,
                        [510323] = 1,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500077] = 10,
                        [510323] = 1,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500077] = 10,
                        [510323] = 1,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 27,
                        [2] = 0,
                    },
                    tag = "c5_27_0",
                    reward = {
                        [500077] = 10,
                        [510323] = 1,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 9401,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 3001,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 100,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [6003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 3,
        activityDsc = "",
        preDungeonId = {
            [1] = 106212,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 6003,
        layout = {
            [1] = 513,
            [2] = 513,
            [3] = 513,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 6,
        timeFrame = {
        },
        name = "300513",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [4002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 104105,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 4002,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 4,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_5001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [6004] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 4,
        activityDsc = "",
        preDungeonId = {
            [1] = 106318,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 6004,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 6,
        timeFrame = {
        },
        name = "300514",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [100001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 60,
        order = 1,
        activityDsc = "",
        preDungeonId = {
            [1] = 109640,
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 4000,
                        [570033] = 1,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c15_90_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [16] = {
                    cond = {
                        [1] = 96,
                        [2] = 0,
                    },
                    tag = "c16_96_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [18] = {
                    cond = {
                        [1] = 108,
                        [2] = 0,
                    },
                    tag = "c18_108_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [19] = {
                    cond = {
                        [1] = 114,
                        [2] = 0,
                    },
                    tag = "c19_114_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [20] = {
                    cond = {
                        [1] = 120,
                        [2] = 0,
                    },
                    tag = "c20_120_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 4000,
                        [570033] = 1,
                    },
                },
                [17] = {
                    cond = {
                        [1] = 102,
                        [2] = 0,
                    },
                    tag = "c17_102_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 15,
                        [500001] = 5000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 20,
                        [500001] = 5000,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c15_90_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 5000,
                        [510319] = 2,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 30,
                        [500001] = 6000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 6000,
                        [510319] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 100001,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 10,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [4003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 3,
        activityDsc = "",
        preDungeonId = {
            [1] = 104211,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 4003,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 4,
        timeFrame = {
        },
        name = "300513",
        pictureIcon = "icon/dungeon/chapter/chapter_5001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [8102] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 50,
                        [510317] = 1,
                        [500001] = 10000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500002] = 50,
                        [510317] = 1,
                        [500001] = 10000,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c15_90_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 10000,
                        [570033] = 1,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 8102,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 2003,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 17,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [1001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 1000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 1000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 1000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 21,
                        [2] = 0,
                    },
                    tag = "c4_21_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 1000,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 10,
                        [500001] = 2000,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 2000,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 2000,
                        [510301] = 2,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 15,
                        [500001] = 3000,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 3000,
                        [510301] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 1001,
        layout = {
            [1] = 511,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 1,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [1201] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 1201,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 1201,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 16,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [110003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 3,
        activityDsc = "",
        preDungeonId = {
            [1] = 111214,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 110003,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 11,
        timeFrame = {
        },
        name = "300513",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [9501] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 9501,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 3101,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 101,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [100002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 110108,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 100002,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 10,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [8004] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 8004,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 504,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 9,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [9101] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 9101,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 601,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 10,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [8101] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 5,
                        [2] = 0,
                    },
                    tag = "c1_5_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 10,
                        [2] = 0,
                    },
                    tag = "c2_10_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 15,
                        [2] = 0,
                    },
                    tag = "c3_15_0",
                    reward = {
                        [500002] = 50,
                        [510302] = 1,
                        [500001] = 10000,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 20,
                        [2] = 0,
                    },
                    tag = "c4_20_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 25,
                        [2] = 0,
                    },
                    tag = "c5_25_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c6_30_0",
                    reward = {
                        [500002] = 50,
                        [510302] = 1,
                        [500001] = 10000,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 35,
                        [2] = 0,
                    },
                    tag = "c7_35_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 40,
                        [2] = 0,
                    },
                    tag = "c8_40_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 45,
                        [2] = 0,
                    },
                    tag = "c9_45_0",
                    reward = {
                        [500002] = 50,
                        [510302] = 1,
                        [500001] = 10000,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 50,
                        [2] = 0,
                    },
                    tag = "c10_50_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 55,
                        [2] = 0,
                    },
                    tag = "c11_55_0",
                    reward = {
                        [500001] = 10000,
                        [500002] = 50,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c12_60_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 10000,
                        [570033] = 1,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 8101,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 701,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 9,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [1002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 101104,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 1002,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 1,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [8006] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 5,
                        [2] = 0,
                    },
                    tag = "c1_5_0",
                    reward = {
                        [500002] = 30,
                        [510317] = 1,
                        [500001] = 5000,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 10,
                        [2] = 0,
                    },
                    tag = "c2_10_0",
                    reward = {
                        [500002] = 30,
                        [510317] = 1,
                        [500001] = 5000,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 15,
                        [2] = 0,
                    },
                    tag = "c3_15_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 5000,
                        [570033] = 1,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 8006,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 2002,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 17,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [9301] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 1,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 9301,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 1201,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 16,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [8002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 3,
                        [2] = 0,
                    },
                    tag = "c1_3_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c2_6_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 9,
                        [2] = 0,
                    },
                    tag = "c3_9_0",
                    reward = {
                        [500002] = 30,
                        [510302] = 1,
                        [500001] = 5000,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c4_12_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 15,
                        [2] = 0,
                    },
                    tag = "c5_15_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c6_18_0",
                    reward = {
                        [500002] = 30,
                        [510302] = 1,
                        [500001] = 5000,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 21,
                        [2] = 0,
                    },
                    tag = "c7_21_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c8_24_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 5000,
                        [570033] = 1,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 8002,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 502,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/fuben/top_shixiang.png",
        dungeonType = 9,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [5001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 27,
        order = 1,
        activityDsc = "",
        preDungeonId = {
            [1] = 104315,
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 3000,
                        [570033] = 1,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 30,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 30,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 30,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500001] = 3000,
                        [500002] = 30,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 69,
                        [2] = 0,
                    },
                    tag = "c12_69_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 3000,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 40,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 20,
                        [500001] = 4000,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 40,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 40,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 4000,
                        [510312] = 2,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 40,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 40,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500002] = 40,
                        [510311] = 2,
                        [500001] = 4000,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 50,
                        [510311] = 3,
                        [500001] = 5000,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 50,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 5000,
                        [510312] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 5001,
        layout = {
            [1] = 512,
            [2] = 512,
            [3] = 512,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 5,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/dungeon/chapter/chapter_5001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [900002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 0,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 900002,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 1203,
        timeFrame = {
        },
        name = "",
        pictureIcon = "",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [5002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 105105,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 5002,
        layout = {
            [1] = 512,
            [2] = 512,
            [3] = 512,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 5,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_5001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [900001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 0,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 900001,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 1203,
        timeFrame = {
        },
        name = "",
        pictureIcon = "",
        dungeonType = 31,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [5003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 3,
        activityDsc = "",
        preDungeonId = {
            [1] = 105209,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 5003,
        layout = {
            [1] = 512,
            [2] = 512,
            [3] = 512,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 5,
        timeFrame = {
        },
        name = "300513",
        pictureIcon = "icon/dungeon/chapter/chapter_5001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [700001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 0,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 700001,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 1001,
        timeFrame = {
        },
        name = "",
        pictureIcon = "",
        dungeonType = 14,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [5004] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 4,
        activityDsc = "",
        preDungeonId = {
            [1] = 105316,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 5004,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 5,
        timeFrame = {
        },
        name = "300514",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [100005] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 5,
        activityDsc = "",
        preDungeonId = {
            [1] = 110430,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 100005,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 10,
        timeFrame = {
        },
        name = "300515",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [100003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 3,
        activityDsc = "",
        preDungeonId = {
            [1] = 110216,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 100003,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 10,
        timeFrame = {
        },
        name = "300513",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [200001] = {
        resIcon = "icon/fuben/003.png",
        heroChapter = 0,
        unlockLevel = 10,
        order = 1,
        activityDsc = "300131",
        preDungeonId = {
        },
        reward = {
        },
        price = {
            [500002] = 50,
        },
        buyCountLimit = 2,
        openTimeType = 4,
        cycleParam = 0,
        cycleType = 1,
        countLimit = 1,
        id = 200001,
        layout = {
            [1] = 0,
        },
        dropShow = {
        },
        desc = "300421",
        dungeonChapterId = 101,
        timeFrame = {
            [1] = 2,
            [2] = 6,
            [3] = 7,
            [4] = 1,
        },
        name = "300520",
        pictureIcon = "icon/fuben/Dungeon_STG.png",
        dungeonType = 3,
        pictureText = "",
        buyCountType = 2,
        titleName = "300549",
    },
    [90004] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 4,
        activityDsc = "",
        preDungeonId = {
            [1] = 109320,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 90004,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 9,
        timeFrame = {
        },
        name = "300514",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [90001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 60,
        order = 1,
        activityDsc = "",
        preDungeonId = {
            [1] = 108646,
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 4000,
                        [570033] = 1,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c15_90_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [16] = {
                    cond = {
                        [1] = 96,
                        [2] = 0,
                    },
                    tag = "c16_96_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [17] = {
                    cond = {
                        [1] = 102,
                        [2] = 0,
                    },
                    tag = "c17_102_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 4000,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 15,
                        [500001] = 5000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 20,
                        [500001] = 5000,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c15_90_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 5000,
                        [510318] = 2,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 30,
                        [500001] = 6000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 6000,
                        [510318] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 90001,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 9,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [90002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 109106,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 90002,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 9,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [80003] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 3,
        activityDsc = "",
        preDungeonId = {
            [1] = 108215,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 80003,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 8,
        timeFrame = {
        },
        name = "300513",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [100006] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 6,
        activityDsc = "",
        preDungeonId = {
            [1] = 110537,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 100006,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 10,
        timeFrame = {
        },
        name = "300516",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [300001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 0,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 300001,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 403,
        timeFrame = {
        },
        name = "",
        pictureIcon = "",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [80001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 50,
        order = 1,
        activityDsc = "",
        preDungeonId = {
            [1] = 107539,
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 4000,
                        [570033] = 1,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c15_90_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [16] = {
                    cond = {
                        [1] = 96,
                        [2] = 0,
                    },
                    tag = "c16_96_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [18] = {
                    cond = {
                        [1] = 108,
                        [2] = 0,
                    },
                    tag = "c18_108_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [19] = {
                    cond = {
                        [1] = 114,
                        [2] = 0,
                    },
                    tag = "c19_114_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
                [20] = {
                    cond = {
                        [1] = 120,
                        [2] = 0,
                    },
                    tag = "c20_120_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 4000,
                        [570033] = 1,
                    },
                },
                [17] = {
                    cond = {
                        [1] = 102,
                        [2] = 0,
                    },
                    tag = "c17_102_0",
                    reward = {
                        [500001] = 4000,
                        [500002] = 30,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 15,
                        [500001] = 5000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 60,
                        [2] = 0,
                    },
                    tag = "c10_60_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [11] = {
                    cond = {
                        [1] = 66,
                        [2] = 0,
                    },
                    tag = "c11_66_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 20,
                        [500001] = 5000,
                    },
                },
                [12] = {
                    cond = {
                        [1] = 72,
                        [2] = 0,
                    },
                    tag = "c12_72_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [13] = {
                    cond = {
                        [1] = 78,
                        [2] = 0,
                    },
                    tag = "c13_78_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [14] = {
                    cond = {
                        [1] = 84,
                        [2] = 0,
                    },
                    tag = "c14_84_0",
                    reward = {
                        [500001] = 5000,
                        [500002] = 40,
                    },
                },
                [15] = {
                    cond = {
                        [1] = 90,
                        [2] = 0,
                    },
                    tag = "c15_90_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 5000,
                        [510316] = 2,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 30,
                        [500001] = 6000,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 6000,
                        [500002] = 50,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 6000,
                        [510316] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 80001,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 8,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [110002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 2,
        activityDsc = "",
        preDungeonId = {
            [1] = 111107,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 110002,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 11,
        timeFrame = {
        },
        name = "300512",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [110004] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 4,
        activityDsc = "",
        preDungeonId = {
            [1] = 111321,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 110004,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 11,
        timeFrame = {
        },
        name = "300514",
        pictureIcon = "icon/dungeon/chapter/chapter_7001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [300002] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 0,
        activityDsc = "",
        preDungeonId = {
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 300002,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 408,
        timeFrame = {
        },
        name = "",
        pictureIcon = "",
        dungeonType = 20,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [110005] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 0,
        order = 5,
        activityDsc = "",
        preDungeonId = {
            [1] = 111429,
        },
        reward = {
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 110005,
        layout = {
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 11,
        timeFrame = {
        },
        name = "300515",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [4001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 18,
        order = 1,
        activityDsc = "",
        preDungeonId = {
            [1] = 103316,
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 30,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 2500,
                        [570033] = 1,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 30,
                    },
                },
                [9] = {
                    cond = {
                        [1] = 54,
                        [2] = 0,
                    },
                    tag = "c9_54_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 30,
                    },
                },
                [10] = {
                    cond = {
                        [1] = 57,
                        [2] = 0,
                    },
                    tag = "c10_57_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 2500,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 15,
                        [500001] = 3500,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 40,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 20,
                        [500001] = 3500,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 40,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 45,
                        [2] = 0,
                    },
                    tag = "c8_45_0",
                    reward = {
                        [500002] = 40,
                        [510308] = 2,
                        [500001] = 3500,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 4500,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 4500,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 30,
                        [500001] = 4500,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 4500,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 27,
                        [2] = 0,
                    },
                    tag = "c5_27_0",
                    reward = {
                        [500002] = 50,
                        [510308] = 3,
                        [500001] = 4500,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 4001,
        layout = {
            [1] = 514,
            [2] = 514,
            [3] = 514,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 4,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/dungeon/chapter/chapter_5001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
    [2001] = {
        resIcon = "",
        heroChapter = 0,
        unlockLevel = 5,
        order = 1,
        activityDsc = "",
        preDungeonId = {
            [1] = 101207,
        },
        reward = {
            [1] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 1500,
                        [500002] = 30,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 1500,
                        [500002] = 30,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500001] = 1500,
                        [500002] = 30,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 1500,
                        [500002] = 30,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 1500,
                        [570033] = 1,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500001] = 1500,
                        [500002] = 30,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 1500,
                        [500002] = 30,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 48,
                        [2] = 0,
                    },
                    tag = "c8_48_0",
                    reward = {
                        [500002] = 30,
                        [500001] = 1500,
                        [570033] = 1,
                    },
                },
            },
            [2] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 40,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 40,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 10,
                        [500001] = 2500,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 40,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 30,
                        [2] = 0,
                    },
                    tag = "c5_30_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 40,
                    },
                },
                [6] = {
                    cond = {
                        [1] = 36,
                        [2] = 0,
                    },
                    tag = "c6_36_0",
                    reward = {
                        [500002] = 40,
                        [570002] = 15,
                        [500001] = 2500,
                    },
                },
                [7] = {
                    cond = {
                        [1] = 42,
                        [2] = 0,
                    },
                    tag = "c7_42_0",
                    reward = {
                        [500001] = 2500,
                        [500002] = 40,
                    },
                },
                [8] = {
                    cond = {
                        [1] = 45,
                        [2] = 0,
                    },
                    tag = "c8_45_0",
                    reward = {
                        [500002] = 40,
                        [500001] = 2500,
                        [510306] = 2,
                    },
                },
            },
            [3] = {
                [1] = {
                    cond = {
                        [1] = 6,
                        [2] = 0,
                    },
                    tag = "c1_6_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 50,
                    },
                },
                [2] = {
                    cond = {
                        [1] = 12,
                        [2] = 0,
                    },
                    tag = "c2_12_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 50,
                    },
                },
                [3] = {
                    cond = {
                        [1] = 18,
                        [2] = 0,
                    },
                    tag = "c3_18_0",
                    reward = {
                        [500002] = 50,
                        [570002] = 20,
                        [500001] = 3500,
                    },
                },
                [4] = {
                    cond = {
                        [1] = 24,
                        [2] = 0,
                    },
                    tag = "c4_24_0",
                    reward = {
                        [500001] = 3500,
                        [500002] = 50,
                    },
                },
                [5] = {
                    cond = {
                        [1] = 27,
                        [2] = 0,
                    },
                    tag = "c5_27_0",
                    reward = {
                        [500002] = 50,
                        [500001] = 3500,
                        [510306] = 3,
                    },
                },
            },
        },
        price = {
        },
        buyCountLimit = 0,
        openTimeType = 0,
        cycleParam = 0,
        cycleType = 0,
        countLimit = 0,
        id = 2001,
        layout = {
            [1] = 512,
            [2] = 512,
            [3] = 512,
        },
        dropShow = {
        },
        desc = "",
        dungeonChapterId = 2,
        timeFrame = {
        },
        name = "300511",
        pictureIcon = "icon/dungeon/chapter/chapter_3001_2.png",
        dungeonType = 2,
        pictureText = "",
        buyCountType = 0,
        titleName = "",
    },
}