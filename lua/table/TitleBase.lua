return {
    [9200000] = {
        superType = 38,
        excursion1 = {
        },
        order = 1,
        pileUp = true,
        condition = {
            hasItem = 570501,
        },
        gridMax = 1,
        effectivetime = -1,
        activeTime = 0,
        clubPush = 0,
        sellProfit = {
        },
        onceUseLimit = 0,
        triggerEnvelope = 0,
        totalMax = 1,
        convertMax = {
        },
        content = 0,
        classify = 3,
        lamp = 1101,
        titleLevel = 1,
        notable = 1325370,
        showPower = 40,
        accessdes = 1326370,
        titleType = 1,
        toplimit = 0,
        size2 = {
            [1] = 120,
            [2] = 100,
            [3] = 100,
        },
        bagType = 0,
        autoUse = false,
        star = 4,
        reward = {
        },
        displayClassify = 1,
        showEffect = "effect/title/chenghao_DAL3/chenghao_DAL3",
        excursion2 = {
            [1] = {
                [1] = 5,
                [2] = 0,
            },
            [2] = {
                [1] = 0,
                [2] = 0,
            },
            [3] = {
                [1] = 0,
                [2] = 15,
            },
        },
        useCast = {
        },
        baseAttribute = {
            [1] = 5000,
            [3] = 1111,
        },
        chatShow = false,
        title = 0,
        relatedTask = 0,
        subType = 0,
        timeDescription = 0,
        showPic = "",
        size1 = 0,
        titleStar = 3,
        dealProfit = {
        },
        probability = 0,
        quality = 1,
        id = 9200000,
    },
}