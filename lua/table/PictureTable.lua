return {
    [1] = {
        id = 1,
        res = "icon/fuben/003_1.png",
        res_en = "icon/fuben/003_1_en.png",
    },
    [2] = {
        id = 2,
        res = "ui/recharge/gifts/qiyue_s.png",
        res_en = "ui/recharge/gifts/qiyue_s_en.png",
    },
    [3] = {
        id = 3,
        res = "icon/item/goods/600018.png",
        res_en = "icon/item/goods/600018_en.png",
    },
    [4] = {
        id = 4,
        res = "icon/fuben/levelIcon/Team/2_zhihui_1.png",
        res_en = "icon/fuben/levelIcon/Team/2_zhihui_1_en.png",
    },
    [5] = {
        id = 5,
        res = "ui/activity/picture/icon52.png",
        res_en = "ui/activity/picture/icon52_en.png",
    },
    [6] = {
        id = 6,
        res = "ui/activity/chunriji/003.png",
        res_en = "ui/activity/chunriji/003_en.png",
    },
    [7] = {
        id = 7,
        res = "icon/fuben/009.png",
        res_en = "icon/fuben/009_en.png",
    },
    [8] = {
        id = 8,
        res = "ui/activity/courage/game/light/f6.png",
        res_en = "ui/activity/courage/game/light/f6_en.png",
    },
    [9] = {
        id = 9,
        res = "ui/summon/011.png",
        res_en = "ui/summon/011_en.png",
    },
    [10] = {
        id = 10,
        res = "ui/activity/picture/ad5.png",
        res_en = "ui/activity/picture/ad5_en.png",
    },
    [11] = {
        id = 11,
        res = "ui/activity/picture/ad45.png",
        res_en = "ui/activity/picture/ad45_en.png",
    },
    [12] = {
        id = 12,
        res = "ui/chat/t1.png",
        res_en = "ui/chat/t1_en.png",
    },
    [13] = {
        id = 13,
        res = "ui/Evaluate/btn_cancle.png",
        res_en = "ui/Evaluate/btn_cancle_en.png",
    },
    [14] = {
        id = 14,
        res = "ui/activity/picture/icon31.png",
        res_en = "ui/activity/picture/icon31_en.png",
    },
    [15] = {
        id = 15,
        res = "icon/fuben/003.png",
        res_en = "icon/fuben/003_en.png",
    },
    [16] = {
        id = 16,
        res = "ui/fuben/rule/003_1.png",
        res_en = "ui/fuben/rule/003_1_en.png",
    },
    [17] = {
        id = 17,
        res = "ui/fuben/1vb.png",
        res_en = "ui/fuben/1vb_en.png",
    },
    [18] = {
        id = 18,
        res = "ui/common/shoutong.png",
        res_en = "ui/common/shoutong_en.png",
    },
    [19] = {
        id = 19,
        res = "ui/activity/picture/ad37.png",
        res_en = "ui/activity/picture/ad37_en.png",
    },
    [20] = {
        id = 20,
        res = "ui/activity/picture/icon4.png",
        res_en = "ui/activity/picture/icon4_en.png",
    },
    [21] = {
        id = 21,
        res = "ui/summon/hot_spot/005.png",
        res_en = "ui/summon/hot_spot/005_en.png",
    },
    [22] = {
        id = 22,
        res = "icon/chatEmotion/019.png",
        res_en = "icon/chatEmotion/019_en.png",
    },
    [23] = {
        id = 23,
        res = "icon/fuben/Dungeon_xialamu_1.png",
        res_en = "icon/fuben/Dungeon_xialamu_1_en.png",
    },
    [24] = {
        id = 24,
        res = "icon/fuben/levelIcon/Team/8_guanghui_2.png",
        res_en = "icon/fuben/levelIcon/Team/8_guanghui_2_en.png",
    },
    [25] = {
        id = 25,
        res = "ui/common/jijie.png",
        res_en = "ui/common/jijie_en.png",
    },
    [26] = {
        id = 26,
        res = "icon/item/goods/600003.png",
        res_en = "icon/item/goods/600003_en.png",
    },
    [27] = {
        id = 27,
        res = "icon/equipType2/Binah.png",
        res_en = "icon/equipType2/Binah_en.png",
    },
    [28] = {
        id = 28,
        res = "ui/task/training/4.png",
        res_en = "ui/task/training/4_en.png",
    },
    [29] = {
        id = 29,
        res = "ui/activity/picture/ad34.png",
        res_en = "ui/activity/picture/ad34_en.png",
    },
    [30] = {
        id = 30,
        res = "icon/equipment/name/Noel.png",
        res_en = "icon/equipment/name/Noel_en.png",
    },
    [31] = {
        id = 31,
        res = "icon/equipment/name/Bastet.png",
        res_en = "icon/equipment/name/Bastet_en.png",
    },
    [32] = {
        id = 32,
        res = "icon/equipment/name/Freya.png",
        res_en = "icon/equipment/name/Freya_en.png",
    },
    [33] = {
        id = 33,
        res = "ui/activity/valentine/003.png",
        res_en = "ui/activity/valentine/003_en.png",
    },
    [34] = {
        id = 34,
        res = "ui/update/s55.png",
        res_en = "ui/update/s55_en.png",
    },
    [35] = {
        id = 35,
        res = "ui/activity/kuangsan_fuben/entrance/002.png",
        res_en = "ui/activity/kuangsan_fuben/entrance/002_en.png",
    },
    [36] = {
        id = 36,
        res = "ui/activity/christmas_dating/022.png",
        res_en = "ui/activity/christmas_dating/022_en.png",
    },
    [37] = {
        id = 37,
        res = "ui/newyear/cardGame/juxie.png",
        res_en = "ui/newyear/cardGame/juxie_en.png",
    },
    [38] = {
        id = 38,
        res = "ui/league/texun/018.png",
        res_en = "ui/league/texun/018_en.png",
    },
    [39] = {
        id = 39,
        res = "ui/activity/add_recharge/004.png",
        res_en = "ui/activity/add_recharge/004_en.png",
    },
    [40] = {
        id = 40,
        res = "ui/activity/allServer_recharge/main/006.png",
        res_en = "ui/activity/allServer_recharge/main/006_en.png",
    },
    [41] = {
        id = 41,
        res = "ui/summon/031.png",
        res_en = "ui/summon/031_en.png",
    },
    [42] = {
        id = 42,
        res = "icon/equipment/name/Shouhuzhijian.png",
        res_en = "icon/equipment/name/Shouhuzhijian_en.png",
    },
    [43] = {
        id = 43,
        res = "ui/onlineteam/capname_3.png",
        res_en = "ui/onlineteam/capname_3_en.png",
    },
    [44] = {
        id = 44,
        res = "ui/activity/activity_bg/001.png",
        res_en = "ui/activity/activity_bg/001_en.png",
    },
    [45] = {
        id = 45,
        res = "ui/oneyearChare/110101.png",
        res_en = "ui/oneyearChare/110101_en.png",
    },
    [46] = {
        id = 46,
        res = "icon/title/2.png",
        res_en = "icon/title/2_en.png",
    },
    [47] = {
        id = 47,
        res = "icon/cg/yearbirth.png",
        res_en = "icon/cg/yearbirth_en.png",
    },
    [48] = {
        id = 48,
        res = "icon/equipment/name/Heqet.png",
        res_en = "icon/equipment/name/Heqet_en.png",
    },
    [49] = {
        id = 49,
        res = "icon/equipment/name/Yiyokosmab.png",
        res_en = "icon/equipment/name/Yiyokosmab_en.png",
    },
    [50] = {
        id = 50,
        res = "icon/hero/name/554006.png",
        res_en = "icon/hero/name/554006_en.png",
    },
    [51] = {
        id = 51,
        res = "ui/fuben/rule/009.png",
        res_en = "ui/fuben/rule/009_en.png",
    },
    [52] = {
        id = 52,
        res = "ui/activity/newPlayer/newPlayerIcon.png",
        res_en = "ui/activity/newPlayer/newPlayerIcon_en.png",
    },
    [53] = {
        id = 53,
        res = "ui/teampve/entrance/d001.png",
        res_en = "ui/teampve/entrance/d001_en.png",
    },
    [54] = {
        id = 54,
        res = "icon/equipment/name/Zadkiel.png",
        res_en = "icon/equipment/name/Zadkiel_en.png",
    },
    [55] = {
        id = 55,
        res = "ui/activity/picture/ad82.png",
        res_en = "ui/activity/picture/ad82_en.png",
    },
    [56] = {
        id = 56,
        res = "ui/dating/1214Dating/main/009.png",
        res_en = "ui/dating/1214Dating/main/009_en.png",
    },
    [57] = {
        id = 57,
        res = "ui/newyear/cardGame/chunv2.png",
        res_en = "ui/newyear/cardGame/chunv2_en.png",
    },
    [58] = {
        id = 58,
        res = "ui/oneyearChare/110501.png",
        res_en = "ui/oneyearChare/110501_en.png",
    },
    [59] = {
        id = 59,
        res = "icon/title/1.png",
        res_en = "icon/title/1_en.png",
    },
    [60] = {
        id = 60,
        res = "icon/item/goods/600005.png",
        res_en = "icon/item/goods/600005_en.png",
    },
    [61] = {
        id = 61,
        res = "ui/activity/newYear/entrance/003.png",
        res_en = "ui/activity/newYear/entrance/003_en.png",
    },
    [62] = {
        id = 62,
        res = "ui/activity/courage/note/011.png",
        res_en = "ui/activity/courage/note/011_en.png",
    },
    [63] = {
        id = 63,
        res = "ui/activity/018.png",
        res_en = "ui/activity/018_en.png",
    },
    [64] = {
        id = 64,
        res = "icon/mainDating/mainName/3.png",
        res_en = "icon/mainDating/mainName/3_en.png",
    },
    [65] = {
        id = 65,
        res = "ui/activity/add_recharge/003.png",
        res_en = "ui/activity/add_recharge/003_en.png",
    },
    [66] = {
        id = 66,
        res = "ui/mainLayer/new_ui/btn_zhanling.png",
        res_en = "ui/mainLayer/new_ui/btn_zhanling_en.png",
    },
    [67] = {
        id = 67,
        res = "ui/activity/picture/ad10.png",
        res_en = "ui/activity/picture/ad10_en.png",
    },
    [68] = {
        id = 68,
        res = "ui/monthcard/004.png",
        res_en = "ui/monthcard/004_en.png",
    },
    [69] = {
        id = 69,
        res = "icon/item/gift/541049.png",
        res_en = "icon/item/gift/541049_en.png",
    },
    [70] = {
        id = 70,
        res = "icon/equipment/name/Tohka.png",
        res_en = "icon/equipment/name/Tohka_en.png",
    },
    [71] = {
        id = 71,
        res = "ui/activity/oneYear/luckyReward1/004.png",
        res_en = "ui/activity/oneYear/luckyReward1/004_en.png",
    },
    [72] = {
        id = 72,
        res = "ui/activity/oneYear/card/002.png",
        res_en = "ui/activity/oneYear/card/002_en.png",
    },
    [73] = {
        id = 73,
        res = "ui/update/s54.png",
        res_en = "ui/update/s54_en.png",
    },
    [74] = {
        id = 74,
        res = "icon/equipment/name/Himiko2.png",
        res_en = "icon/equipment/name/Himiko2_en.png",
    },
    [75] = {
        id = 75,
        res = "ui/iphoneX/roleTeach/roleTeachBuy/9.png",
        res_en = "ui/iphoneX/roleTeach/roleTeachBuy/9_en.png",
    },
    [76] = {
        id = 76,
        res = "icon/title/5.png",
        res_en = "icon/title/5_en.png",
    },
    [77] = {
        id = 77,
        res = "ui/activity/activityStyle/giftActivity/styleCur3/img_title.png",
        res_en = "ui/activity/activityStyle/giftActivity/styleCur3/img_title_en.png",
    },
    [78] = {
        id = 78,
        res = "ui/update/s8.png",
        res_en = "ui/update/s8_en.png",
    },
    [79] = {
        id = 79,
        res = "ui/mainLayer/new_ui/a6.png",
        res_en = "ui/mainLayer/new_ui/a6_en.png",
    },
    [80] = {
        id = 80,
        res = "ui/activity/activity_bg/003.png",
        res_en = "ui/activity/activity_bg/003_en.png",
    },
    [81] = {
        id = 81,
        res = "ui/activity/019.png",
        res_en = "ui/activity/019_en.png",
    },
    [82] = {
        id = 82,
        res = "ui/activity/midAutumn/entrance/003.png",
        res_en = "ui/activity/midAutumn/entrance/003_en.png",
    },
    [83] = {
        id = 83,
        res = "ui/activity/newyear_fuben/ui_003.png",
        res_en = "ui/activity/newyear_fuben/ui_003_en.png",
    },
    [84] = {
        id = 84,
        res = "ui/activity/picture/icon84.png",
        res_en = "ui/activity/picture/icon84_en.png",
    },
    [85] = {
        id = 85,
        res = "ui/playerInfo/new/13.png",
        res_en = "ui/playerInfo/new/13_en.png",
    },
    [86] = {
        id = 86,
        res = "icon/equipment/name/Yujingzhifeng.png",
        res_en = "icon/equipment/name/Yujingzhifeng_en.png",
    },
    [87] = {
        id = 87,
        res = "ui/update/s5.png",
        res_en = "ui/update/s5_en.png",
    },
    [88] = {
        id = 88,
        res = "ui/activity/summer/002.png",
        res_en = "ui/activity/summer/002_en.png",
    },
    [89] = {
        id = 89,
        res = "ui/dating/unLock/2.png",
        res_en = "ui/dating/unLock/2_en.png",
    },
    [90] = {
        id = 90,
        res = "ui/fairy_angle/new_79.png",
        res_en = "ui/fairy_angle/new_79_en.png",
    },
    [91] = {
        id = 91,
        res = "icon/equipment/suit/modaogongming.png",
        res_en = "icon/equipment/suit/modaogongming_en.png",
    },
    [92] = {
        id = 92,
        res = "icon/chatBubble/009.png",
        res_en = "icon/chatBubble/009_en.png",
    },
    [93] = {
        id = 93,
        res = "icon/item/goods/600008.png",
        res_en = "icon/item/goods/600008_en.png",
    },
    [94] = {
        id = 94,
        res = "icon/equipment/name/Camila.png",
        res_en = "icon/equipment/name/Camila_en.png",
    },
    [95] = {
        id = 95,
        res = "ui/newyear/cardGame/juxie2.png",
        res_en = "ui/newyear/cardGame/juxie2_en.png",
    },
    [96] = {
        id = 96,
        res = "ui/newyear/cardGame/jinniu2.png",
        res_en = "ui/newyear/cardGame/jinniu2_en.png",
    },
    [97] = {
        id = 97,
        res = "ui/newyear/cardGame/sheshou.png",
        res_en = "ui/newyear/cardGame/sheshou_en.png",
    },
    [98] = {
        id = 98,
        res = "ui/task/training/ui_011.png",
        res_en = "ui/task/training/ui_011_en.png",
    },
    [99] = {
        id = 99,
        res = "ui/activity/clothese_summon/003.png",
        res_en = "ui/activity/clothese_summon/003_en.png",
    },
    [100] = {
        id = 100,
        res = "ui/activity/logo/logo58.png",
        res_en = "ui/activity/logo/logo58_en.png",
    },
    [101] = {
        id = 101,
        res = "ui/activity/picture/bg1.png",
        res_en = "ui/activity/picture/bg1_en.png",
    },
    [102] = {
        id = 102,
        res = "icon/hero/frame/11312.png",
        res_en = "icon/hero/frame/11312_en.png",
    },
    [103] = {
        id = 103,
        res = "ui/function/fight.png",
        res_en = "ui/function/fight_en.png",
    },
    [104] = {
        id = 104,
        res = "ui/newyear/cardGame/mojie.png",
        res_en = "ui/newyear/cardGame/mojie_en.png",
    },
    [105] = {
        id = 105,
        res = "ui/summon/045.png",
        res_en = "ui/summon/045_en.png",
    },
    [106] = {
        id = 106,
        res = "icon/equipment/name/Tangguojingling.png",
        res_en = "icon/equipment/name/Tangguojingling_en.png",
    },
    [107] = {
        id = 107,
        res = "ui/activity/newYear/entrance/001.png",
        res_en = "ui/activity/newYear/entrance/001_en.png",
    },
    [108] = {
        id = 108,
        res = "ui/teampve/huntingInvitation/003.png",
        res_en = "ui/teampve/huntingInvitation/003_en.png",
    },
    [109] = {
        id = 109,
        res = "ui/makefood/make/019.png",
        res_en = "ui/makefood/make/019_en.png",
    },
    [110] = {
        id = 110,
        res = "icon/equipment/name/Relish2.png",
        res_en = "icon/equipment/name/Relish2_en.png",
    },
    [111] = {
        id = 111,
        res = "ui/oneyearChare/110901.png",
        res_en = "ui/oneyearChare/110901_en.png",
    },
    [112] = {
        id = 112,
        res = "ui/dalmap/event/wenan_1.png",
        res_en = "ui/dalmap/event/wenan_1_en.png",
    },
    [113] = {
        id = 113,
        res = "ui/activity/assist/kuangsan/sign_011.png",
        res_en = "ui/activity/assist/kuangsan/sign_011_en.png",
    },
    [114] = {
        id = 114,
        res = "ui/activity/picture/icon126.png",
        res_en = "ui/activity/picture/icon126_en.png",
    },
    [115] = {
        id = 115,
        res = "ui/monthcardNew1/bg3.png",
        res_en = "ui/monthcardNew1/bg3_en.png",
    },
    [116] = {
        id = 116,
        res = "ui/activity/kuangsan_card/enter/05.png",
        res_en = "ui/activity/kuangsan_card/enter/05_en.png",
    },
    [117] = {
        id = 117,
        res = "ui/summon/027.png",
        res_en = "ui/summon/027_en.png",
    },
    [118] = {
        id = 118,
        res = "icon/equipment/name/Beelzebub.png",
        res_en = "icon/equipment/name/Beelzebub_en.png",
    },
    [119] = {
        id = 119,
        res = "ui/activity/picture/ad3.png",
        res_en = "ui/activity/picture/ad3_en.png",
    },
    [120] = {
        id = 120,
        res = "ui/task/training/ui_025.png",
        res_en = "ui/task/training/ui_025_en.png",
    },
    [121] = {
        id = 121,
        res = "ui/task/01/banner.png",
        res_en = "ui/task/01/banner_en.png",
    },
    [122] = {
        id = 122,
        res = "ui/dispatch/ui_099.png",
        res_en = "ui/dispatch/ui_099_en.png",
    },
    [123] = {
        id = 123,
        res = "ui/simulation_trial5/003.png",
        res_en = "ui/simulation_trial5/003_en.png",
    },
    [124] = {
        id = 124,
        res = "ui/summon/032.png",
        res_en = "ui/summon/032_en.png",
    },
    [125] = {
        id = 125,
        res = "ui/activity/picture/icon106.png",
        res_en = "ui/activity/picture/icon106_en.png",
    },
    [126] = {
        id = 126,
        res = "ui/fuben/endless/new_016.png",
        res_en = "ui/fuben/endless/new_016_en.png",
    },
    [127] = {
        id = 127,
        res = "icon/item/goods/570511.png",
        res_en = "icon/item/goods/570511_en.png",
    },
    [128] = {
        id = 128,
        res = "ui/activity/courage/enter/008.png",
        res_en = "ui/activity/courage/enter/008_en.png",
    },
    [129] = {
        id = 129,
        res = "ui/simulation_trial/bg3.png",
        res_en = "ui/simulation_trial/bg3_en.png",
    },
    [130] = {
        id = 130,
        res = "ui/activity/sevenEx/014.png",
        res_en = "ui/activity/sevenEx/014_en.png",
    },
    [131] = {
        id = 131,
        res = "ui/role/newScene/icon_5.png",
        res_en = "ui/role/newScene/icon_5_en.png",
    },
    [132] = {
        id = 132,
        res = "icon/equipment/name/Geouphin.png",
        res_en = "icon/equipment/name/Geouphin_en.png",
    },
    [133] = {
        id = 133,
        res = "ui/activity/017.png",
        res_en = "ui/activity/017_en.png",
    },
    [134] = {
        id = 134,
        res = "ui/iphoneX/roleTeach/roleTeachBuy/005.png",
        res_en = "ui/iphoneX/roleTeach/roleTeachBuy/005_en.png",
    },
    [135] = {
        id = 135,
        res = "ui/oneyearChare/110601.png",
        res_en = "ui/oneyearChare/110601_en.png",
    },
    [136] = {
        id = 136,
        res = "ui/mainLayer/new_ui/btn_battle.png",
        res_en = "ui/mainLayer/new_ui/btn_battle_en.png",
    },
    [137] = {
        id = 137,
        res = "icon/equipment/name/Antoinette.png",
        res_en = "icon/equipment/name/Antoinette_en.png",
    },
    [138] = {
        id = 138,
        res = "ui/activity/oneYear/luckyReward/vote/cj007.png",
        res_en = "ui/activity/oneYear/luckyReward/vote/cj007_en.png",
    },
    [139] = {
        id = 139,
        res = "ui/recharge/diban.png",
        res_en = "ui/recharge/diban_en.png",
    },
    [140] = {
        id = 140,
        res = "ui/recharge/new_ui/01.png",
        res_en = "ui/recharge/new_ui/01_en.png",
    },
    [141] = {
        id = 141,
        res = "icon/hero/frame/10132.png",
        res_en = "icon/hero/frame/10132_en.png",
    },
    [142] = {
        id = 142,
        res = "ui/summon/9000.png",
        res_en = "ui/summon/9000_en.png",
    },
    [143] = {
        id = 143,
        res = "icon/equipment/suit/aixinyiji.png",
        res_en = "icon/equipment/suit/aixinyiji_en.png",
    },
    [144] = {
        id = 144,
        res = "icon/equipment/suit/shenglingbodong.png",
        res_en = "icon/equipment/suit/shenglingbodong_en.png",
    },
    [145] = {
        id = 145,
        res = "icon/equipment/name/Shengdanzhezhi.png",
        res_en = "icon/equipment/name/Shengdanzhezhi_en.png",
    },
    [146] = {
        id = 146,
        res = "ui/activity/oneYear/luckyReward1/019.png",
        res_en = "ui/activity/oneYear/luckyReward1/019_en.png",
    },
    [147] = {
        id = 147,
        res = "ui/activity/picture/ad71.png",
        res_en = "ui/activity/picture/ad71_en.png",
    },
    [148] = {
        id = 148,
        res = "icon/title/4.png",
        res_en = "icon/title/4_en.png",
    },
    [149] = {
        id = 149,
        res = "icon/fuben/levelIcon/Team/9_jichu.png",
        res_en = "icon/fuben/levelIcon/Team/9_jichu_en.png",
    },
    [150] = {
        id = 150,
        res = "icon/equipment/suit/zhongxiazuozhan.png",
        res_en = "icon/equipment/suit/zhongxiazuozhan_en.png",
    },
    [151] = {
        id = 151,
        res = "ui/iphoneX/roleTeach/roleTeachBuy/004.png",
        res_en = "ui/iphoneX/roleTeach/roleTeachBuy/004_en.png",
    },
    [152] = {
        id = 152,
        res = "icon/equipment/name/Luna.png",
        res_en = "icon/equipment/name/Luna_en.png",
    },
    [153] = {
        id = 153,
        res = "ui/recharge/growFundview/1.png",
        res_en = "ui/recharge/growFundview/1_en.png",
    },
    [154] = {
        id = 154,
        res = "icon/item/goods/570208.png",
        res_en = "icon/item/goods/570208_en.png",
    },
    [155] = {
        id = 155,
        res = "ui/activity/duanwu/entrance/logo.png",
        res_en = "ui/activity/duanwu/entrance/logo_en.png",
    },
    [156] = {
        id = 156,
        res = "icon/item/goods/600011.png",
        res_en = "icon/item/goods/600011_en.png",
    },
    [157] = {
        id = 157,
        res = "ui/activity/picture/icon27.png",
        res_en = "ui/activity/picture/icon27_en.png",
    },
    [158] = {
        id = 158,
        res = "ui/activity/christmas_pre/b1.png",
        res_en = "ui/activity/christmas_pre/b1_en.png",
    },
    [159] = {
        id = 159,
        res = "ui/activity/preheat/main.png",
        res_en = "ui/activity/preheat/main_en.png",
    },
    [160] = {
        id = 160,
        res = "ui/activity/anniversary/add_recharge_01.png",
        res_en = "ui/activity/anniversary/add_recharge_01_en.png",
    },
    [161] = {
        id = 161,
        res = "icon/fuben/levelIcon/Team/2_zhihui_2.png",
        res_en = "icon/fuben/levelIcon/Team/2_zhihui_2_en.png",
    },
    [162] = {
        id = 162,
        res = "ui/update/s1.png",
        res_en = "ui/update/s1_en.png",
    },
    [163] = {
        id = 163,
        res = "ui/activity/picture/icon63.png",
        res_en = "ui/activity/picture/icon63_en.png",
    },
    [164] = {
        id = 164,
        res = "ui/activity/picture/icon7.png",
        res_en = "ui/activity/picture/icon7_en.png",
    },
    [165] = {
        id = 165,
        res = "ui/activity/020.png",
        res_en = "ui/activity/020_en.png",
    },
    [166] = {
        id = 166,
        res = "icon/item/spring/008.png",
        res_en = "icon/item/spring/008_en.png",
    },
    [167] = {
        id = 167,
        res = "ui/mainLayer/new_ui/bg_oneyear.png",
        res_en = "ui/mainLayer/new_ui/bg_oneyear_en.png",
    },
    [168] = {
        id = 168,
        res = "icon/equipment/suit/shenglilihua.png",
        res_en = "icon/equipment/suit/shenglilihua_en.png",
    },
    [169] = {
        id = 169,
        res = "icon/equipment/suit/ganzizhixin.png",
        res_en = "icon/equipment/suit/ganzizhixin_en.png",
    },
    [170] = {
        id = 170,
        res = "icon/fuben/levelIcon/Team/6_shengli_2.png",
        res_en = "icon/fuben/levelIcon/Team/6_shengli_2_en.png",
    },
    [171] = {
        id = 171,
        res = "icon/chatBubble/chatUseRes/007.png",
        res_en = "icon/chatBubble/chatUseRes/007_en.png",
    },
    [172] = {
        id = 172,
        res = "ui/playerInfo/new/6796.png",
        res_en = "ui/playerInfo/new/6796_en.png",
    },
    [173] = {
        id = 173,
        res = "icon/equipment/suit/chunbaizhixin.png",
        res_en = "icon/equipment/suit/chunbaizhixin_en.png",
    },
    [174] = {
        id = 174,
        res = "icon/item/goods/501019.png",
        res_en = "icon/item/goods/501019_en.png",
    },
    [175] = {
        id = 175,
        res = "icon/item/goods/570242.png",
        res_en = "icon/item/goods/570242_en.png",
    },
    [176] = {
        id = 176,
        res = "icon/item/goods/570518.png",
        res_en = "icon/item/goods/570518_en.png",
    },
    [177] = {
        id = 177,
        res = "ui/mainLayer/new_ui/a1.png",
        res_en = "ui/mainLayer/new_ui/a1_en.png",
    },
    [178] = {
        id = 178,
        res = "ui/onlineteam/capname_2.png",
        res_en = "ui/onlineteam/capname_2_en.png",
    },
    [179] = {
        id = 179,
        res = "icon/equipment/name/Meiguiwuzhuang.png",
        res_en = "icon/equipment/name/Meiguiwuzhuang_en.png",
    },
    [180] = {
        id = 180,
        res = "ui/activity/picture/ad110.png",
        res_en = "ui/activity/picture/ad110_en.png",
    },
    [181] = {
        id = 181,
        res = "ui/activity/newguy_summon/003.png",
        res_en = "ui/activity/newguy_summon/003_en.png",
    },
    [182] = {
        id = 182,
        res = "icon/equipment/name/Shengdanshixiang.png",
        res_en = "icon/equipment/name/Shengdanshixiang_en.png",
    },
    [183] = {
        id = 183,
        res = "ui/mainLayer3/c21.png",
        res_en = "ui/mainLayer3/c21_en.png",
    },
    [184] = {
        id = 184,
        res = "ui/activity/picture/ad17.png",
        res_en = "ui/activity/picture/ad17_en.png",
    },
    [185] = {
        id = 185,
        res = "icon/equipment/suit/shengheizhixin.png",
        res_en = "icon/equipment/suit/shengheizhixin_en.png",
    },
    [186] = {
        id = 186,
        res = "icon/equipment/name/Lunhuizhiying.png",
        res_en = "icon/equipment/name/Lunhuizhiying_en.png",
    },
    [187] = {
        id = 187,
        res = "ui/summon/exchange/009.png",
        res_en = "ui/summon/exchange/009_en.png",
    },
    [188] = {
        id = 188,
        res = "icon/equipType2/Yesod.png",
        res_en = "icon/equipType2/Yesod_en.png",
    },
    [189] = {
        id = 189,
        res = "icon/item/spring/013.png",
        res_en = "icon/item/spring/013_en.png",
    },
    [190] = {
        id = 190,
        res = "ui/summon/042.png",
        res_en = "ui/summon/042_en.png",
    },
    [191] = {
        id = 191,
        res = "ui/activity/courage/note/008.png",
        res_en = "ui/activity/courage/note/008_en.png",
    },
    [192] = {
        id = 192,
        res = "icon/fuben/levelIcon/Team/3_lijie.png",
        res_en = "icon/fuben/levelIcon/Team/3_lijie_en.png",
    },
    [193] = {
        id = 193,
        res = "ui/activity/courage/game/light/bg1.png",
        res_en = "ui/activity/courage/game/light/bg1_en.png",
    },
    [194] = {
        id = 194,
        res = "icon/item/spring/007.png",
        res_en = "icon/item/spring/007_en.png",
    },
    [195] = {
        id = 195,
        res = "icon/equipment/suit/leitingcaijue.png",
        res_en = "icon/equipment/suit/leitingcaijue_en.png",
    },
    [196] = {
        id = 196,
        res = "ui/task/01/3.png",
        res_en = "ui/task/01/3_en.png",
    },
    [197] = {
        id = 197,
        res = "icon/item/spring/011.png",
        res_en = "icon/item/spring/011_en.png",
    },
    [198] = {
        id = 198,
        res = "ui/termBegin/main/003.png",
        res_en = "ui/termBegin/main/003_en.png",
    },
    [199] = {
        id = 199,
        res = "ui/oneyearChare/112001.png",
        res_en = "ui/oneyearChare/112001_en.png",
    },
    [200] = {
        id = 200,
        res = "icon/equipment/name/Huhuanzhijian.png",
        res_en = "icon/equipment/name/Huhuanzhijian_en.png",
    },
    [201] = {
        id = 201,
        res = "ui/activity/picture/ad49.png",
        res_en = "ui/activity/picture/ad49_en.png",
    },
    [202] = {
        id = 202,
        res = "ui/activity/picture/ad18.png",
        res_en = "ui/activity/picture/ad18_en.png",
    },
    [203] = {
        id = 203,
        res = "ui/fuli/seven_sign/002.png",
        res_en = "ui/fuli/seven_sign/002_en.png",
    },
    [204] = {
        id = 204,
        res = "ui/fuben/flight_mode.png",
        res_en = "ui/fuben/flight_mode_en.png",
    },
    [205] = {
        id = 205,
        res = "ui/kabalatree/skip1.png",
        res_en = "ui/kabalatree/skip1_en.png",
    },
    [206] = {
        id = 206,
        res = "ui/activity/summonActivity/cover2.png",
        res_en = "ui/activity/summonActivity/cover2_en.png",
    },
    [207] = {
        id = 207,
        res = "icon/pokedexActivity/010.png",
        res_en = "icon/pokedexActivity/010_en.png",
    },
    [208] = {
        id = 208,
        res = "ui/activity/courage/map/BG.png",
        res_en = "ui/activity/courage/map/BG_en.png",
    },
    [209] = {
        id = 209,
        res = "icon/pokedexActivity/012.png",
        res_en = "icon/pokedexActivity/012_en.png",
    },
    [210] = {
        id = 210,
        res = "ui/update/s10.png",
        res_en = "ui/update/s10_en.png",
    },
    [211] = {
        id = 211,
        res = "ui/activity/welfareSign/003.png",
        res_en = "ui/activity/welfareSign/003_en.png",
    },
    [212] = {
        id = 212,
        res = "ui/mail/mail_02.png",
        res_en = "ui/mail/mail_02_en.png",
    },
    [213] = {
        id = 213,
        res = "ui/activity/courage/game/light/f5.png",
        res_en = "ui/activity/courage/game/light/f5_en.png",
    },
    [214] = {
        id = 214,
        res = "ui/activity/recharge/LXCZ_bg1.png",
        res_en = "ui/activity/recharge/LXCZ_bg1_en.png",
    },
    [215] = {
        id = 215,
        res = "ui/activity/preheat/btn.png",
        res_en = "ui/activity/preheat/btn_en.png",
    },
    [216] = {
        id = 216,
        res = "ui/monthcardNew1/010x.png",
        res_en = "ui/monthcardNew1/010x_en.png",
    },
    [217] = {
        id = 217,
        res = "ui/activity/activityStyle/wefareSignActivity/styleCur/bg1.png",
        res_en = "ui/activity/activityStyle/wefareSignActivity/styleCur/bg1_en.png",
    },
    [218] = {
        id = 218,
        res = "ui/activity/coffee/entrance/logo.png",
        res_en = "ui/activity/coffee/entrance/logo_en.png",
    },
    [219] = {
        id = 219,
        res = "ui/activity/Halloween/030.png",
        res_en = "ui/activity/Halloween/030_en.png",
    },
    [220] = {
        id = 220,
        res = "icon/equipment/name/Flora.png",
        res_en = "icon/equipment/name/Flora_en.png",
    },
    [221] = {
        id = 221,
        res = "ui/activity/assist/032.png",
        res_en = "ui/activity/assist/032_en.png",
    },
    [222] = {
        id = 222,
        res = "ui/dispatch/ui_0102.png",
        res_en = "ui/dispatch/ui_0102_en.png",
    },
    [223] = {
        id = 223,
        res = "ui/recharge/gifts/xianshi_bg.png",
        res_en = "ui/recharge/gifts/xianshi_bg_en.png",
    },
    [224] = {
        id = 224,
        res = "ui/activity/activity_bg/034.png",
        res_en = "ui/activity/activity_bg/034_en.png",
    },
    [225] = {
        id = 225,
        res = "ui/update/s7.png",
        res_en = "ui/update/s7_en.png",
    },
    [226] = {
        id = 226,
        res = "icon/equipment/name/Heianjunwang.png",
        res_en = "icon/equipment/name/Heianjunwang_en.png",
    },
    [227] = {
        id = 227,
        res = "ui/simulation_trial2/bg33.png",
        res_en = "ui/simulation_trial2/bg33_en.png",
    },
    [228] = {
        id = 228,
        res = "icon/dafuweng/500051.png",
        res_en = "icon/dafuweng/500051_en.png",
    },
    [229] = {
        id = 229,
        res = "ui/activity/activityScale9/got_get_small.png",
        res_en = "ui/activity/activityScale9/got_get_small_en.png",
    },
    [230] = {
        id = 230,
        res = "icon/chatBubble/chatUseRes/008.png",
        res_en = "icon/chatBubble/chatUseRes/008_en.png",
    },
    [231] = {
        id = 231,
        res = "ui/mainLayer/new_ui/9.png",
        res_en = "ui/mainLayer/new_ui/9_en.png",
    },
    [232] = {
        id = 232,
        res = "ui/summon/030.png",
        res_en = "ui/summon/030_en.png",
    },
    [233] = {
        id = 233,
        res = "ui/oneyearChare/1.png",
        res_en = "ui/oneyearChare/1_en.png",
    },
    [234] = {
        id = 234,
        res = "ui/activity/picture/icon8.png",
        res_en = "ui/activity/picture/icon8_en.png",
    },
    [235] = {
        id = 235,
        res = "ui/summon/elf_contract/010.png",
        res_en = "ui/summon/elf_contract/010_en.png",
    },
    [236] = {
        id = 236,
        res = "ui/dalmap/event/wenan_4.png",
        res_en = "ui/dalmap/event/wenan_4_en.png",
    },
    [237] = {
        id = 237,
        res = "icon/equipment/name/Mut.png",
        res_en = "icon/equipment/name/Mut_en.png",
    },
    [238] = {
        id = 238,
        res = "ui/activity/picture/ad126.png",
        res_en = "ui/activity/picture/ad126_en.png",
    },
    [239] = {
        id = 239,
        res = "ui/activity/picture/ad65.png",
        res_en = "ui/activity/picture/ad65_en.png",
    },
    [240] = {
        id = 240,
        res = "icon/equipment/name/Valkyrie.png",
        res_en = "icon/equipment/name/Valkyrie_en.png",
    },
    [241] = {
        id = 241,
        res = "icon/chatEmotion/010.png",
        res_en = "icon/chatEmotion/010_en.png",
    },
    [242] = {
        id = 242,
        res = "icon/equipment/name/Qianxinzhijian.png",
        res_en = "icon/equipment/name/Qianxinzhijian_en.png",
    },
    [243] = {
        id = 243,
        res = "icon/equipment/name/Satan.png",
        res_en = "icon/equipment/name/Satan_en.png",
    },
    [244] = {
        id = 244,
        res = "ui/activity/picture/icon107.png",
        res_en = "ui/activity/picture/icon107_en.png",
    },
    [245] = {
        id = 245,
        res = "icon/equipment/name/Maria.png",
        res_en = "icon/equipment/name/Maria_en.png",
    },
    [246] = {
        id = 246,
        res = "ui/summon/elf_contract/012.png",
        res_en = "ui/summon/elf_contract/012_en.png",
    },
    [247] = {
        id = 247,
        res = "ui/Halloween/Main/002.png",
        res_en = "ui/Halloween/Main/002_en.png",
    },
    [248] = {
        id = 248,
        res = "icon/equipment/name/Nvpuzhezhi.png",
        res_en = "icon/equipment/name/Nvpuzhezhi_en.png",
    },
    [249] = {
        id = 249,
        res = "ui/chat/003.png",
        res_en = "ui/chat/003_en.png",
    },
    [250] = {
        id = 250,
        res = "ui/activity/picture/ad70.png",
        res_en = "ui/activity/picture/ad70_en.png",
    },
    [251] = {
        id = 251,
        res = "ui/activity/summonActivity/cover1.png",
        res_en = "ui/activity/summonActivity/cover1_en.png",
    },
    [252] = {
        id = 252,
        res = "icon/hero/name/553103.png",
        res_en = "icon/hero/name/553103_en.png",
    },
    [253] = {
        id = 253,
        res = "ui/activity/picture/icon6.png",
        res_en = "ui/activity/picture/icon6_en.png",
    },
    [254] = {
        id = 254,
        res = "ui/activity/picture/ad16.png",
        res_en = "ui/activity/picture/ad16_en.png",
    },
    [255] = {
        id = 255,
        res = "ui/activity/picture/icon36.png",
        res_en = "ui/activity/picture/icon36_en.png",
    },
    [256] = {
        id = 256,
        res = "ui/newyear/cardGame/shizi2.png",
        res_en = "ui/newyear/cardGame/shizi2_en.png",
    },
    [257] = {
        id = 257,
        res = "icon/title/7.png",
        res_en = "icon/title/7_en.png",
    },
    [258] = {
        id = 258,
        res = "icon/equipType2/Netzach.png",
        res_en = "icon/equipType2/Netzach_en.png",
    },
    [259] = {
        id = 259,
        res = "ui/update/s9.png",
        res_en = "ui/update/s9_en.png",
    },
    [260] = {
        id = 260,
        res = "ui/oneyearChare/110701.png",
        res_en = "ui/oneyearChare/110701_en.png",
    },
    [261] = {
        id = 261,
        res = "ui/activity/picture/icon111.png",
        res_en = "ui/activity/picture/icon111_en.png",
    },
    [262] = {
        id = 262,
        res = "ui/dalmap/event/card.png",
        res_en = "ui/dalmap/event/card_en.png",
    },
    [263] = {
        id = 263,
        res = "icon/item/goods/600016.png",
        res_en = "icon/item/goods/600016_en.png",
    },
    [264] = {
        id = 264,
        res = "ui/osd/main/026.png",
        res_en = "ui/osd/main/026_en.png",
    },
    [265] = {
        id = 265,
        res = "icon/equipment/name/Relish.png",
        res_en = "icon/equipment/name/Relish_en.png",
    },
    [266] = {
        id = 266,
        res = "ui/activity/coffee/main/001.png",
        res_en = "ui/activity/coffee/main/001_en.png",
    },
    [267] = {
        id = 267,
        res = "ui/newyear/cardGame/tianchen2.png",
        res_en = "ui/newyear/cardGame/tianchen2_en.png",
    },
    [268] = {
        id = 268,
        res = "ui/activity/picture/icon9.png",
        res_en = "ui/activity/picture/icon9_en.png",
    },
    [269] = {
        id = 269,
        res = "icon/equipment/name/Elisa.png",
        res_en = "icon/equipment/name/Elisa_en.png",
    },
    [270] = {
        id = 270,
        res = "ui/activity/oneYear/card/003.png",
        res_en = "ui/activity/oneYear/card/003_en.png",
    },
    [271] = {
        id = 271,
        res = "icon/chatEmotion/013.png",
        res_en = "icon/chatEmotion/013_en.png",
    },
    [272] = {
        id = 272,
        res = "ui/activity/picture/icon26.png",
        res_en = "ui/activity/picture/icon26_en.png",
    },
    [273] = {
        id = 273,
        res = "ui/fuben/bump_mode.png",
        res_en = "ui/fuben/bump_mode_en.png",
    },
    [274] = {
        id = 274,
        res = "ui/activity/duanwu/entrance/1.png",
        res_en = "ui/activity/duanwu/entrance/1_en.png",
    },
    [275] = {
        id = 275,
        res = "icon/equipment/suit/shenshengkeyin.png",
        res_en = "icon/equipment/suit/shenshengkeyin_en.png",
    },
    [276] = {
        id = 276,
        res = "icon/fuben/levelIcon/Team/4_renai_2.png",
        res_en = "icon/fuben/levelIcon/Team/4_renai_2_en.png",
    },
    [277] = {
        id = 277,
        res = "ui/activity/picture/ad149.png",
        res_en = "ui/activity/picture/ad149_en.png",
    },
    [278] = {
        id = 278,
        res = "icon/equipment/name/Maria2.png",
        res_en = "icon/equipment/name/Maria2_en.png",
    },
    [279] = {
        id = 279,
        res = "ui/dalmap/dalmap/001.png",
        res_en = "ui/dalmap/dalmap/001_en.png",
    },
    [280] = {
        id = 280,
        res = "icon/item/goods/600017.png",
        res_en = "icon/item/goods/600017_en.png",
    },
    [281] = {
        id = 281,
        res = "ui/mainLayer/new_ui/btn_dating.png",
        res_en = "ui/mainLayer/new_ui/btn_dating_en.png",
    },
    [282] = {
        id = 282,
        res = "icon/equipment/name/Rophocale.png",
        res_en = "icon/equipment/name/Rophocale_en.png",
    },
    [283] = {
        id = 283,
        res = "ui/newyear/21.png",
        res_en = "ui/newyear/21_en.png",
    },
    [284] = {
        id = 284,
        res = "ui/summon/039.png",
        res_en = "ui/summon/039_en.png",
    },
    [285] = {
        id = 285,
        res = "ui/activity/activityStyle/serverRechargeActivity/styleCur/bg.png",
        res_en = "ui/activity/activityStyle/serverRechargeActivity/styleCur/bg_en.png",
    },
    [286] = {
        id = 286,
        res = "ui/oneyearChare/111301.png",
        res_en = "ui/oneyearChare/111301_en.png",
    },
    [287] = {
        id = 287,
        res = "ui/recharge/act.png",
        res_en = "ui/recharge/act_en.png",
    },
    [288] = {
        id = 288,
        res = "icon/fuben/levelIcon/Team/8_guanghui_1.png",
        res_en = "icon/fuben/levelIcon/Team/8_guanghui_1_en.png",
    },
    [289] = {
        id = 289,
        res = "icon/equipment/name/Amalthea.png",
        res_en = "icon/equipment/name/Amalthea_en.png",
    },
    [290] = {
        id = 290,
        res = "ui/onlineteam/btn_jijie.png",
        res_en = "ui/onlineteam/btn_jijie_en.png",
    },
    [291] = {
        id = 291,
        res = "icon/equipment/name/Heb.png",
        res_en = "icon/equipment/name/Heb_en.png",
    },
    [292] = {
        id = 292,
        res = "ui/activity/oneYear/card/pop/001.png",
        res_en = "ui/activity/oneYear/card/pop/001_en.png",
    },
    [293] = {
        id = 293,
        res = "icon/equipment/name/Raphael2.png",
        res_en = "icon/equipment/name/Raphael2_en.png",
    },
    [294] = {
        id = 294,
        res = "ui/activity/activity_bg/035.png",
        res_en = "ui/activity/activity_bg/035_en.png",
    },
    [295] = {
        id = 295,
        res = "ui/activity/duanwu/achievement/004.png",
        res_en = "ui/activity/duanwu/achievement/004_en.png",
    },
    [296] = {
        id = 296,
        res = "ui/activity/picture/ad53.png",
        res_en = "ui/activity/picture/ad53_en.png",
    },
    [297] = {
        id = 297,
        res = "ui/activity/picture/ad23.png",
        res_en = "ui/activity/picture/ad23_en.png",
    },
    [298] = {
        id = 298,
        res = "ui/activity/picture/ad57.png",
        res_en = "ui/activity/picture/ad57_en.png",
    },
    [299] = {
        id = 299,
        res = "icon/equipment/suit/jiushu.png",
        res_en = "icon/equipment/suit/jiushu_en.png",
    },
    [300] = {
        id = 300,
        res = "ui/activity/activityScale9/btn.png",
        res_en = "ui/activity/activityScale9/btn_en.png",
    },
    [301] = {
        id = 301,
        res = "ui/iphoneX/roleTeach/roleTeachBuy/8.png",
        res_en = "ui/iphoneX/roleTeach/roleTeachBuy/8_en.png",
    },
    [302] = {
        id = 302,
        res = "ui/oneyearChare/9.png",
        res_en = "ui/oneyearChare/9_en.png",
    },
    [303] = {
        id = 303,
        res = "ui/activity/activityStyle/giftActivity/style2/img_title.png",
        res_en = "ui/activity/activityStyle/giftActivity/style2/img_title_en.png",
    },
    [304] = {
        id = 304,
        res = "ui/activity/courage/game/light/f2.png",
        res_en = "ui/activity/courage/game/light/f2_en.png",
    },
    [305] = {
        id = 305,
        res = "ui/activity/courage/note/009.png",
        res_en = "ui/activity/courage/note/009_en.png",
    },
    [306] = {
        id = 306,
        res = "ui/activity/picture/ad11.png",
        res_en = "ui/activity/picture/ad11_en.png",
    },
    [307] = {
        id = 307,
        res = "ui/fairy_angle/3.png",
        res_en = "ui/fairy_angle/3_en.png",
    },
    [308] = {
        id = 308,
        res = "ui/summon/046.png",
        res_en = "ui/summon/046_en.png",
    },
    [309] = {
        id = 309,
        res = "ui/playerInfo/avatar/TXBK_1.jpg",
        res_en = "ui/playerInfo/avatar/TXBK_1_en.jpg",
    },
    [310] = {
        id = 310,
        res = "ui/activity/picture/ad12.png",
        res_en = "ui/activity/picture/ad12_en.png",
    },
    [311] = {
        id = 311,
        res = "icon/equipment/name/Jibanshiyue.png",
        res_en = "icon/equipment/name/Jibanshiyue_en.png",
    },
    [312] = {
        id = 312,
        res = "ui/fuben/rule/011.png",
        res_en = "ui/fuben/rule/011_en.png",
    },
    [313] = {
        id = 313,
        res = "ui/activity/oneYear/card/005.png",
        res_en = "ui/activity/oneYear/card/005_en.png",
    },
    [314] = {
        id = 314,
        res = "ui/fuben/rule/011_1.png",
        res_en = "ui/fuben/rule/011_1_en.png",
    },
    [315] = {
        id = 315,
        res = "ui/summon/hot_spot/003.png",
        res_en = "ui/summon/hot_spot/003_en.png",
    },
    [316] = {
        id = 316,
        res = "ui/activity/picture/icon83.png",
        res_en = "ui/activity/picture/icon83_en.png",
    },
    [317] = {
        id = 317,
        res = "icon/equipment/name/Himiko.png",
        res_en = "icon/equipment/name/Himiko_en.png",
    },
    [318] = {
        id = 318,
        res = "ui/activity/picture/ad56.png",
        res_en = "ui/activity/picture/ad56_en.png",
    },
    [319] = {
        id = 319,
        res = "ui/fuben/halloween/title_1.png",
        res_en = "ui/fuben/halloween/title_1_en.png",
    },
    [320] = {
        id = 320,
        res = "ui/summon/024.png",
        res_en = "ui/summon/024_en.png",
    },
    [321] = {
        id = 321,
        res = "ui/activity/picture/052.png",
        res_en = "ui/activity/picture/052_en.png",
    },
    [322] = {
        id = 322,
        res = "ui/role/9.png",
        res_en = "ui/role/9_en.png",
    },
    [323] = {
        id = 323,
        res = "ui/help_view/fairy/10.png",
        res_en = "ui/help_view/fairy/10_en.png",
    },
    [324] = {
        id = 324,
        res = "ui/store/hot.png",
        res_en = "ui/store/hot_en.png",
    },
    [325] = {
        id = 325,
        res = "icon/fuben/007_1.png",
        res_en = "icon/fuben/007_1_en.png",
    },
    [326] = {
        id = 326,
        res = "ui/tujian/011.png",
        res_en = "ui/tujian/011_en.png",
    },
    [327] = {
        id = 327,
        res = "ui/role/newRoleShow/002.png",
        res_en = "ui/role/newRoleShow/002_en.png",
    },
    [328] = {
        id = 328,
        res = "ui/summon/047.png",
        res_en = "ui/summon/047_en.png",
    },
    [329] = {
        id = 329,
        res = "icon/hero/frame/10133.png",
        res_en = "icon/hero/frame/10133_en.png",
    },
    [330] = {
        id = 330,
        res = "ui/dalmap/event/wenan_3.png",
        res_en = "ui/dalmap/event/wenan_3_en.png",
    },
    [331] = {
        id = 331,
        res = "ui/activity/activity_bg/jumpbutton.png",
        res_en = "ui/activity/activity_bg/jumpbutton_en.png",
    },
    [332] = {
        id = 332,
        res = "ui/summon/029.png",
        res_en = "ui/summon/029_en.png",
    },
    [333] = {
        id = 333,
        res = "ui/simulation_trial/wenzi.png",
        res_en = "ui/simulation_trial/wenzi_en.png",
    },
    [334] = {
        id = 334,
        res = "icon/dafuweng/500053.png",
        res_en = "icon/dafuweng/500053_en.png",
    },
    [335] = {
        id = 335,
        res = "ui/fairy/new_ui/new_19.png",
        res_en = "ui/fairy/new_ui/new_19_en.png",
    },
    [336] = {
        id = 336,
        res = "ui/fairy_up/level_up.png",
        res_en = "ui/fairy_up/level_up_en.png",
    },
    [337] = {
        id = 337,
        res = "icon/equipment/name/Isabella.png",
        res_en = "icon/equipment/name/Isabella_en.png",
    },
    [338] = {
        id = 338,
        res = "icon/equipType2/Hod_gray.png",
        res_en = "icon/equipType2/Hod_gray_en.png",
    },
    [339] = {
        id = 339,
        res = "ui/simulation_trial/icon3.png",
        res_en = "ui/simulation_trial/icon3_en.png",
    },
    [340] = {
        id = 340,
        res = "ui/activity/kuangsan_card/enter/04.png",
        res_en = "ui/activity/kuangsan_card/enter/04_en.png",
    },
    [341] = {
        id = 341,
        res = "ui/activity/picture/ad106.png",
        res_en = "ui/activity/picture/ad106_en.png",
    },
    [342] = {
        id = 342,
        res = "icon/equipment/name/Anake.png",
        res_en = "icon/equipment/name/Anake_en.png",
    },
    [343] = {
        id = 343,
        res = "icon/equipment/name/Yaojingzhiwu.png",
        res_en = "icon/equipment/name/Yaojingzhiwu_en.png",
    },
    [344] = {
        id = 344,
        res = "ui/summon/028.png",
        res_en = "ui/summon/028_en.png",
    },
    [345] = {
        id = 345,
        res = "ui/recharge/gifts/remai_s.png",
        res_en = "ui/recharge/gifts/remai_s_en.png",
    },
    [346] = {
        id = 346,
        res = "icon/item/gift/541050.png",
        res_en = "icon/item/gift/541050_en.png",
    },
    [347] = {
        id = 347,
        res = "icon/equipType2/Chokhmah.png",
        res_en = "icon/equipType2/Chokhmah_en.png",
    },
    [348] = {
        id = 348,
        res = "ui/battle/n246.png",
        res_en = "ui/battle/n246_en.png",
    },
    [349] = {
        id = 349,
        res = "ui/fuben/rule/005.png",
        res_en = "ui/fuben/rule/005_en.png",
    },
    [350] = {
        id = 350,
        res = "icon/equipType2/Netzach_gray.png",
        res_en = "icon/equipType2/Netzach_gray_en.png",
    },
    [351] = {
        id = 351,
        res = "ui/activity/picture/ad19.png",
        res_en = "ui/activity/picture/ad19_en.png",
    },
    [352] = {
        id = 352,
        res = "ui/activity/midAutumn/entrance/002.png",
        res_en = "ui/activity/midAutumn/entrance/002_en.png",
    },
    [353] = {
        id = 353,
        res = "ui/monthcard/001.png",
        res_en = "ui/monthcard/001_en.png",
    },
    [354] = {
        id = 354,
        res = "ui/activity/yanhua_compose/entrance/004.png",
        res_en = "ui/activity/yanhua_compose/entrance/004_en.png",
    },
    [355] = {
        id = 355,
        res = "ui/newyear/cardGame/tianchen.png",
        res_en = "ui/newyear/cardGame/tianchen_en.png",
    },
    [356] = {
        id = 356,
        res = "icon/equipType2/Hod.png",
        res_en = "icon/equipType2/Hod_en.png",
    },
    [357] = {
        id = 357,
        res = "ui/activity/assist/kuangsan/add_recharge_001.png",
        res_en = "ui/activity/assist/kuangsan/add_recharge_001_en.png",
    },
    [358] = {
        id = 358,
        res = "icon/equipment/name/Nanguagongzhu.png",
        res_en = "icon/equipment/name/Nanguagongzhu_en.png",
    },
    [359] = {
        id = 359,
        res = "ui/dalmap/mainmap/001.png",
        res_en = "ui/dalmap/mainmap/001_en.png",
    },
    [360] = {
        id = 360,
        res = "ui/fuben/linkage/014.png",
        res_en = "ui/fuben/linkage/014_en.png",
    },
    [361] = {
        id = 361,
        res = "icon/fuben/Dungeon_xialamu.png",
        res_en = "icon/fuben/Dungeon_xialamu_en.png",
    },
    [362] = {
        id = 362,
        res = "ui/summon/upTips.png",
        res_en = "ui/summon/upTips_en.png",
    },
    [363] = {
        id = 363,
        res = "icon/item/gift/541059.png",
        res_en = "icon/item/gift/541059_en.png",
    },
    [364] = {
        id = 364,
        res = "ui/activity/picture/ad1.png",
        res_en = "ui/activity/picture/ad1_en.png",
    },
    [365] = {
        id = 365,
        res = "ui/activity/picture/icon109.png",
        res_en = "ui/activity/picture/icon109_en.png",
    },
    [366] = {
        id = 366,
        res = "ui/makefood/make/011.png",
        res_en = "ui/makefood/make/011_en.png",
    },
    [367] = {
        id = 367,
        res = "ui/simulation_trial5/result/title.png",
        res_en = "ui/simulation_trial5/result/title_en.png",
    },
    [368] = {
        id = 368,
        res = "ui/activity/znq_huigu/rukou/001.png",
        res_en = "ui/activity/znq_huigu/rukou/001_en.png",
    },
    [369] = {
        id = 369,
        res = "icon/title/6.png",
        res_en = "icon/title/6_en.png",
    },
    [370] = {
        id = 370,
        res = "ui/recharge/new_ui/02.png",
        res_en = "ui/recharge/new_ui/02_en.png",
    },
    [371] = {
        id = 371,
        res = "ui/activity/picture/icon24.png",
        res_en = "ui/activity/picture/icon24_en.png",
    },
    [372] = {
        id = 372,
        res = "icon/item/gift/541048.png",
        res_en = "icon/item/gift/541048_en.png",
    },
    [373] = {
        id = 373,
        res = "icon/equipment/name/Nvpumeijiu.png",
        res_en = "icon/equipment/name/Nvpumeijiu_en.png",
    },
    [374] = {
        id = 374,
        res = "ui/termBegin/main/005.png",
        res_en = "ui/termBegin/main/005_en.png",
    },
    [375] = {
        id = 375,
        res = "ui/activity/activity_bg/004.png",
        res_en = "ui/activity/activity_bg/004_en.png",
    },
    [376] = {
        id = 376,
        res = "ui/store/new_ui/icon10.png",
        res_en = "ui/store/new_ui/icon10_en.png",
    },
    [377] = {
        id = 377,
        res = "icon/item/goods/600007.png",
        res_en = "icon/item/goods/600007_en.png",
    },
    [378] = {
        id = 378,
        res = "ui/fuben/rule/007_1.png",
        res_en = "ui/fuben/rule/007_1_en.png",
    },
    [379] = {
        id = 379,
        res = "icon/equipment/name/Pozhenyiji.png",
        res_en = "icon/equipment/name/Pozhenyiji_en.png",
    },
    [380] = {
        id = 380,
        res = "icon/equipment/name/Liuzhuanzhijing.png",
        res_en = "icon/equipment/name/Liuzhuanzhijing_en.png",
    },
    [381] = {
        id = 381,
        res = "icon/item/goods/600009.png",
        res_en = "icon/item/goods/600009_en.png",
    },
    [382] = {
        id = 382,
        res = "icon/equipment/name/Christina.png",
        res_en = "icon/equipment/name/Christina_en.png",
    },
    [383] = {
        id = 383,
        res = "icon/equipment/name/Shengdankuangsan.png",
        res_en = "icon/equipment/name/Shengdankuangsan_en.png",
    },
    [384] = {
        id = 384,
        res = "icon/equipment/suit/shaonvsanrenzu.png",
        res_en = "icon/equipment/suit/shaonvsanrenzu_en.png",
    },
    [385] = {
        id = 385,
        res = "ui/activity/picture/ad59.png",
        res_en = "ui/activity/picture/ad59_en.png",
    },
    [386] = {
        id = 386,
        res = "icon/title/8.png",
        res_en = "icon/title/8_en.png",
    },
    [387] = {
        id = 387,
        res = "ui/activity/picture/icon110.png",
        res_en = "ui/activity/picture/icon110_en.png",
    },
    [388] = {
        id = 388,
        res = "ui/activity/ChronoCross/a1.png",
        res_en = "ui/activity/ChronoCross/a1_en.png",
    },
    [389] = {
        id = 389,
        res = "ui/monthcard/e.png",
        res_en = "ui/monthcard/e_en.png",
    },
    [390] = {
        id = 390,
        res = "icon/equipment/name/Diana2.png",
        res_en = "icon/equipment/name/Diana2_en.png",
    },
    [391] = {
        id = 391,
        res = "ui/activity/activity_bg/005.png",
        res_en = "ui/activity/activity_bg/005_en.png",
    },
    [392] = {
        id = 392,
        res = "icon/chatBubble/chatUseRes/004.png",
        res_en = "icon/chatBubble/chatUseRes/004_en.png",
    },
    [393] = {
        id = 393,
        res = "icon/chatBubble/008.png",
        res_en = "icon/chatBubble/008_en.png",
    },
    [394] = {
        id = 394,
        res = "icon/equipment/suit/nvpuzhuli.png",
        res_en = "icon/equipment/suit/nvpuzhuli_en.png",
    },
    [395] = {
        id = 395,
        res = "icon/equipment/name/Chaohanlingyu.png",
        res_en = "icon/equipment/name/Chaohanlingyu_en.png",
    },
    [396] = {
        id = 396,
        res = "ui/update/s3.png",
        res_en = "ui/update/s3_en.png",
    },
    [397] = {
        id = 397,
        res = "ui/activity/picture/icon146.png",
        res_en = "ui/activity/picture/icon146_en.png",
    },
    [398] = {
        id = 398,
        res = "ui/fuli/christmas_sign/002.png",
        res_en = "ui/fuli/christmas_sign/002_en.png",
    },
    [399] = {
        id = 399,
        res = "icon/equipment/name/Shengdanmeijiu.png",
        res_en = "icon/equipment/name/Shengdanmeijiu_en.png",
    },
    [400] = {
        id = 400,
        res = "ui/activity/picture/ad52.png",
        res_en = "ui/activity/picture/ad52_en.png",
    },
    [401] = {
        id = 401,
        res = "ui/update/s12.png",
        res_en = "ui/update/s12_en.png",
    },
    [402] = {
        id = 402,
        res = "icon/title/9.png",
        res_en = "icon/title/9_en.png",
    },
    [403] = {
        id = 403,
        res = "ui/common/webViewBack.png",
        res_en = "ui/common/webViewBack_en.png",
    },
    [404] = {
        id = 404,
        res = "ui/fairy/new_ui/new_41.png",
        res_en = "ui/fairy/new_ui/new_41_en.png",
    },
    [405] = {
        id = 405,
        res = "icon/equipment/name/Nomos.png",
        res_en = "icon/equipment/name/Nomos_en.png",
    },
    [406] = {
        id = 406,
        res = "icon/fuben/011.png",
        res_en = "icon/fuben/011_en.png",
    },
    [407] = {
        id = 407,
        res = "icon/equipment/name/Chunjieshixiang.png",
        res_en = "icon/equipment/name/Chunjieshixiang_en.png",
    },
    [408] = {
        id = 408,
        res = "icon/title/3.png",
        res_en = "icon/title/3_en.png",
    },
    [409] = {
        id = 409,
        res = "ui/fight_result/success.png",
        res_en = "ui/fight_result/success_en.png",
    },
    [410] = {
        id = 410,
        res = "ui/simulation_trial4/fanshi.png",
        res_en = "ui/simulation_trial4/fanshi_en.png",
    },
    [411] = {
        id = 411,
        res = "ui/simulation_trial5/result/3.png",
        res_en = "ui/simulation_trial5/result/3_en.png",
    },
    [412] = {
        id = 412,
        res = "ui/activity/picture/icon44.png",
        res_en = "ui/activity/picture/icon44_en.png",
    },
    [413] = {
        id = 413,
        res = "ui/activity/backPlayer/backPlayer2.png",
        res_en = "ui/activity/backPlayer/backPlayer2_en.png",
    },
    [414] = {
        id = 414,
        res = "ui/activity/015.png",
        res_en = "ui/activity/015_en.png",
    },
    [415] = {
        id = 415,
        res = "icon/equipment/name/Jinghongzhiyu.png",
        res_en = "icon/equipment/name/Jinghongzhiyu_en.png",
    },
    [416] = {
        id = 416,
        res = "ui/onlineteam/ready.png",
        res_en = "ui/onlineteam/ready_en.png",
    },
    [417] = {
        id = 417,
        res = "ui/activity/courage/007.png",
        res_en = "ui/activity/courage/007_en.png",
    },
    [418] = {
        id = 418,
        res = "ui/activity/picture/icon125.png",
        res_en = "ui/activity/picture/icon125_en.png",
    },
    [419] = {
        id = 419,
        res = "ui/oneyearChare/110301.png",
        res_en = "ui/oneyearChare/110301_en.png",
    },
    [420] = {
        id = 420,
        res = "icon/equipment/name/Isabella2.png",
        res_en = "icon/equipment/name/Isabella2_en.png",
    },
    [421] = {
        id = 421,
        res = "icon/teamHelp/006.png",
        res_en = "icon/teamHelp/006_en.png",
    },
    [422] = {
        id = 422,
        res = "ui/recharge/gifts/huodong_s.png",
        res_en = "ui/recharge/gifts/huodong_s_en.png",
    },
    [423] = {
        id = 423,
        res = "ui/activity/oneYear/card/007.png",
        res_en = "ui/activity/oneYear/card/007_en.png",
    },
    [424] = {
        id = 424,
        res = "icon/hero/frame/10109.png",
        res_en = "icon/hero/frame/10109_en.png",
    },
    [425] = {
        id = 425,
        res = "icon/equipment/name/Margaret.png",
        res_en = "icon/equipment/name/Margaret_en.png",
    },
    [426] = {
        id = 426,
        res = "icon/equipment/suit/xuesehuixiang.png",
        res_en = "icon/equipment/suit/xuesehuixiang_en.png",
    },
    [427] = {
        id = 427,
        res = "ui/activity/chrismasEntry/002.png",
        res_en = "ui/activity/chrismasEntry/002_en.png",
    },
    [428] = {
        id = 428,
        res = "ui/activity/picture/icon45.png",
        res_en = "ui/activity/picture/icon45_en.png",
    },
    [429] = {
        id = 429,
        res = "icon/equipment/suit/hundunsheling.png",
        res_en = "icon/equipment/suit/hundunsheling_en.png",
    },
    [430] = {
        id = 430,
        res = "ui/playerInfo/avatar/111.png",
        res_en = "ui/playerInfo/avatar/111_en.png",
    },
    [431] = {
        id = 431,
        res = "ui/Halloween/Main/018.png",
        res_en = "ui/Halloween/Main/018_en.png",
    },
    [432] = {
        id = 432,
        res = "ui/summon/simulation/fz.png",
        res_en = "ui/summon/simulation/fz_en.png",
    },
    [433] = {
        id = 433,
        res = "ui/activity/christmas_pre/b2.png",
        res_en = "ui/activity/christmas_pre/b2_en.png",
    },
    [434] = {
        id = 434,
        res = "ui/activity/christmas_dating/012.png",
        res_en = "ui/activity/christmas_dating/012_en.png",
    },
    [435] = {
        id = 435,
        res = "ui/monthcardNew1/bg.png",
        res_en = "ui/monthcardNew1/bg_en.png",
    },
    [436] = {
        id = 436,
        res = "ui/activity/courage/game/light/f4.png",
        res_en = "ui/activity/courage/game/light/f4_en.png",
    },
    [437] = {
        id = 437,
        res = "ui/summon/elf_contract/033.png",
        res_en = "ui/summon/elf_contract/033_en.png",
    },
    [438] = {
        id = 438,
        res = "ui/Evaluate/text_pl.png",
        res_en = "ui/Evaluate/text_pl_en.png",
    },
    [439] = {
        id = 439,
        res = "ui/help_view/fairy/11.png",
        res_en = "ui/help_view/fairy/11_en.png",
    },
    [440] = {
        id = 440,
        res = "icon/equipment/name/Shengdanshizhi.png",
        res_en = "icon/equipment/name/Shengdanshizhi_en.png",
    },
    [441] = {
        id = 441,
        res = "ui/activity/picture/icon49.png",
        res_en = "ui/activity/picture/icon49_en.png",
    },
    [442] = {
        id = 442,
        res = "icon/equipment/name/Corday.png",
        res_en = "icon/equipment/name/Corday_en.png",
    },
    [443] = {
        id = 443,
        res = "ui/activity/picture/ad107.png",
        res_en = "ui/activity/picture/ad107_en.png",
    },
    [444] = {
        id = 444,
        res = "ui/help_view/fairy/12.png",
        res_en = "ui/help_view/fairy/12_en.png",
    },
    [445] = {
        id = 445,
        res = "icon/fuben/levelIcon/Team/7_shengli_1.png",
        res_en = "icon/fuben/levelIcon/Team/7_shengli_1_en.png",
    },
    [446] = {
        id = 446,
        res = "ui/task/01/bg.png",
        res_en = "ui/task/01/bg_en.png",
    },
    [447] = {
        id = 447,
        res = "ui/recharge/gifts/yueka_n.png",
        res_en = "ui/recharge/gifts/yueka_n_en.png",
    },
    [448] = {
        id = 448,
        res = "icon/fuben/levelIcon/Team/5_yange_2.png",
        res_en = "icon/fuben/levelIcon/Team/5_yange_2_en.png",
    },
    [449] = {
        id = 449,
        res = "icon/equipment/name/Utsunomiya.png",
        res_en = "icon/equipment/name/Utsunomiya_en.png",
    },
    [450] = {
        id = 450,
        res = "ui/tujian/ui_04.png",
        res_en = "ui/tujian/ui_04_en.png",
    },
    [451] = {
        id = 451,
        res = "ui/activity/picture/ad61.png",
        res_en = "ui/activity/picture/ad61_en.png",
    },
    [452] = {
        id = 452,
        res = "icon/item/spring/004.png",
        res_en = "icon/item/spring/004_en.png",
    },
    [453] = {
        id = 453,
        res = "ui/activity/picture/icon37.png",
        res_en = "ui/activity/picture/icon37_en.png",
    },
    [454] = {
        id = 454,
        res = "ui/fuben/wanyouli/title.png",
        res_en = "ui/fuben/wanyouli/title_en.png",
    },
    [455] = {
        id = 455,
        res = "icon/item/goods/529011.png",
        res_en = "icon/item/goods/529011_en.png",
    },
    [456] = {
        id = 456,
        res = "ui/activity/activityBuy/bg.png",
        res_en = "ui/activity/activityBuy/bg_en.png",
    },
    [457] = {
        id = 457,
        res = "ui/common/mini_pop/funtion_open.png",
        res_en = "ui/common/mini_pop/funtion_open_en.png",
    },
    [458] = {
        id = 458,
        res = "ui/recharge/double.png",
        res_en = "ui/recharge/double_en.png",
    },
    [459] = {
        id = 459,
        res = "icon/equipment/suit/xiwangzhijian.png",
        res_en = "icon/equipment/suit/xiwangzhijian_en.png",
    },
    [460] = {
        id = 460,
        res = "ui/summon/010.png",
        res_en = "ui/summon/010_en.png",
    },
    [461] = {
        id = 461,
        res = "ui/activity/oneYear/luckyReward1/018.png",
        res_en = "ui/activity/oneYear/luckyReward1/018_en.png",
    },
    [462] = {
        id = 462,
        res = "ui/dalmap/event/project.png",
        res_en = "ui/dalmap/event/project_en.png",
    },
    [463] = {
        id = 463,
        res = "ui/activity/newPlayer/newPlayer02.png",
        res_en = "ui/activity/newPlayer/newPlayer02_en.png",
    },
    [464] = {
        id = 464,
        res = "ui/newyear/fishGame/4.png",
        res_en = "ui/newyear/fishGame/4_en.png",
    },
    [465] = {
        id = 465,
        res = "ui/activity/courage/note/007.png",
        res_en = "ui/activity/courage/note/007_en.png",
    },
    [466] = {
        id = 466,
        res = "icon/item/giftShowCourage/901002.png",
        res_en = "icon/item/giftShowCourage/901002_en.png",
    },
    [467] = {
        id = 467,
        res = "icon/equipment/name/Tiandongshuangling.png",
        res_en = "icon/equipment/name/Tiandongshuangling_en.png",
    },
    [468] = {
        id = 468,
        res = "ui/chat/roomTitle.png",
        res_en = "ui/chat/roomTitle_en.png",
    },
    [469] = {
        id = 469,
        res = "icon/equipment/suit/qianbianwanhua.png",
        res_en = "icon/equipment/suit/qianbianwanhua_en.png",
    },
    [470] = {
        id = 470,
        res = "ui/fairy/new_ui/baoshi/lock.png",
        res_en = "ui/fairy/new_ui/baoshi/lock_en.png",
    },
    [471] = {
        id = 471,
        res = "ui/summon/noobup.png",
        res_en = "ui/summon/noobup_en.png",
    },
    [472] = {
        id = 472,
        res = "ui/activity/activityScale9/check_check_big.png",
        res_en = "ui/activity/activityScale9/check_check_big_en.png",
    },
    [473] = {
        id = 473,
        res = "ui/fuben/wanyouli/chapter_1.png",
        res_en = "ui/fuben/wanyouli/chapter_1_en.png",
    },
    [474] = {
        id = 474,
        res = "ui/activity/feedback/get.png",
        res_en = "ui/activity/feedback/get_en.png",
    },
    [475] = {
        id = 475,
        res = "ui/activity/picture/ad146.png",
        res_en = "ui/activity/picture/ad146_en.png",
    },
    [476] = {
        id = 476,
        res = "icon/item/goods/528008.png",
        res_en = "icon/item/goods/528008_en.png",
    },
    [477] = {
        id = 477,
        res = "ui/activity/znq_huigu/main/023.png",
        res_en = "ui/activity/znq_huigu/main/023_en.png",
    },
    [478] = {
        id = 478,
        res = "ui/activity/kuangsan/kuangsanEntry.png",
        res_en = "ui/activity/kuangsan/kuangsanEntry_en.png",
    },
    [479] = {
        id = 479,
        res = "icon/chatEmotion/014.png",
        res_en = "icon/chatEmotion/014_en.png",
    },
    [480] = {
        id = 480,
        res = "ui/recharge/growFundview/bg.png",
        res_en = "ui/recharge/growFundview/bg_en.png",
    },
    [481] = {
        id = 481,
        res = "ui/activity/picture/icon47.png",
        res_en = "ui/activity/picture/icon47_en.png",
    },
    [482] = {
        id = 482,
        res = "ui/recharge/month_card_2.png",
        res_en = "ui/recharge/month_card_2_en.png",
    },
    [483] = {
        id = 483,
        res = "icon/fuben/levelIcon/Team/10_wangguo_2.png",
        res_en = "icon/fuben/levelIcon/Team/10_wangguo_2_en.png",
    },
    [484] = {
        id = 484,
        res = "ui/agora/5.png",
        res_en = "ui/agora/5_en.png",
    },
    [485] = {
        id = 485,
        res = "icon/item/goods/570515.png",
        res_en = "icon/item/goods/570515_en.png",
    },
    [486] = {
        id = 486,
        res = "ui/fuben/rule/003.png",
        res_en = "ui/fuben/rule/003_en.png",
    },
    [487] = {
        id = 487,
        res = "ui/summon/052.png",
        res_en = "ui/summon/052_en.png",
    },
    [488] = {
        id = 488,
        res = "icon/equipment/name/Pojianzhishi.png",
        res_en = "icon/equipment/name/Pojianzhishi_en.png",
    },
    [489] = {
        id = 489,
        res = "ui/activity/picture/icon5.png",
        res_en = "ui/activity/picture/icon5_en.png",
    },
    [490] = {
        id = 490,
        res = "ui/activity/picture/ad36.png",
        res_en = "ui/activity/picture/ad36_en.png",
    },
    [491] = {
        id = 491,
        res = "ui/newyear/cardGame/shuangyu2.png",
        res_en = "ui/newyear/cardGame/shuangyu2_en.png",
    },
    [492] = {
        id = 492,
        res = "icon/equipment/name/Elizabeth.png",
        res_en = "icon/equipment/name/Elizabeth_en.png",
    },
    [493] = {
        id = 493,
        res = "ui/activity/activityScale9/finishTips.png",
        res_en = "ui/activity/activityScale9/finishTips_en.png",
    },
    [494] = {
        id = 494,
        res = "ui/activity/oneYear/luckyReward/001.png",
        res_en = "ui/activity/oneYear/luckyReward/001_en.png",
    },
    [495] = {
        id = 495,
        res = "ui/recharge/gifts/teqing_n.png",
        res_en = "ui/recharge/gifts/teqing_n_en.png",
    },
    [496] = {
        id = 496,
        res = "ui/activity/016.png",
        res_en = "ui/activity/016_en.png",
    },
    [497] = {
        id = 497,
        res = "ui/mainLayer/new_ui/dating_text.png",
        res_en = "ui/mainLayer/new_ui/dating_text_en.png",
    },
    [498] = {
        id = 498,
        res = "icon/equipment/name/Bastet2.png",
        res_en = "icon/equipment/name/Bastet2_en.png",
    },
    [499] = {
        id = 499,
        res = "icon/equipment/name/Shengdanqinli.png",
        res_en = "icon/equipment/name/Shengdanqinli_en.png",
    },
    [500] = {
        id = 500,
        res = "ui/activity/Halloween/logo/logo58.png",
        res_en = "ui/activity/Halloween/logo/logo58_en.png",
    },
    [501] = {
        id = 501,
        res = "ui/task/01/4.png",
        res_en = "ui/task/01/4_en.png",
    },
    [502] = {
        id = 502,
        res = "ui/summon/elf_contract/title_tip.png",
        res_en = "ui/summon/elf_contract/title_tip_en.png",
    },
    [503] = {
        id = 503,
        res = "ui/summon/050.png",
        res_en = "ui/summon/050_en.png",
    },
    [504] = {
        id = 504,
        res = "ui/activity/picture/ad147.png",
        res_en = "ui/activity/picture/ad147_en.png",
    },
    [505] = {
        id = 505,
        res = "ui/makefood/food_finish.png",
        res_en = "ui/makefood/food_finish_en.png",
    },
    [506] = {
        id = 506,
        res = "ui/simulation_trial2/bg55.png",
        res_en = "ui/simulation_trial2/bg55_en.png",
    },
    [507] = {
        id = 507,
        res = "ui/activity/courage/note/010.png",
        res_en = "ui/activity/courage/note/010_en.png",
    },
    [508] = {
        id = 508,
        res = "ui/simulation_trial2/lipu.png",
        res_en = "ui/simulation_trial2/lipu_en.png",
    },
    [509] = {
        id = 509,
        res = "ui/monthcard/s.png",
        res_en = "ui/monthcard/s_en.png",
    },
    [510] = {
        id = 510,
        res = "icon/equipment/name/Freya2.png",
        res_en = "icon/equipment/name/Freya2_en.png",
    },
    [511] = {
        id = 511,
        res = "ui/bag/new_ui/new_10.png",
        res_en = "ui/bag/new_ui/new_10_en.png",
    },
    [512] = {
        id = 512,
        res = "ui/activity/newyear_fuben/ui_002.png",
        res_en = "ui/activity/newyear_fuben/ui_002_en.png",
    },
    [513] = {
        id = 513,
        res = "ui/activity/anniversary/welfare_01.png",
        res_en = "ui/activity/anniversary/welfare_01_en.png",
    },
    [514] = {
        id = 514,
        res = "ui/Equipment/new_ui/JLGZ_shuxing_icon.png",
        res_en = "ui/Equipment/new_ui/JLGZ_shuxing_icon_en.png",
    },
    [515] = {
        id = 515,
        res = "ui/activity/picture/icon21.png",
        res_en = "ui/activity/picture/icon21_en.png",
    },
    [516] = {
        id = 516,
        res = "ui/activity/oneYear/luckyReward1/013.png",
        res_en = "ui/activity/oneYear/luckyReward1/013_en.png",
    },
    [517] = {
        id = 517,
        res = "ui/activity/picture/ad9.png",
        res_en = "ui/activity/picture/ad9_en.png",
    },
    [518] = {
        id = 518,
        res = "ui/chat/t4.png",
        res_en = "ui/chat/t4_en.png",
    },
    [519] = {
        id = 519,
        res = "ui/summon/ad_clothes.png",
        res_en = "ui/summon/ad_clothes_en.png",
    },
    [520] = {
        id = 520,
        res = "ui/recharge/recharge_total_icon_rmb.png",
        res_en = "ui/recharge/recharge_total_icon_rmb_en.png",
    },
    [521] = {
        id = 521,
        res = "icon/fuben/levelIcon/Team/3_lijie_2.png",
        res_en = "icon/fuben/levelIcon/Team/3_lijie_2_en.png",
    },
    [522] = {
        id = 522,
        res = "ui/dfwautumn/rukou/002.png",
        res_en = "ui/dfwautumn/rukou/002_en.png",
    },
    [523] = {
        id = 523,
        res = "ui/Evaluate/btn_ok1.png",
        res_en = "ui/Evaluate/btn_ok1_en.png",
    },
    [524] = {
        id = 524,
        res = "ui/monthcard/018.png",
        res_en = "ui/monthcard/018_en.png",
    },
    [525] = {
        id = 525,
        res = "icon/equipment/suit/meiyingguici.png",
        res_en = "icon/equipment/suit/meiyingguici_en.png",
    },
    [526] = {
        id = 526,
        res = "icon/equipment/name/Mocana.png",
        res_en = "icon/equipment/name/Mocana_en.png",
    },
    [527] = {
        id = 527,
        res = "icon/equipment/name/Shiguangzhiwu.png",
        res_en = "icon/equipment/name/Shiguangzhiwu_en.png",
    },
    [528] = {
        id = 528,
        res = "ui/activity/activityScale9/reach_get_big.png",
        res_en = "ui/activity/activityScale9/reach_get_big_en.png",
    },
    [529] = {
        id = 529,
        res = "icon/equipment/suit/zhanjishouhu.png",
        res_en = "icon/equipment/suit/zhanjishouhu_en.png",
    },
    [530] = {
        id = 530,
        res = "ui/activity/picture/icon16.png",
        res_en = "ui/activity/picture/icon16_en.png",
    },
    [531] = {
        id = 531,
        res = "ui/newyear/cardGame/shizi.png",
        res_en = "ui/newyear/cardGame/shizi_en.png",
    },
    [532] = {
        id = 532,
        res = "ui/summon/mengxin.png",
        res_en = "ui/summon/mengxin_en.png",
    },
    [533] = {
        id = 533,
        res = "icon/equipment/name/heb2.png",
        res_en = "icon/equipment/name/heb2_en.png",
    },
    [534] = {
        id = 534,
        res = "ui/simulation_trial4/002.png",
        res_en = "ui/simulation_trial4/002_en.png",
    },
    [535] = {
        id = 535,
        res = "icon/mainDating/mainName/4.png",
        res_en = "icon/mainDating/mainName/4_en.png",
    },
    [536] = {
        id = 536,
        res = "ui/activity/picture/icon124.png",
        res_en = "ui/activity/picture/icon124_en.png",
    },
    [537] = {
        id = 537,
        res = "ui/summon/simulation/wyl.png",
        res_en = "ui/summon/simulation/wyl_en.png",
    },
    [538] = {
        id = 538,
        res = "ui/summon/elf_contract/002.png",
        res_en = "ui/summon/elf_contract/002_en.png",
    },
    [539] = {
        id = 539,
        res = "icon/equipment/name/Qiangweizhan.png",
        res_en = "icon/equipment/name/Qiangweizhan_en.png",
    },
    [540] = {
        id = 540,
        res = "ui/activity/oneYear/luckyReward/vote/cj001.png",
        res_en = "ui/activity/oneYear/luckyReward/vote/cj001_en.png",
    },
    [541] = {
        id = 541,
        res = "ui/activity/picture/ad13.png",
        res_en = "ui/activity/picture/ad13_en.png",
    },
    [542] = {
        id = 542,
        res = "ui/monthcardNew1/010.png",
        res_en = "ui/monthcardNew1/010_en.png",
    },
    [543] = {
        id = 543,
        res = "ui/activity/activityScale9/bg.png",
        res_en = "ui/activity/activityScale9/bg_en.png",
    },
    [544] = {
        id = 544,
        res = "ui/task/training/ui_banner1.png",
        res_en = "ui/task/training/ui_banner1_en.png",
    },
    [545] = {
        id = 545,
        res = "ui/activity/picture/icon82.png",
        res_en = "ui/activity/picture/icon82_en.png",
    },
    [546] = {
        id = 546,
        res = "icon/equipType2/Chokhmah_gray.png",
        res_en = "icon/equipType2/Chokhmah_gray_en.png",
    },
    [547] = {
        id = 547,
        res = "ui/newyear/cardGame/chunv.png",
        res_en = "ui/newyear/cardGame/chunv_en.png",
    },
    [548] = {
        id = 548,
        res = "icon/equipment/name/Chunjieqinli.png",
        res_en = "icon/equipment/name/Chunjieqinli_en.png",
    },
    [549] = {
        id = 549,
        res = "icon/equipment/name/Imola.png",
        res_en = "icon/equipment/name/Imola_en.png",
    },
    [550] = {
        id = 550,
        res = "ui/role/5.png",
        res_en = "ui/role/5_en.png",
    },
    [551] = {
        id = 551,
        res = "ui/fairy/new_ui/new_14.png",
        res_en = "ui/fairy/new_ui/new_14_en.png",
    },
    [552] = {
        id = 552,
        res = "ui/activity/christmas_pre/b3.png",
        res_en = "ui/activity/christmas_pre/b3_en.png",
    },
    [553] = {
        id = 553,
        res = "ui/task/training/ui_bg.png",
        res_en = "ui/task/training/ui_bg_en.png",
    },
    [554] = {
        id = 554,
        res = "icon/fuben/levelIcon/Team/2_zhihui.png",
        res_en = "icon/fuben/levelIcon/Team/2_zhihui_en.png",
    },
    [555] = {
        id = 555,
        res = "ui/activity/picture/icon17.png",
        res_en = "ui/activity/picture/icon17_en.png",
    },
    [556] = {
        id = 556,
        res = "ui/newyear/cardGame/shuangzi2.png",
        res_en = "ui/newyear/cardGame/shuangzi2_en.png",
    },
    [557] = {
        id = 557,
        res = "ui/bag/new_ui/limit.png",
        res_en = "ui/bag/new_ui/limit_en.png",
    },
    [558] = {
        id = 558,
        res = "ui/tujian/003.png",
        res_en = "ui/tujian/003_en.png",
    },
    [559] = {
        id = 559,
        res = "ui/update/s56.png",
        res_en = "ui/update/s56_en.png",
    },
    [560] = {
        id = 560,
        res = "ui/dalmap/event/wenan_2.png",
        res_en = "ui/dalmap/event/wenan_2_en.png",
    },
    [561] = {
        id = 561,
        res = "icon/fuben/levelIcon/Team/7_shengli.png",
        res_en = "icon/fuben/levelIcon/Team/7_shengli_en.png",
    },
    [562] = {
        id = 562,
        res = "ui/summon/elf_contract/031.png",
        res_en = "ui/summon/elf_contract/031_en.png",
    },
    [563] = {
        id = 563,
        res = "icon/fuben/levelIcon/Team/3_lijie_1.png",
        res_en = "icon/fuben/levelIcon/Team/3_lijie_1_en.png",
    },
    [564] = {
        id = 564,
        res = "ui/summon/simulation/fs.png",
        res_en = "ui/summon/simulation/fs_en.png",
    },
    [565] = {
        id = 565,
        res = "ui/activity/picture/icon35.png",
        res_en = "ui/activity/picture/icon35_en.png",
    },
    [566] = {
        id = 566,
        res = "ui/activity/picture/ad127.png",
        res_en = "ui/activity/picture/ad127_en.png",
    },
    [567] = {
        id = 567,
        res = "ui/mainui/main_infoStation.png",
        res_en = "ui/mainui/main_infoStation_en.png",
    },
    [568] = {
        id = 568,
        res = "ui/activity/assist/kuangsan/add_recharge_006.png",
        res_en = "ui/activity/assist/kuangsan/add_recharge_006_en.png",
    },
    [569] = {
        id = 569,
        res = "icon/equipment/name/Hestia.png",
        res_en = "icon/equipment/name/Hestia_en.png",
    },
    [570] = {
        id = 570,
        res = "ui/dalmap/task/002.png",
        res_en = "ui/dalmap/task/002_en.png",
    },
    [571] = {
        id = 571,
        res = "icon/hero/frame/10134.png",
        res_en = "icon/hero/frame/10134_en.png",
    },
    [572] = {
        id = 572,
        res = "ui/activity/picture/ad124.png",
        res_en = "ui/activity/picture/ad124_en.png",
    },
    [573] = {
        id = 573,
        res = "icon/equipment/suit/cuilvzhixin.png",
        res_en = "icon/equipment/suit/cuilvzhixin_en.png",
    },
    [574] = {
        id = 574,
        res = "ui/ChronoCros/main/dateBtn.png",
        res_en = "ui/ChronoCros/main/dateBtn_en.png",
    },
    [575] = {
        id = 575,
        res = "ui/dispatch/ui_097.png",
        res_en = "ui/dispatch/ui_097_en.png",
    },
    [576] = {
        id = 576,
        res = "ui/activity/picture/icon34.png",
        res_en = "ui/activity/picture/icon34_en.png",
    },
    [577] = {
        id = 577,
        res = "icon/fuben/005.png",
        res_en = "icon/fuben/005_en.png",
    },
    [578] = {
        id = 578,
        res = "icon/item/goods/501018.png",
        res_en = "icon/item/goods/501018_en.png",
    },
    [579] = {
        id = 579,
        res = "ui/fairy/new_ui/btn_share.png",
        res_en = "ui/fairy/new_ui/btn_share_en.png",
    },
    [580] = {
        id = 580,
        res = "ui/newyear/cardGame/mojie2.png",
        res_en = "ui/newyear/cardGame/mojie2_en.png",
    },
    [581] = {
        id = 581,
        res = "icon/chatEmotion/016.png",
        res_en = "icon/chatEmotion/016_en.png",
    },
    [582] = {
        id = 582,
        res = "ui/activity/023.png",
        res_en = "ui/activity/023_en.png",
    },
    [583] = {
        id = 583,
        res = "ui/activity/sevenEx/009.png",
        res_en = "ui/activity/sevenEx/009_en.png",
    },
    [584] = {
        id = 584,
        res = "ui/fuben/wanyouli/chapter_unknow.png",
        res_en = "ui/fuben/wanyouli/chapter_unknow_en.png",
    },
    [585] = {
        id = 585,
        res = "ui/update/s2.png",
        res_en = "ui/update/s2_en.png",
    },
    [586] = {
        id = 586,
        res = "icon/item/goods/570517.png",
        res_en = "icon/item/goods/570517_en.png",
    },
    [587] = {
        id = 587,
        res = "ui/mainLayer/new_ui/a2.png",
        res_en = "ui/mainLayer/new_ui/a2_en.png",
    },
    [588] = {
        id = 588,
        res = "ui/activity/activityScale9/reach_get_small.png",
        res_en = "ui/activity/activityScale9/reach_get_small_en.png",
    },
    [589] = {
        id = 589,
        res = "ui/activity/christmas_pre/b4.png",
        res_en = "ui/activity/christmas_pre/b4_en.png",
    },
    [590] = {
        id = 590,
        res = "ui/simulation_trial4/003.png",
        res_en = "ui/simulation_trial4/003_en.png",
    },
    [591] = {
        id = 591,
        res = "ui/activity/preheat/box1.png",
        res_en = "ui/activity/preheat/box1_en.png",
    },
    [592] = {
        id = 592,
        res = "icon/equipment/name/Shengdanwanyouli.png",
        res_en = "icon/equipment/name/Shengdanwanyouli_en.png",
    },
    [593] = {
        id = 593,
        res = "icon/equipment/name/Katyusha.png",
        res_en = "icon/equipment/name/Katyusha_en.png",
    },
    [594] = {
        id = 594,
        res = "icon/hero/frame/10105.png",
        res_en = "icon/hero/frame/10105_en.png",
    },
    [595] = {
        id = 595,
        res = "icon/fuben/levelIcon/Team/10_wangguo.png",
        res_en = "icon/fuben/levelIcon/Team/10_wangguo_en.png",
    },
    [596] = {
        id = 596,
        res = "ui/activity/picture/ad108.png",
        res_en = "ui/activity/picture/ad108_en.png",
    },
    [597] = {
        id = 597,
        res = "ui/activity/picture/ad8.png",
        res_en = "ui/activity/picture/ad8_en.png",
    },
    [598] = {
        id = 598,
        res = "icon/item/goods/600004.png",
        res_en = "icon/item/goods/600004_en.png",
    },
    [599] = {
        id = 599,
        res = "ui/simulation_trial5/result/3-1.png",
        res_en = "ui/simulation_trial5/result/3-1_en.png",
    },
    [600] = {
        id = 600,
        res = "ui/activity/newYear/mainlayer/024.png",
        res_en = "ui/activity/newYear/mainlayer/024_en.png",
    },
    [601] = {
        id = 601,
        res = "icon/equipment/name/Guinevere2.png",
        res_en = "icon/equipment/name/Guinevere2_en.png",
    },
    [602] = {
        id = 602,
        res = "icon/chatBubble/chatUseRes/001.png",
        res_en = "icon/chatBubble/chatUseRes/001_en.png",
    },
    [603] = {
        id = 603,
        res = "ui/oneyearChare/110401.png",
        res_en = "ui/oneyearChare/110401_en.png",
    },
    [604] = {
        id = 604,
        res = "ui/activity/picture/ad86.png",
        res_en = "ui/activity/picture/ad86_en.png",
    },
    [605] = {
        id = 605,
        res = "ui/summon/026.png",
        res_en = "ui/summon/026_en.png",
    },
    [606] = {
        id = 606,
        res = "icon/item/goods/529025.png",
        res_en = "icon/item/goods/529025_en.png",
    },
    [607] = {
        id = 607,
        res = "ui/activity/sevenEx/022.png",
        res_en = "ui/activity/sevenEx/022_en.png",
    },
    [608] = {
        id = 608,
        res = "icon/equipment/name/Geobiyye.png",
        res_en = "icon/equipment/name/Geobiyye_en.png",
    },
    [609] = {
        id = 609,
        res = "ui/dawuwong/help.png",
        res_en = "ui/dawuwong/help_en.png",
    },
    [610] = {
        id = 610,
        res = "ui/summon/025.png",
        res_en = "ui/summon/025_en.png",
    },
    [611] = {
        id = 611,
        res = "icon/hero/name/554009.png",
        res_en = "icon/hero/name/554009_en.png",
    },
    [612] = {
        id = 612,
        res = "icon/equipment/name/Feikongnvwu.png",
        res_en = "icon/equipment/name/Feikongnvwu_en.png",
    },
    [613] = {
        id = 613,
        res = "icon/equipment/name/Hidekatsu2.png",
        res_en = "icon/equipment/name/Hidekatsu2_en.png",
    },
    [614] = {
        id = 614,
        res = "ui/update/s15.png",
        res_en = "ui/update/s15_en.png",
    },
    [615] = {
        id = 615,
        res = "ui/activity/sevenEx/009_1.png",
        res_en = "ui/activity/sevenEx/009_1_en.png",
    },
    [616] = {
        id = 616,
        res = "icon/equipment/name/Duanzuizhizhan.png",
        res_en = "icon/equipment/name/Duanzuizhizhan_en.png",
    },
    [617] = {
        id = 617,
        res = "ui/recharge/tokenPopView/5.png",
        res_en = "ui/recharge/tokenPopView/5_en.png",
    },
    [618] = {
        id = 618,
        res = "ui/recharge/16.png",
        res_en = "ui/recharge/16_en.png",
    },
    [619] = {
        id = 619,
        res = "ui/iphoneX/roleTeach/roleTeachBuy/007.png",
        res_en = "ui/iphoneX/roleTeach/roleTeachBuy/007_en.png",
    },
    [620] = {
        id = 620,
        res = "ui/tujian/ui_10.png",
        res_en = "ui/tujian/ui_10_en.png",
    },
    [621] = {
        id = 621,
        res = "ui/dispatch/ui_085.png",
        res_en = "ui/dispatch/ui_085_en.png",
    },
    [622] = {
        id = 622,
        res = "icon/equipment/suit/huolingshouhu.png",
        res_en = "icon/equipment/suit/huolingshouhu_en.png",
    },
    [623] = {
        id = 623,
        res = "icon/chatEmotion/024.png",
        res_en = "icon/chatEmotion/024_en.png",
    },
    [624] = {
        id = 624,
        res = "icon/equipment/suit/yingyongchongfeng.png",
        res_en = "icon/equipment/suit/yingyongchongfeng_en.png",
    },
    [625] = {
        id = 625,
        res = "ui/simulation_trial2/bulan.png",
        res_en = "ui/simulation_trial2/bulan_en.png",
    },
    [626] = {
        id = 626,
        res = "ui/dispatch/ui_051.png",
        res_en = "ui/dispatch/ui_051_en.png",
    },
    [627] = {
        id = 627,
        res = "ui/summon/049.png",
        res_en = "ui/summon/049_en.png",
    },
    [628] = {
        id = 628,
        res = "ui/recharge/gifts/huodong_n.png",
        res_en = "ui/recharge/gifts/huodong_n_en.png",
    },
    [629] = {
        id = 629,
        res = "ui/activity/picture/ad15.png",
        res_en = "ui/activity/picture/ad15_en.png",
    },
    [630] = {
        id = 630,
        res = "ui/activity/allServer_recharge/v2/005.png",
        res_en = "ui/activity/allServer_recharge/v2/005_en.png",
    },
    [631] = {
        id = 631,
        res = "icon/mainDating/mainName/5.png",
        res_en = "icon/mainDating/mainName/5_en.png",
    },
    [632] = {
        id = 632,
        res = "ui/activity/picture/icon66.png",
        res_en = "ui/activity/picture/icon66_en.png",
    },
    [633] = {
        id = 633,
        res = "ui/activity/picture/icon64.png",
        res_en = "ui/activity/picture/icon64_en.png",
    },
    [634] = {
        id = 634,
        res = "ui/newyear/cardGame/tianxie2.png",
        res_en = "ui/newyear/cardGame/tianxie2_en.png",
    },
    [635] = {
        id = 635,
        res = "icon/item/goods/570523.png",
        res_en = "icon/item/goods/570523_en.png",
    },
    [636] = {
        id = 636,
        res = "ui/Equipment/new_ui/btn_stepup.png",
        res_en = "ui/Equipment/new_ui/btn_stepup_en.png",
    },
    [637] = {
        id = 637,
        res = "ui/activity/picture/icon108.png",
        res_en = "ui/activity/picture/icon108_en.png",
    },
    [638] = {
        id = 638,
        res = "icon/equipment/name/Chunjiemeijiu.png",
        res_en = "icon/equipment/name/Chunjiemeijiu_en.png",
    },
    [639] = {
        id = 639,
        res = "icon/item/goods/1200027.png",
        res_en = "icon/item/goods/1200027_en.png",
    },
    [640] = {
        id = 640,
        res = "icon/dafuweng/500052.png",
        res_en = "icon/dafuweng/500052_en.png",
    },
    [641] = {
        id = 641,
        res = "ui/activity/picture/icon68.png",
        res_en = "ui/activity/picture/icon68_en.png",
    },
    [642] = {
        id = 642,
        res = "ui/activity/znq_huigu/tip/015.png",
        res_en = "ui/activity/znq_huigu/tip/015_en.png",
    },
    [643] = {
        id = 643,
        res = "icon/pokedexActivity/logo_manyueji1.png",
        res_en = "icon/pokedexActivity/logo_manyueji1_en.png",
    },
    [644] = {
        id = 644,
        res = "icon/fuben/levelIcon/Team/1_wangguan_2.png",
        res_en = "icon/fuben/levelIcon/Team/1_wangguan_2_en.png",
    },
    [645] = {
        id = 645,
        res = "icon/equipment/suit/binxuelinyu.png",
        res_en = "icon/equipment/suit/binxuelinyu_en.png",
    },
    [646] = {
        id = 646,
        res = "icon/equipment/name/Elena.png",
        res_en = "icon/equipment/name/Elena_en.png",
    },
    [647] = {
        id = 647,
        res = "ui/activity/activityStyle/giftActivity/styleCur/img_title.png",
        res_en = "ui/activity/activityStyle/giftActivity/styleCur/img_title_en.png",
    },
    [648] = {
        id = 648,
        res = "ui/newCity/reward/020.png",
        res_en = "ui/newCity/reward/020_en.png",
    },
    [649] = {
        id = 649,
        res = "ui/activity/picture/icon135.png",
        res_en = "ui/activity/picture/icon135_en.png",
    },
    [650] = {
        id = 650,
        res = "ui/activity/picture/icon58.png",
        res_en = "ui/activity/picture/icon58_en.png",
    },
    [651] = {
        id = 651,
        res = "ui/oneyearChare/2.png",
        res_en = "ui/oneyearChare/2_en.png",
    },
    [652] = {
        id = 652,
        res = "ui/summon/exchange.png",
        res_en = "ui/summon/exchange_en.png",
    },
    [653] = {
        id = 653,
        res = "ui/summon/044.png",
        res_en = "ui/summon/044_en.png",
    },
    [654] = {
        id = 654,
        res = "ui/summon/award.png",
        res_en = "ui/summon/award_en.png",
    },
    [655] = {
        id = 655,
        res = "ui/onlineteam/getting_ready.png",
        res_en = "ui/onlineteam/getting_ready_en.png",
    },
    [656] = {
        id = 656,
        res = "ui/fairy_angle/passSkillBG.png",
        res_en = "ui/fairy_angle/passSkillBG_en.png",
    },
    [657] = {
        id = 657,
        res = "icon/equipType2/Yesod_gray.png",
        res_en = "icon/equipType2/Yesod_gray_en.png",
    },
    [658] = {
        id = 658,
        res = "ui/activity/picture/icon11.png",
        res_en = "ui/activity/picture/icon11_en.png",
    },
    [659] = {
        id = 659,
        res = "ui/summon/051.png",
        res_en = "ui/summon/051_en.png",
    },
    [660] = {
        id = 660,
        res = "ui/activity/picture/icon10.png",
        res_en = "ui/activity/picture/icon10_en.png",
    },
    [661] = {
        id = 661,
        res = "icon/mainDating/mainName/2.png",
        res_en = "icon/mainDating/mainName/2_en.png",
    },
    [662] = {
        id = 662,
        res = "icon/equipment/name/Aerdese.png",
        res_en = "icon/equipment/name/Aerdese_en.png",
    },
    [663] = {
        id = 663,
        res = "ui/Evaluate/text_pl1.png",
        res_en = "ui/Evaluate/text_pl1_en.png",
    },
    [664] = {
        id = 664,
        res = "icon/item/goods/529015.png",
        res_en = "icon/item/goods/529015_en.png",
    },
    [665] = {
        id = 665,
        res = "ui/activity/picture/ad2.png",
        res_en = "ui/activity/picture/ad2_en.png",
    },
    [666] = {
        id = 666,
        res = "ui/onlineteam/capname_1.png",
        res_en = "ui/onlineteam/capname_1_en.png",
    },
    [667] = {
        id = 667,
        res = "ui/activity/picture/icon136.png",
        res_en = "ui/activity/picture/icon136_en.png",
    },
    [668] = {
        id = 668,
        res = "ui/activity/picture/icon59.png",
        res_en = "ui/activity/picture/icon59_en.png",
    },
    [669] = {
        id = 669,
        res = "ui/activity/picture/icon152.png",
        res_en = "ui/activity/picture/icon152_en.png",
    },
    [670] = {
        id = 670,
        res = "icon/equipment/name/Jinjici.png",
        res_en = "icon/equipment/name/Jinjici_en.png",
    },
    [671] = {
        id = 671,
        res = "ui/newyear/cardGame/jinniu.png",
        res_en = "ui/newyear/cardGame/jinniu_en.png",
    },
    [672] = {
        id = 672,
        res = "ui/league/texun/030.png",
        res_en = "ui/league/texun/030_en.png",
    },
    [673] = {
        id = 673,
        res = "ui/activity/activityBuy/geted.png",
        res_en = "ui/activity/activityBuy/geted_en.png",
    },
    [674] = {
        id = 674,
        res = "icon/item/goods/529009.png",
        res_en = "icon/item/goods/529009_en.png",
    },
    [675] = {
        id = 675,
        res = "ui/activity/activityMain3/bg3.png",
        res_en = "ui/activity/activityMain3/bg3_en.png",
    },
    [676] = {
        id = 676,
        res = "ui/termBegin/main/007.png",
        res_en = "ui/termBegin/main/007_en.png",
    },
    [677] = {
        id = 677,
        res = "ui/activity/picture/ad4.png",
        res_en = "ui/activity/picture/ad4_en.png",
    },
    [678] = {
        id = 678,
        res = "ui/activity/001.png",
        res_en = "ui/activity/001_en.png",
    },
    [679] = {
        id = 679,
        res = "ui/playerInfo/playerView/032.png",
        res_en = "ui/playerInfo/playerView/032_en.png",
    },
    [680] = {
        id = 680,
        res = "ui/summon/more.png",
        res_en = "ui/summon/more_en.png",
    },
    [681] = {
        id = 681,
        res = "ui/collect/002.png",
        res_en = "ui/collect/002_en.png",
    },
    [682] = {
        id = 682,
        res = "ui/activity/welfareSign/001.png",
        res_en = "ui/activity/welfareSign/001_en.png",
    },
    [683] = {
        id = 683,
        res = "ui/activity/picture/ad38.png",
        res_en = "ui/activity/picture/ad38_en.png",
    },
    [684] = {
        id = 684,
        res = "ui/BlackAndWhite/Main/001.png",
        res_en = "ui/BlackAndWhite/Main/001_en.png",
    },
    [685] = {
        id = 685,
        res = "ui/mainLayer/new_ui/battle_text.png",
        res_en = "ui/mainLayer/new_ui/battle_text_en.png",
    },
    [686] = {
        id = 686,
        res = "ui/activity/christmas_dating/003.png",
        res_en = "ui/activity/christmas_dating/003_en.png",
    },
    [687] = {
        id = 687,
        res = "ui/mainui/battletrance.png",
        res_en = "ui/mainui/battle_entrance_en.png",
    },
    [688] = {
        id = 688,
        res = "ui/share/get.png",
        res_en = "ui/share/get_en.png",
    },
    [689] = {
        id = 689,
        res = "ui/infoStation/infostation_bg.png",
        res_en = "ui/infoStation/infostation_bg_en.png",
    },
    [690] = {
        id = 690,
        res = "ui/summon/035.png",
        res_en = "ui/summon/035_en.png",
    },
    [691] = {
        id = 691,
        res = "ui/activity/picture/ad55.png",
        res_en = "ui/activity/picture/ad55_en.png",
    },
    [692] = {
        id = 692,
        res = "ui/mainLayer/new_ui_1/btn_battle.png",
        res_en = "ui/mainLayer/new_ui_1/btn_battle_en.png",
    },
    [693] = {
        id = 693,
        res = "icon/equipment/name/Margaret2.png",
        res_en = "icon/equipment/name/Margaret2_en.png",
    },
    [694] = {
        id = 694,
        res = "icon/item/goods/600014.png",
        res_en = "icon/item/goods/600014_en.png",
    },
    [695] = {
        id = 695,
        res = "ui/summon/036.png",
        res_en = "ui/summon/036_en.png",
    },
    [696] = {
        id = 696,
        res = "ui/Equipment/new_ui/new_49.png",
        res_en = "ui/Equipment/new_ui/new_49_en.png",
    },
    [697] = {
        id = 697,
        res = "ui/activity/christmas_sign/002.png",
        res_en = "ui/activity/christmas_sign/002_en.png",
    },
    [698] = {
        id = 698,
        res = "ui/newguy/buy_btn.png",
        res_en = "ui/newguy/buy_btn_en.png",
    },
    [699] = {
        id = 699,
        res = "ui/league/ui_using.png",
        res_en = "ui/league/ui_using_en.png",
    },
    [700] = {
        id = 700,
        res = "ui/summon/033.png",
        res_en = "ui/summon/033_en.png",
    },
    [701] = {
        id = 701,
        res = "icon/equipment/name/Noel2.png",
        res_en = "icon/equipment/name/Noel2_en.png",
    },
    [702] = {
        id = 702,
        res = "icon/equipment/name/Yinglingzhizi.png",
        res_en = "icon/equipment/name/Yinglingzhizi_en.png",
    },
    [703] = {
        id = 703,
        res = "ui/activity/kuangsan_card/enter/03.png",
        res_en = "ui/activity/kuangsan_card/enter/03_en.png",
    },
    [704] = {
        id = 704,
        res = "icon/equipment/name/Nemesis2.png",
        res_en = "icon/equipment/name/Nemesis2_en.png",
    },
    [705] = {
        id = 705,
        res = "ui/recharge/gifts/qiyue_n.png",
        res_en = "ui/recharge/gifts/qiyue_n_en.png",
    },
    [706] = {
        id = 706,
        res = "ui/mainui/main_shilian.png",
        res_en = "ui/mainui/main_shilian_en.png",
    },
    [707] = {
        id = 707,
        res = "ui/simulation_trial5/result/003.png",
        res_en = "ui/simulation_trial5/result/003_en.png",
    },
    [708] = {
        id = 708,
        res = "ui/newyear/cardGame/tianxie.png",
        res_en = "ui/newyear/cardGame/tianxie_en.png",
    },
    [709] = {
        id = 709,
        res = "ui/activity/picture/icon147.png",
        res_en = "ui/activity/picture/icon147_en.png",
    },
    [710] = {
        id = 710,
        res = "ui/playerInfo/medal/ui_04.png",
        res_en = "ui/playerInfo/medal/ui_04_en.png",
    },
    [711] = {
        id = 711,
        res = "icon/fuben/levelIcon/Team/8_guanghui.png",
        res_en = "icon/fuben/levelIcon/Team/8_guanghui_en.png",
    },
    [712] = {
        id = 712,
        res = "ui/dispatch/ui_042.png",
        res_en = "ui/dispatch/ui_042_en.png",
    },
    [713] = {
        id = 713,
        res = "ui/onlineteam/cap_3.png",
        res_en = "ui/onlineteam/cap_3_en.png",
    },
    [714] = {
        id = 714,
        res = "ui/monthcard/002.png",
        res_en = "ui/monthcard/002_en.png",
    },
    [715] = {
        id = 715,
        res = "icon/equipment/name/Juno.png",
        res_en = "icon/equipment/name/Juno_en.png",
    },
    [716] = {
        id = 716,
        res = "ui/task/double_card_tips.png",
        res_en = "ui/task/double_card_tips_en.png",
    },
    [717] = {
        id = 717,
        res = "ui/dawuwong/003.png",
        res_en = "ui/dawuwong/003_en.png",
    },
    [718] = {
        id = 718,
        res = "ui/score/allserver.png",
        res_en = "ui/score/allserver_en.png",
    },
    [719] = {
        id = 719,
        res = "icon/equipment/name/Qishijian.png",
        res_en = "icon/equipment/name/Qishijian_en.png",
    },
    [720] = {
        id = 720,
        res = "icon/item/goods/600002.png",
        res_en = "icon/item/goods/600002_en.png",
    },
    [721] = {
        id = 721,
        res = "ui/mainLayer/new_ui_1/btn_dating.png",
        res_en = "ui/mainLayer/new_ui_1/btn_dating_en.png",
    },
    [722] = {
        id = 722,
        res = "ui/summon/simulation/dh.png",
        res_en = "ui/summon/simulation/dh_en.png",
    },
    [723] = {
        id = 723,
        res = "icon/item/goods/600001.png",
        res_en = "icon/item/goods/600001_en.png",
    },
    [724] = {
        id = 724,
        res = "icon/item/goods/600015.png",
        res_en = "icon/item/goods/600015_en.png",
    },
    [725] = {
        id = 725,
        res = "icon/equipment/suit/kuanshuzhiren.png",
        res_en = "icon/equipment/suit/kuanshuzhiren_en.png",
    },
    [726] = {
        id = 726,
        res = "ui/update/s4.png",
        res_en = "ui/update/s4_en.png",
    },
    [727] = {
        id = 727,
        res = "icon/equipment/name/Saozhounvwu.png",
        res_en = "icon/equipment/name/Saozhounvwu_en.png",
    },
    [728] = {
        id = 728,
        res = "ui/activity/picture/ad104.png",
        res_en = "ui/activity/picture/ad104_en.png",
    },
    [729] = {
        id = 729,
        res = "ui/newyear/cardGame/shuangzi.png",
        res_en = "ui/newyear/cardGame/shuangzi_en.png",
    },
    [730] = {
        id = 730,
        res = "ui/activity/BlackAndWhite/enter/004.png",
        res_en = "ui/activity/BlackAndWhite/enter/004_en.png",
    },
    [731] = {
        id = 731,
        res = "ui/fairy/new_ui/element/img_01.png",
        res_en = "ui/fairy/new_ui/element/img_01_en.png",
    },
    [732] = {
        id = 732,
        res = "icon/equipment/name/Circe.png",
        res_en = "icon/equipment/name/Circe_en.png",
    },
    [733] = {
        id = 733,
        res = "ui/ChronoCros/main/bg3.png",
        res_en = "ui/ChronoCros/main/bg3_en.png",
    },
    [734] = {
        id = 734,
        res = "ui/activity/activityScale9/got_get_big.png",
        res_en = "ui/activity/activityScale9/got_get_big_en.png",
    },
    [735] = {
        id = 735,
        res = "ui/dating/skipVideo.png",
        res_en = "ui/dating/skipVideo_en.png",
    },
    [736] = {
        id = 736,
        res = "ui/oneyearChare/110201.png",
        res_en = "ui/oneyearChare/110201_en.png",
    },
    [737] = {
        id = 737,
        res = "icon/equipment/name/Chixiaozhiyong.png",
        res_en = "icon/equipment/name/Chixiaozhiyong_en.png",
    },
    [738] = {
        id = 738,
        res = "icon/fuben/009_1.png",
        res_en = "icon/fuben/009_1_en.png",
    },
    [739] = {
        id = 739,
        res = "icon/equipment/suit/tianshenzhijian.png",
        res_en = "icon/equipment/suit/tianshenzhijian_en.png",
    },
    [740] = {
        id = 740,
        res = "ui/activity/sevenEx/021.png",
        res_en = "ui/activity/sevenEx/021_en.png",
    },
    [741] = {
        id = 741,
        res = "ui/newyear/cardGame/baiyang.png",
        res_en = "ui/newyear/cardGame/baiyang_en.png",
    },
    [742] = {
        id = 742,
        res = "ui/activity/midAutumn/achievement/004.png",
        res_en = "ui/activity/midAutumn/achievement/004_en.png",
    },
    [743] = {
        id = 743,
        res = "ui/activity/welfareActivity/002.png",
        res_en = "ui/activity/welfareActivity/002_en.png",
    },
    [744] = {
        id = 744,
        res = "ui/dating/unLock/001.png",
        res_en = "ui/dating/unLock/001_en.png",
    },
    [745] = {
        id = 745,
        res = "ui/oneyearChare/111001.png",
        res_en = "ui/oneyearChare/111001_en.png",
    },
    [746] = {
        id = 746,
        res = "icon/equipment/name/Shengdanxixian.png",
        res_en = "icon/equipment/name/Shengdanxixian_en.png",
    },
    [747] = {
        id = 747,
        res = "ui/mainLayer/new_ui/btn_newPlayer.png",
        res_en = "ui/mainLayer/new_ui/btn_newPlayer_en.png",
    },
    [748] = {
        id = 748,
        res = "ui/update/s25.png",
        res_en = "ui/update/s25_en.png",
    },
    [749] = {
        id = 749,
        res = "ui/oneyearChare/110801.png",
        res_en = "ui/oneyearChare/110801_en.png",
    },
    [750] = {
        id = 750,
        res = "icon/equipment/name/Zuosuimiyou.png",
        res_en = "icon/equipment/name/Zuosuimiyou_en.png",
    },
    [751] = {
        id = 751,
        res = "ui/summon/elf_contract/001.png",
        res_en = "ui/summon/elf_contract/001_en.png",
    },
    [752] = {
        id = 752,
        res = "ui/update/s6.png",
        res_en = "ui/update/s6_en.png",
    },
    [753] = {
        id = 753,
        res = "icon/equipType2/Binah_gray.png",
        res_en = "icon/equipType2/Binah_gray_en.png",
    },
    [754] = {
        id = 754,
        res = "icon/equipment/suit/xinshengshiyue.png",
        res_en = "icon/equipment/suit/xinshengshiyue_en.png",
    },
    [755] = {
        id = 755,
        res = "icon/equipment/name/Circe2.png",
        res_en = "icon/equipment/name/Circe2_en.png",
    },
    [756] = {
        id = 756,
        res = "ui/iphoneX/roleTeach/teach/009.png",
        res_en = "ui/iphoneX/roleTeach/teach/009_en.png",
    },
    [757] = {
        id = 757,
        res = "ui/summon/053.png",
        res_en = "ui/summon/053_en.png",
    },
    [758] = {
        id = 758,
        res = "icon/equipment/name/Hilska.png",
        res_en = "icon/equipment/name/Hilska_en.png",
    },
    [759] = {
        id = 759,
        res = "icon/item/goods/529012.png",
        res_en = "icon/item/goods/529012_en.png",
    },
    [760] = {
        id = 760,
        res = "ui/activity/courage/map/param/BG.png",
        res_en = "ui/activity/courage/map/param/BG_en.png",
    },
    [761] = {
        id = 761,
        res = "ui/recharge/pop/b3.png",
        res_en = "ui/recharge/pop/b3_en.png",
    },
    [762] = {
        id = 762,
        res = "ui/activity/picture/icon62.png",
        res_en = "ui/activity/picture/icon62_en.png",
    },
    [763] = {
        id = 763,
        res = "icon/equipment/name/Diana.png",
        res_en = "icon/equipment/name/Diana_en.png",
    },
    [764] = {
        id = 764,
        res = "icon/fuben/011_1.png",
        res_en = "icon/fuben/011_1_en.png",
    },
    [765] = {
        id = 765,
        res = "icon/equipment/suit/shenpanzhiqiang.png",
        res_en = "icon/equipment/suit/shenpanzhiqiang_en.png",
    },
    [766] = {
        id = 766,
        res = "ui/activity/picture/ad136.png",
        res_en = "ui/activity/picture/ad136_en.png",
    },
    [767] = {
        id = 767,
        res = "ui/activity/picture/ad125.png",
        res_en = "ui/activity/picture/ad125_en.png",
    },
    [768] = {
        id = 768,
        res = "icon/equipment/name/Qinxinzhiyue.png",
        res_en = "icon/equipment/name/Qinxinzhiyue_en.png",
    },
    [769] = {
        id = 769,
        res = "ui/activity/activityScale9/check_check_small.png",
        res_en = "ui/activity/activityScale9/check_check_small_en.png",
    },
    [770] = {
        id = 770,
        res = "ui/dfwautumn/main/card.png",
        res_en = "ui/dfwautumn/main/card_en.png",
    },
    [771] = {
        id = 771,
        res = "ui/mainLayer3/j10bg_test.png",
        res_en = "ui/mainLayer3/j10bg_test_en.png",
    },
    [772] = {
        id = 772,
        res = "ui/summon/009.png",
        res_en = "ui/summon/009_en.png",
    },
    [773] = {
        id = 773,
        res = "icon/fuben/007.png",
        res_en = "icon/fuben/007_en.png",
    },
    [774] = {
        id = 774,
        res = "ui/share/bg.png",
        res_en = "ui/share/bg_en.png",
    },
    [775] = {
        id = 775,
        res = "ui/activity/courage/game/light/f3.png",
        res_en = "ui/activity/courage/game/light/f3_en.png",
    },
    [776] = {
        id = 776,
        res = "icon/equipment/name/Shengdanyejushi.png",
        res_en = "icon/equipment/name/Shengdanyejushi_en.png",
    },
    [777] = {
        id = 777,
        res = "icon/equipment/name/Pomozhizhong.png",
        res_en = "icon/equipment/name/Pomozhizhong_en.png",
    },
    [778] = {
        id = 778,
        res = "icon/fuben/005_1.png",
        res_en = "icon/fuben/005_1_en.png",
    },
    [779] = {
        id = 779,
        res = "ui/fairy/new_ui/new_50.png",
        res_en = "ui/fairy/new_ui/new_50_en.png",
    },
    [780] = {
        id = 780,
        res = "icon/equipment/name/Christina2.png",
        res_en = "icon/equipment/name/Christina2_en.png",
    },
    [781] = {
        id = 781,
        res = "icon/equipment/name/Qianmianzhifa.png",
        res_en = "icon/equipment/name/Qianmianzhifa_en.png",
    },
    [782] = {
        id = 782,
        res = "ui/activity/picture/icon86.png",
        res_en = "ui/activity/picture/icon86_en.png",
    },
    [783] = {
        id = 783,
        res = "icon/item/goods/570204.png",
        res_en = "icon/item/goods/570204_en.png",
    },
    [784] = {
        id = 784,
        res = "ui/activity/picture/ad20.png",
        res_en = "ui/activity/picture/ad20_en.png",
    },
    [785] = {
        id = 785,
        res = "ui/recharge/gifts/teqing_s.png",
        res_en = "ui/recharge/gifts/teqing_s_en.png",
    },
    [786] = {
        id = 786,
        res = "ui/simulation_trial5/002.png",
        res_en = "ui/simulation_trial5/002_en.png",
    },
    [787] = {
        id = 787,
        res = "icon/equipment/name/Hidekatsu.png",
        res_en = "icon/equipment/name/Hidekatsu_en.png",
    },
    [788] = {
        id = 788,
        res = "ui/update/s11.png",
        res_en = "ui/update/s11_en.png",
    },
    [789] = {
        id = 789,
        res = "ui/newyear/23.png",
        res_en = "ui/newyear/23_en.png",
    },
    [790] = {
        id = 790,
        res = "ui/newyear/cardGame/shuipin.png",
        res_en = "ui/newyear/cardGame/shuipin_en.png",
    },
    [791] = {
        id = 791,
        res = "icon/equipment/name/Guinevere.png",
        res_en = "icon/equipment/name/Guinevere_en.png",
    },
    [792] = {
        id = 792,
        res = "icon/item/goods/570516.png",
        res_en = "icon/item/goods/570516_en.png",
    },
    [793] = {
        id = 793,
        res = "ui/activity/bingoGame/enter/logo.png",
        res_en = "ui/activity/bingoGame/enter/logo_en.png",
    },
    [794] = {
        id = 794,
        res = "ui/activity/picture/icon87.png",
        res_en = "ui/activity/picture/icon87_en.png",
    },
    [795] = {
        id = 795,
        res = "ui/fuben/linkage/015.png",
        res_en = "ui/fuben/linkage/015_en.png",
    },
    [796] = {
        id = 796,
        res = "ui/mainLayer3/c22.png",
        res_en = "ui/mainLayer3/c22_en.png",
    },
    [797] = {
        id = 797,
        res = "icon/item/goods/570502.png",
        res_en = "icon/item/goods/570502_en.png",
    },
    [798] = {
        id = 798,
        res = "ui/league/ui_44.png",
        res_en = "ui/league/ui_44_en.png",
    },
    [799] = {
        id = 799,
        res = "ui/Equipment/new_ui/new_52.png",
        res_en = "ui/Equipment/new_ui/new_52_en.png",
    },
    [800] = {
        id = 800,
        res = "ui/fairy_up/jingjie.png",
        res_en = "ui/fairy_up/jingjie_en.png",
    },
    [801] = {
        id = 801,
        res = "ui/activity/lanternFestival/entrance/002.png",
        res_en = "ui/activity/lanternFestival/entrance/002_en.png",
    },
    [802] = {
        id = 802,
        res = "ui/activity/bingoGame/enter/abg.png",
        res_en = "ui/activity/bingoGame/enter/abg_en.png",
    },
    [803] = {
        id = 803,
        res = "ui/activity/clothese_summon/004.png",
        res_en = "ui/activity/clothese_summon/004_en.png",
    },
    [804] = {
        id = 804,
        res = "ui/activity/picture/ad6.png",
        res_en = "ui/activity/picture/ad6_en.png",
    },
    [805] = {
        id = 805,
        res = "ui/dispatch/ui_0100.png",
        res_en = "ui/dispatch/ui_0100_en.png",
    },
    [806] = {
        id = 806,
        res = "icon/equipment/name/Nemesis.png",
        res_en = "icon/equipment/name/Nemesis_en.png",
    },
    [807] = {
        id = 807,
        res = "icon/equipment/name/Theresia.png",
        res_en = "icon/equipment/name/Theresia_en.png",
    },
    [808] = {
        id = 808,
        res = "ui/activity/picture/ad48.png",
        res_en = "ui/activity/picture/ad48_en.png",
    },
    [809] = {
        id = 809,
        res = "ui/setting/uires/33.png",
        res_en = "ui/setting/uires/33_en.png",
    },
    [810] = {
        id = 810,
        res = "ui/recharge/gifts/yueka_s.png",
        res_en = "ui/recharge/gifts/yueka_s_en.png",
    },
    [811] = {
        id = 811,
        res = "ui/activity/newYear/mainlayer/025.png",
        res_en = "ui/activity/newYear/mainlayer/025_en.png",
    },
    [812] = {
        id = 812,
        res = "icon/fuben/levelIcon/Team/9_jichu_1.png",
        res_en = "icon/fuben/levelIcon/Team/9_jichu_1_en.png",
    },
    [813] = {
        id = 813,
        res = "ui/fairy/new_ui/new_29.png",
        res_en = "ui/fairy/new_ui/new_29_en.png",
    },
    [814] = {
        id = 814,
        res = "ui/activity/picture/icon2.png",
        res_en = "ui/activity/picture/icon2_en.png",
    },
    [815] = {
        id = 815,
        res = "icon/equipment/suit/suiyuelizan.png",
        res_en = "icon/equipment/suit/suiyuelizan_en.png",
    },
    [816] = {
        id = 816,
        res = "ui/activity/picture/ad31.png",
        res_en = "ui/activity/picture/ad31_en.png",
    },
    [817] = {
        id = 817,
        res = "ui/newyear/cardGame/shuipin2.png",
        res_en = "ui/newyear/cardGame/shuipin2_en.png",
    },
    [818] = {
        id = 818,
        res = "ui/summon/040.png",
        res_en = "ui/summon/040_en.png",
    },
    [819] = {
        id = 819,
        res = "icon/equipment/name/Valkyrie2.png",
        res_en = "icon/equipment/name/Valkyrie2_en.png",
    },
    [820] = {
        id = 820,
        res = "ui/activity/picture/icon123.png",
        res_en = "ui/activity/picture/icon123_en.png",
    },
    [821] = {
        id = 821,
        res = "ui/new_equip/025.png",
        res_en = "ui/new_equip/025_en.png",
    },
    [822] = {
        id = 822,
        res = "ui/Evaluate/btn_ok.png",
        res_en = "ui/Evaluate/btn_ok_en.png",
    },
    [823] = {
        id = 823,
        res = "ui/fuben/wanyouli/chapter_2.png",
        res_en = "ui/fuben/wanyouli/chapter_2_en.png",
    },
    [824] = {
        id = 824,
        res = "ui/makefood/make/014.png",
        res_en = "ui/makefood/make/014_en.png",
    },
    [825] = {
        id = 825,
        res = "ui/fuben/rule/007.png",
        res_en = "ui/fuben/rule/007_en.png",
    },
    [826] = {
        id = 826,
        res = "ui/recharge/gifts/remai_n.png",
        res_en = "ui/recharge/gifts/remai_n_en.png",
    },
    [827] = {
        id = 827,
        res = "ui/monthcardNew1/bg2.png",
        res_en = "ui/monthcardNew1/bg2_en.png",
    },
    [828] = {
        id = 828,
        res = "ui/chat/jinru.png",
        res_en = "ui/chat/jinru_en.png",
    },
    [829] = {
        id = 829,
        res = "ui/update/s14.png",
        res_en = "ui/update/s14_en.png",
    },
    [830] = {
        id = 830,
        res = "icon/item/goods/600006.png",
        res_en = "icon/item/goods/600006_en.png",
    },
    [831] = {
        id = 831,
        res = "icon/fuben/levelIcon/Team/9_jichu_2.png",
        res_en = "icon/fuben/levelIcon/Team/9_jichu_2_en.png",
    },
    [832] = {
        id = 832,
        res = "ui/activity/picture/ad14.png",
        res_en = "ui/activity/picture/ad14_en.png",
    },
    [833] = {
        id = 833,
        res = "icon/fuben/dungeon_daliangshan.png",
        res_en = "icon/fuben/dungeon_daliangshan_en.png",
    },
    [834] = {
        id = 834,
        res = "ui/simulation_trial/icon4.png",
        res_en = "ui/simulation_trial/icon4_en.png",
    },
    [835] = {
        id = 835,
        res = "icon/equipment/suit/chiyanchongji.png",
        res_en = "icon/equipment/suit/chiyanchongji_en.png",
    },
    [836] = {
        id = 836,
        res = "ui/summon/048.png",
        res_en = "ui/summon/048_en.png",
    },
    [837] = {
        id = 837,
        res = "ui/battle/n069.png",
        res_en = "ui/battle/n069_en.png",
    },
    [838] = {
        id = 838,
        res = "ui/newguy/leftbg.png",
        res_en = "ui/newguy/leftbg_en.png",
    },
    [839] = {
        id = 839,
        res = "ui/activity/newguy_summon/002.png",
        res_en = "ui/activity/newguy_summon/002_en.png",
    },
    [840] = {
        id = 840,
        res = "ui/simulation_trial2/bg5.png",
        res_en = "ui/simulation_trial2/bg5_en.png",
    },
    [841] = {
        id = 841,
        res = "ui/activity/picture/icon23.png",
        res_en = "ui/activity/picture/icon23_en.png",
    },
    [842] = {
        id = 842,
        res = "ui/fuben/linkage/012.png",
        res_en = "ui/fuben/linkage/012_en.png",
    },
    [843] = {
        id = 843,
        res = "ui/summon/037.png",
        res_en = "ui/summon/037_en.png",
    },
    [844] = {
        id = 844,
        res = "ui/activity/valentine/001.png",
        res_en = "ui/activity/valentine/001_en.png",
    },
    [845] = {
        id = 845,
        res = "ui/dawuwong/001.png",
        res_en = "ui/dawuwong/001_en.png",
    },
    [846] = {
        id = 846,
        res = "icon/item/goods/600013.png",
        res_en = "icon/item/goods/600013_en.png",
    },
    [847] = {
        id = 847,
        res = "icon/item/goods/600012.png",
        res_en = "icon/item/goods/600012_en.png",
    },
    [848] = {
        id = 848,
        res = "ui/dispatch/ui_0101.png",
        res_en = "ui/dispatch/ui_0101_en.png",
    },
    [849] = {
        id = 849,
        res = "icon/equipment/name/Theresia2.png",
        res_en = "icon/equipment/name/Theresia2_en.png",
    },
    [850] = {
        id = 850,
        res = "icon/equipment/name/Nvpukuangsan.png",
        res_en = "icon/equipment/name/Nvpukuangsan_en.png",
    },
    [851] = {
        id = 851,
        res = "ui/activity/concoctDrinks/3.png",
        res_en = "ui/activity/concoctDrinks/3_en.png",
    },
    [852] = {
        id = 852,
        res = "ui/summon/043.png",
        res_en = "ui/summon/043_en.png",
    },
    [853] = {
        id = 853,
        res = "ui/mainLayer/new_ui/a5.png",
        res_en = "ui/mainLayer/new_ui/a5_en.png",
    },
    [854] = {
        id = 854,
        res = "ui/activity/ChronoCross/a2.png",
        res_en = "ui/activity/ChronoCross/a2_en.png",
    },
    [855] = {
        id = 855,
        res = "ui/summon/034.png",
        res_en = "ui/summon/034_en.png",
    },
    [856] = {
        id = 856,
        res = "ui/score/person.png",
        res_en = "ui/score/person_en.png",
    },
    [857] = {
        id = 857,
        res = "ui/newyear/cardGame/baiyang2.png",
        res_en = "ui/newyear/cardGame/baiyang2_en.png",
    },
    [858] = {
        id = 858,
        res = "icon/equipment/name/Minerva.png",
        res_en = "icon/equipment/name/Minerva_en.png",
    },
    [859] = {
        id = 859,
        res = "ui/activity/whiteValentine/223.png",
        res_en = "ui/activity/whiteValentine/223_en.png",
    },
    [860] = {
        id = 860,
        res = "ui/activity/picture/ad21.png",
        res_en = "ui/activity/picture/ad21_en.png",
    },
    [861] = {
        id = 861,
        res = "icon/teamHelp/007.png",
        res_en = "icon/teamHelp/007_en.png",
    },
    [862] = {
        id = 862,
        res = "icon/fuben/levelIcon/Team/7_shengli_2.png",
        res_en = "icon/fuben/levelIcon/Team/7_shengli_2_en.png",
    },
    [863] = {
        id = 863,
        res = "ui/fairy/new_ui/new_09.png",
        res_en = "ui/fairy/new_ui/new_09_en.png",
    },
    [864] = {
        id = 864,
        res = "icon/mainDating/mainName/1.png",
        res_en = "icon/mainDating/mainName/1_en.png",
    },
    [865] = {
        id = 865,
        res = "ui/activity/courage/game/light/bg.png",
        res_en = "ui/activity/courage/game/light/bg_en.png",
    },
    [866] = {
        id = 866,
        res = "icon/item/stuff/532109.png",
        res_en = "icon/item/stuff/532109_en.png",
    },
    [867] = {
        id = 867,
        res = "ui/summon/038.png",
        res_en = "ui/summon/038_en.png",
    },
    [868] = {
        id = 868,
        res = "ui/mainLayer/new_ui/a4.png",
        res_en = "ui/mainLayer/new_ui/a4_en.png",
    },
    [869] = {
        id = 869,
        res = "icon/equipment/name/Astaroth.png",
        res_en = "icon/equipment/name/Astaroth_en.png",
    },
    [870] = {
        id = 870,
        res = "ui/activity/oneYear/card/006.png",
        res_en = "ui/activity/oneYear/card/006_en.png",
    },
    [871] = {
        id = 871,
        res = "icon/item/goods/528002.png",
        res_en = "icon/item/goods/528002_en.png",
    },
    [872] = {
        id = 872,
        res = "ui/fuli/seven_sign/001.png",
        res_en = "ui/fuli/seven_sign/001_en.png",
    },
    [873] = {
        id = 873,
        res = "ui/kabalatree/skip2.png",
        res_en = "ui/kabalatree/skip2_en.png",
    },
    [874] = {
        id = 874,
        res = "icon/item/goods/600010.png",
        res_en = "icon/item/goods/600010_en.png",
    },
    [875] = {
        id = 875,
        res = "ui/role/newRoleShow/ZY_KBN_icon_2.png",
        res_en = "ui/role/newRoleShow/ZY_KBN_icon_2_en.png",
    },
    [876] = {
        id = 876,
        res = "ui/fuben/rule/005_1.png",
        res_en = "ui/fuben/rule/005_1_en.png",
    },
    [877] = {
        id = 877,
        res = "icon/equipment/name/Lalatis.png",
        res_en = "icon/equipment/name/Lalatis_en.png",
    },
    [878] = {
        id = 878,
        res = "ui/fairy/new_ui/energy/ui_002.png",
        res_en = "ui/fairy/new_ui/energy/ui_002_en.png",
    },
    [879] = {
        id = 879,
        res = "icon/equipment/name/Luna2.png",
        res_en = "icon/equipment/name/Luna2_en.png",
    },
    [880] = {
        id = 880,
        res = "icon/item/spring/006.png",
        res_en = "icon/item/spring/006_en.png",
    },
    [881] = {
        id = 881,
        res = "ui/activity/activity_btn/btn_1.png",
        res_en = "ui/activity/activity_btn/btn_1_en.png",
    },
    [882] = {
        id = 882,
        res = "icon/equipment/name/Hilska2.png",
        res_en = "icon/equipment/name/Hilska2_en.png",
    },
    [883] = {
        id = 883,
        res = "icon/item/spring/005.png",
        res_en = "icon/item/spring/005_en.png",
    },
    [884] = {
        id = 884,
        res = "ui/activity/lanternFestival/achievement/008.png",
        res_en = "ui/activity/lanternFestival/achievement/008_en.png",
    },
    [885] = {
        id = 885,
        res = "ui/tujian/icon_show.png",
        res_en = "ui/tujian/icon_show_en.png",
    },
    [886] = {
        id = 886,
        res = "ui/common/reward_top.png",
        res_en = "ui/common/reward_top_en.png",
    },
    [887] = {
        id = 887,
        res = "icon/equipment/name/Raphael.png",
        res_en = "icon/equipment/name/Raphael_en.png",
    },
    [888] = {
        id = 888,
        res = "ui/common/huodong.png",
        res_en = "ui/common/huodong_en.png",
    },
    [889] = {
        id = 889,
        res = "ui/activity/znq_huigu/002.png",
        res_en = "ui/activity/znq_huigu/002_en.png",
    },
    [890] = {
        id = 890,
        res = "ui/update/s13.png",
        res_en = "ui/update/s13_en.png",
    },
    [891] = {
        id = 891,
        res = "ui/share/share.png",
        res_en = "ui/share/share_en.png",
    },
    [892] = {
        id = 892,
        res = "ui/activity/courage/game/light/f1.png",
        res_en = "ui/activity/courage/game/light/f1_en.png",
    },
    [893] = {
        id = 893,
        res = "ui/newCity/dating_result/031.png",
        res_en = "ui/newCity/dating_result/031_en.png",
    },
    [894] = {
        id = 894,
        res = "ui/activity/picture/ad111.png",
        res_en = "ui/activity/picture/ad111_en.png",
    },
    [895] = {
        id = 895,
        res = "ui/activity/picture/ad7.png",
        res_en = "ui/activity/picture/ad7_en.png",
    },
    [896] = {
        id = 896,
        res = "ui/activity/picture/ad40.png",
        res_en = "ui/activity/picture/ad40_en.png",
    },
    [897] = {
        id = 897,
        res = "ui/activity/courage/enter/012.png",
        res_en = "ui/activity/courage/enter/012_en.png",
    },
    [898] = {
        id = 898,
        res = "ui/activity/power/3.png",
        res_en = "ui/activity/power/3_en.png",
    },
    [899] = {
        id = 899,
        res = "ui/activity/activityMain3/bg4.png",
        res_en = "ui/activity/activityMain3/bg4_en.png",
    },
    [900] = {
        id = 900,
        res = "ui/activity/picture/icon65.png",
        res_en = "ui/activity/picture/icon65_en.png",
    },
    [901] = {
        id = 901,
        res = "ui/newyear/cardGame/shuangyu.png",
        res_en = "ui/newyear/cardGame/shuangyu_en.png",
    },
    [902] = {
        id = 902,
        res = "ui/update/s24.png",
        res_en = "ui/update/s24_en.png",
    },
    [903] = {
        id = 903,
        res = "ui/activity/picture/icon13.png",
        res_en = "ui/activity/picture/icon13_en.png",
    },
    [904] = {
        id = 904,
        res = "ui/activity/summonActivity/cover0.png",
        res_en = "ui/activity/summonActivity/cover0_en.png",
    },
    [905] = {
        id = 905,
        res = "ui/newyear/cardGame/sheshou2.png",
        res_en = "ui/newyear/cardGame/sheshou2_en.png",
    },
    [906] = {
        id = 906,
        res = "ui/recharge/month_card_1.png",
        res_en = "ui/recharge/month_card_1_en.png",
    },
    [907] = {
        id = 907,
        res = "icon/equipment/suit/qiangweifengbao.png",
        res_en = "icon/equipment/suit/qiangweifengbao_en.png",
    },
    [908] = {
        id = 908,
        res = "ui/activity/picture/icon55.png",
        res_en = "ui/activity/picture/icon55_en.png",
    },
    [909] = {
        id = 909,
        res = "icon/chatEmotion/017.png",
        res_en = "icon/chatEmotion/017_en.png",
    },
    [910] = {
        id = 910,
        res = "icon/equipment/suit/daodanpaidui.png",
        res_en = "icon/equipment/suit/daodanpaidui_en.png",
    },
    [911] = {
        id = 911,
        res = "ui/guide/15.png",
        res_en = "ui/guide/15_en.png",
    },
    [912] = {
        id = 912,
        res = "ui/activity/chunriji/001.png",
        res_en = "ui/activity/chunriji/001_en.png",
    },
    [913] = {
        id = 913,
        res = "ui/summon/041.png",
        res_en = "ui/summon/041_en.png",
    },
    [914] = {
        id = 914,
        res = "icon/equipment/name/Shengdansisinai.png",
        res_en = "icon/equipment/name/Shengdansisinai_en.png",
    },
    [915] = {
        id = 915,
        res = "ui/simulation_trial2/bg3.png",
        res_en = "ui/simulation_trial2/bg3_en.png",
    },
    [916] = {
        id = 916,
        res = "ui/summon/noob.png",
        res_en = "ui/summon/noob_en.png",
    },
    [917] = {
        id = 917,
        res = "ui/activity/whiteValentine/m2.png",
        res_en = "ui/activity/whiteValentine/m2_en.png",
    },
    [918] = {
        id = 918,
        res = "icon/item/goods/529031.png",
        res_en = "icon/item/goods/529031_en.png",
    },
    [919] = {
        id = 919,
        res = "ui/iphoneX/roleTeach/teach/014.png",
        res_en = "ui/iphoneX/roleTeach/teach/014_en.png",
    },
    [920] = {
        id = 920,
        res = "ui/dalmap/event/wenan_5.png",
        res_en = "ui/dalmap/event/wenan_5_en.png",
    },
    [921] = {
        id = 921,
        res = "ui/monthcard/003.png",
        res_en = "ui/monthcard/003_en.png",
    },
    [922] = {
        id = 922,
        res = "ui/fight_result/reward_rise.png",
        res_en = "ui/fight_result/reward_rise_en.png",
    },
    [923] = {
        id = 923,
        res = "ui/activity/picture/icon14.png",
        res_en = "ui/activity/picture/icon14_en.png",
    },
    [924] = {
        id = 924,
        res = "ui/activity/picture/icon15.png",
        res_en = "ui/activity/picture/icon15_en.png",
    },
    [925] = {
        id = 925,
        res = "icon/equipType2/Chesed.png",
        res_en = "icon/equipType2/Chesed_en.png",
    },
    [926] = {
        id = 926,
        res = "icon/equipType2/Chesed_gray.png",
        res_en = "icon/equipType2/Chesed_gray_en.png",
    },
    [927] = {
        id = 927,
        res = "icon/equipType2/Geburah.png",
        res_en = "icon/equipType2/Geburah_en.png",
    },
    [928] = {
        id = 928,
        res = "icon/equipType2/Geburah_gray.png",
        res_en = "icon/equipType2/Geburah_gray_en.png",
    },
    [929] = {
        id = 929,
        res = "icon/equipType2/Kether.png",
        res_en = "icon/equipType2/Kether_en.png",
    },
    [930] = {
        id = 930,
        res = "icon/equipType2/Kether_gray.png",
        res_en = "icon/equipType2/Kether_gray_en.png",
    },
    [931] = {
        id = 931,
        res = "icon/equipType2/Malkuth.png",
        res_en = "icon/equipType2/Malkuth_en.png",
    },
    [932] = {
        id = 932,
        res = "icon/equipType2/Malkuth_gray.png",
        res_en = "icon/equipType2/Malkuth_gray_en.png",
    },
    [933] = {
        id = 933,
        res = "icon/equipType2/Tiphareth.png",
        res_en = "icon/equipType2/Tiphareth_en.png",
    },
    [934] = {
        id = 934,
        res = "icon/equipType2/Tiphareth_gray.png",
        res_en = "icon/equipType2/Tiphareth_gray_en.png",
    },
    [935] = {
        id = 935,
        res = "ui/mainLayer/new_ui/btn_shop_a.png",
        res_en = "ui/mainLayer/new_ui/btn_shop_a_en.png",
    },
    [936] = {
        id = 936,
        res = "ui/mainLayer/new_ui/btn_shop_b.png",
        res_en = "ui/mainLayer/new_ui/btn_shop_b_en.png",
    },
    [937] = {
        id = 937,
        res = "ui/mainLayer/new_ui/btn_recharge_a.png",
        res_en = "ui/mainLayer/new_ui/btn_recharge_a_en.png",
    },
    [938] = {
        id = 938,
        res = "ui/mainLayer/new_ui/btn_recharge_b.png",
        res_en = "ui/mainLayer/new_ui/btn_recharge_b_en.png",
    },
    [939] = {
        id = 939,
        res = "ui/mainLayer/new_ui/btn_recharge_c.png",
        res_en = "ui/mainLayer/new_ui/btn_recharge_c_en.png",
    },
    [940] = {
        id = 940,
        res = "ui/mainLayer/new_ui/ad/btn_gift.png",
        res_en = "ui/mainLayer/new_ui/ad/btn_gift_en.png",
    },
    [941] = {
        id = 941,
        res = "ui/activity/crazyDiamond/crazyDiamond_7.png",
        res_en = "ui/activity/crazyDiamond/crazyDiamond_7_en.png",
    },
    [942] = {
        id = 942,
        res = "ui/activity/crazyDiamond/crazyDiamond_9.png",
        res_en = "ui/activity/crazyDiamond/crazyDiamond_9_en.png",
    },
    [943] = {
        id = 943,
        res = "ui/activity/crazyDiamond/crazyDiamond_14.png",
        res_en = "ui/activity/crazyDiamond/crazyDiamond_14_en.png",
    },
    [944] = {
        id = 944,
        res = "ui/activity/picture/icon300.png",
        res_en = "ui/activity/picture/icon300_en.png",
    },
    [945] = {
        id = 945,
        res = "ui/activity/picture/icon301.png",
        res_en = "ui/activity/picture/icon301_en.png",
    },
    [946] = {
        id = 946,
        res = "ui/collect/TJ_EQUIP_NULL.png",
        res_en = "ui/collect/TJ_EQUIP_NULL_en.png",
    },
    [947] = {
        id = 947,
        res = "ui/activity/turtableActivity/bg1.png",
        res_en = "ui/activity/turtableActivity/bg1_en.png",
    },
    [948] = {
        id = 948,
        res = "ui/dfwNew/ui_dfw_ad.png",
        res_en = "ui/dfwNew/ui_dfw_ad_en.png",
    },
    [949] = {
        id = 949,
        res = "ui/dfwNew/ui_dfw_ad_button.png",
        res_en = "ui/dfwNew/ui_dfw_ad_button_en.png",
    },
    [950] = {
        id = 950,
        res = "ui/dfwNew/ui_dfw_ad_title.png",
        res_en = "ui/dfwNew/ui_dfw_ad_title_en.png",
    },
    [951] = {
        id = 951,
        res = "ui/dfwNew/ui_dfw_logo.png",
        res_en = "ui/dfwNew/ui_dfw_logo_en.png",
    },
    [952] = {
        id = 952,
        res = "icon/equipment/name/Zhanchangzhihua.png",
        res_en = "icon/equipment/name/Zhanchangzhihua_en.png",
    },
    [953] = {
        id = 953,
        res = "icon/equipment/name/Zhihuishuangzhan.png",
        res_en = "icon/equipment/name/Zhihuishuangzhan_en.png",
    },
    [954] = {
        id = 954,
        res = "icon/equipment/name/Qianghuawuzhaung.png",
        res_en = "icon/equipment/name/Qianghuawuzhaung_en.png",
    },
    [955] = {
        id = 955,
        res = "icon/equipment/name/Aerfeiliya.png",
        res_en = "icon/equipment/name/Aerfeiliya_en.png",
    },
    [956] = {
        id = 956,
        res = "icon/equipment/name/Beiminzhiwu.png",
        res_en = "icon/equipment/name/Beiminzhiwu_en.png",
    },
    [957] = {
        id = 957,
        res = "icon/equipment/name/bupo.png",
        res_en = "icon/equipment/name/bupo_en.png",
    },
    [958] = {
        id = 958,
        res = "icon/equipment/name/chunbai.png",
        res_en = "icon/equipment/name/chunbai_en.png",
    },
    [959] = {
        id = 959,
        res = "icon/equipment/name/cuilv.png",
        res_en = "icon/equipment/name/cuilv_en.png",
    },
    [960] = {
        id = 960,
        res = "icon/equipment/name/Cxiaodiao.png",
        res_en = "icon/equipment/name/Cxiaodiao_en.png",
    },
    [961] = {
        id = 961,
        res = "icon/equipment/name/Ddadiao.png",
        res_en = "icon/equipment/name/Ddadiao_en.png",
    },
    [962] = {
        id = 962,
        res = "icon/equipment/name/Duolisi.png",
        res_en = "icon/equipment/name/Duolisi.png",
    },
    [963] = {
        id = 963,
        res = "icon/equipment/name/fanzhuanzhezhi1.png",
        res_en = "icon/equipment/name/fanzhuanzhezhi1_en.png",
    },
    [964] = {
        id = 964,
        res = "icon/equipment/name/fanzhuanzhezhi2.png",
        res_en = "icon/equipment/name/fanzhuanzhezhi2_en.png",
    },
    [965] = {
        id = 965,
        res = "icon/equipment/name/fanzhuanzhezhi3.png",
        res_en = "icon/equipment/name/fanzhuanzhezhi3_en.png",
    },
    [966] = {
        id = 966,
        res = "icon/equipment/name/ganzi.png",
        res_en = "icon/equipment/name/ganzi_en.png",
    },
    [967] = {
        id = 967,
        res = "icon/equipment/name/Guangmingtiannv.png",
        res_en = "icon/equipment/name/Guangmingtiannv_en.png",
    },
    [968] = {
        id = 968,
        res = "icon/equipment/name/Laikexier.png",
        res_en = "icon/equipment/name/Laikexier_en.png",
    },
    [969] = {
        id = 969,
        res = "icon/equipment/name/mixue.png",
        res_en = "icon/equipment/name/mixue_en.png",
    },
    [970] = {
        id = 970,
        res = "icon/equipment/name/nuanxue.png",
        res_en = "icon/equipment/name/nuanxue_en.png",
    },
    [971] = {
        id = 971,
        res = "icon/equipment/name/Shengmingzhange.png",
        res_en = "icon/equipment/name/Shengmingzhange_en.png",
    },
    [972] = {
        id = 972,
        res = "icon/equipment/name/wuxue.png",
        res_en = "icon/equipment/name/wuxue_en.png",
    },
    [973] = {
        id = 973,
        res = "icon/equipment/name/Xchangpian.png",
        res_en = "icon/equipment/name/Xchangpian_en.png",
    },
    [974] = {
        id = 974,
        res = "icon/equipment/name/xiaosan.png",
        res_en = "icon/equipment/name/xiaosan_en.png",
    },
    [975] = {
        id = 975,
        res = "icon/equipment/name/yongheng.png",
        res_en = "icon/equipment/name/yongheng_en.png",
    },
    [976] = {
        id = 976,
        res = "icon/equipment/name/zhongxiaailun.png",
        res_en = "icon/equipment/name/zhongxiaailun_en.png",
    },
    [977] = {
        id = 977,
        res = "icon/equipment/name/zhongxiazhenna.png",
        res_en = "icon/equipment/name/zhongxiazhenna_en.png",
    },
    [978] = {
        id = 978,
        res = "icon/equipment/name/zhongxiazhezhi.png",
        res_en = "icon/equipment/name/zhongxiazhezhi_en.png",
    },
}