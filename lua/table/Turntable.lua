return {
    [10005] = {
        type = 2,
        color = "ui/dawuwong/turntable/DFW_CJ_2.png",
        weight = 300,
        name = 1240004,
        rewardMax = 0,
        sort = 5,
        id = 10005,
        icon = 570033,
        reward = {
            fix = {
                items = {
                    [1] = {
                        id = 570033,
                        num = 1,
                    },
                },
            },
        },
    },
    [10007] = {
        type = 3,
        color = "ui/dawuwong/turntable/DFW_CJ_4.png",
        weight = 30,
        name = 13311514,
        rewardMax = 3,
        sort = 7,
        id = 10007,
        icon = 580162,
        reward = {
            fix = {
                items = {
                    [1] = {
                        id = 580162,
                        num = 1,
                    },
                },
            },
        },
    },
    [10009] = {
        type = 2,
        color = "ui/dawuwong/turntable/DFW_CJ_2.png",
        weight = 300,
        name = 13311571,
        rewardMax = 0,
        sort = 9,
        id = 10009,
        icon = 520222,
        reward = {
            fix = {
                items = {
                    [1] = {
                        id = 520222,
                        num = 1,
                    },
                },
            },
        },
    },
    [10011] = {
        type = 3,
        color = "ui/dawuwong/turntable/DFW_CJ_4.png",
        weight = 50,
        name = 266553,
        rewardMax = 1,
        sort = 11,
        id = 10011,
        icon = 570230,
        reward = {
            fix = {
                items = {
                    [1] = {
                        id = 570230,
                        num = 1,
                    },
                },
            },
        },
    },
    [10002] = {
        type = 1,
        color = "ui/dawuwong/turntable/DFW_CJ_5.png",
        weight = 1300,
        name = 13107,
        rewardMax = 0,
        sort = 2,
        id = 10002,
        icon = 520219,
        reward = {
            roll = {
                count = 1,
                items = {
                    [1] = {
                        min = 2,
                        id = 551101,
                        max = 2,
                        weight = 1000,
                    },
                    [2] = {
                        min = 2,
                        id = 551102,
                        max = 2,
                        weight = 1000,
                    },
                    [3] = {
                        min = 2,
                        id = 551103,
                        max = 2,
                        weight = 1000,
                    },
                    [4] = {
                        min = 2,
                        id = 551104,
                        max = 2,
                        weight = 1000,
                    },
                    [5] = {
                        min = 2,
                        id = 551105,
                        max = 2,
                        weight = 1000,
                    },
                    [6] = {
                        min = 2,
                        id = 551106,
                        max = 2,
                        weight = 1000,
                    },
                    [7] = {
                        min = 2,
                        id = 551107,
                        max = 2,
                        weight = 1000,
                    },
                    [8] = {
                        min = 2,
                        id = 551108,
                        max = 2,
                        weight = 1000,
                    },
                    [9] = {
                        min = 2,
                        id = 551109,
                        max = 2,
                        weight = 1000,
                    },
                    [10] = {
                        min = 2,
                        id = 551110,
                        max = 2,
                        weight = 1000,
                    },
                },
            },
        },
    },
    [10004] = {
        type = 1,
        color = "ui/dawuwong/turntable/DFW_CJ_5.png",
        weight = 1300,
        name = 13311571,
        rewardMax = 0,
        sort = 4,
        id = 10004,
        icon = 520220,
        reward = {
            fix = {
                items = {
                    [1] = {
                        id = 520220,
                        num = 1,
                    },
                },
            },
        },
    },
    [10006] = {
        type = 1,
        color = "ui/dawuwong/turntable/DFW_CJ_5.png",
        weight = 1300,
        name = 13109,
        rewardMax = 0,
        sort = 6,
        id = 10006,
        icon = 520221,
        reward = {
            roll = {
                count = 1,
                items = {
                    [1] = {
                        min = 5,
                        id = 510105,
                        max = 5,
                        weight = 1500,
                    },
                    [2] = {
                        min = 4,
                        id = 510105,
                        max = 4,
                        weight = 2000,
                    },
                    [3] = {
                        min = 3,
                        id = 510105,
                        max = 3,
                        weight = 2000,
                    },
                    [4] = {
                        min = 2,
                        id = 510105,
                        max = 2,
                        weight = 2000,
                    },
                    [5] = {
                        min = 1,
                        id = 510105,
                        max = 1,
                        weight = 2500,
                    },
                },
            },
            fix = {
                items = {
                    [1] = {
                        id = 510105,
                        num = 2,
                    },
                },
            },
        },
    },
    [10008] = {
        type = 1,
        color = "ui/dawuwong/turntable/DFW_CJ_5.png",
        weight = 1300,
        name = 1241001,
        rewardMax = 0,
        sort = 8,
        id = 10008,
        icon = 551111,
        reward = {
            fix = {
                items = {
                    [1] = {
                        id = 551111,
                        num = 1,
                    },
                },
            },
        },
    },
    [10010] = {
        type = 1,
        color = "ui/dawuwong/turntable/DFW_CJ_5.png",
        weight = 1300,
        name = 13111,
        rewardMax = 0,
        sort = 10,
        id = 10010,
        icon = 520223,
        reward = {
            roll = {
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 510152,
                        max = 1,
                        weight = 1500,
                    },
                    [2] = {
                        min = 1,
                        id = 510153,
                        max = 1,
                        weight = 2000,
                    },
                    [3] = {
                        min = 1,
                        id = 510154,
                        max = 1,
                        weight = 3000,
                    },
                    [4] = {
                        min = 1,
                        id = 510155,
                        max = 1,
                        weight = 3500,
                    },
                },
            },
        },
    },
    [10012] = {
        type = 1,
        color = "ui/dawuwong/turntable/DFW_CJ_5.png",
        weight = 1300,
        name = 13112,
        rewardMax = 0,
        sort = 12,
        id = 10012,
        icon = 520224,
        reward = {
            roll = {
                count = 1,
                items = {
                    [1] = {
                        min = 1,
                        id = 570002,
                        max = 1,
                        weight = 3500,
                    },
                    [2] = {
                        min = 2,
                        id = 570002,
                        max = 2,
                        weight = 3000,
                    },
                    [3] = {
                        min = 3,
                        id = 570002,
                        max = 3,
                        weight = 2000,
                    },
                    [4] = {
                        min = 5,
                        id = 570002,
                        max = 5,
                        weight = 1500,
                    },
                },
            },
        },
    },
    [10001] = {
        type = 2,
        color = "ui/dawuwong/turntable/DFW_CJ_2.png",
        weight = 1500,
        name = 13311510,
        rewardMax = 12,
        sort = 1,
        id = 10001,
        icon = 580160,
        reward = {
            fix = {
                items = {
                    [1] = {
                        id = 580160,
                        num = 2,
                    },
                },
            },
        },
    },
    [10003] = {
        type = 3,
        color = "ui/dawuwong/turntable/DFW_CJ_4.png",
        weight = 20,
        name = 13311512,
        rewardMax = 6,
        sort = 3,
        id = 10003,
        icon = 580163,
        reward = {
            fix = {
                items = {
                    [1] = {
                        id = 580163,
                        num = 1,
                    },
                },
            },
        },
    },
}