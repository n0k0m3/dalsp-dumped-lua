return {
    [1200001] = {
        superType = 12,
        order = 1,
        exposedview = "icon/item/goods/1200001.png",
        accessdes = "2470000",
        skilltitle = "",
        effectivetime = -1,
        star = 3,
        icon = "icon/item/goods/1200001.png",
        quality = 1,
        baseAttribute = {
        },
        id = 1200001,
        baseskill = {
        },
        name = "2450000",
        accessway = {
        },
        desTextId = 2460000,
        MedalType = 3,
        size = {
            [1] = 55,
            [2] = 60,
        },
    },
}