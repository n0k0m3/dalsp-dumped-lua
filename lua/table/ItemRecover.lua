return {
    [1] = {
        recover_count = 1,
        resetBuyCountTime = 0,
        price = {
        },
        item_id = 500004,
        id = 1,
        maxRecoverCount = 0,
        cooldown = 360,
        sendNum = 0,
    },
    [2] = {
        recover_count = 3,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 570032,
                        num = 1,
                    },
                    targetNum = 1,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 570032,
                        num = 1,
                    },
                    targetNum = 1,
                },
            },
        },
        item_id = 500011,
        id = 2,
        maxRecoverCount = 3,
        cooldown = -1,
        sendNum = 0,
    },
    [3] = {
        recover_count = 20,
        resetBuyCountTime = 0,
        price = {
        },
        item_id = 500012,
        id = 3,
        maxRecoverCount = 20,
        cooldown = -1,
        sendNum = 0,
    },
    [4] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 25,
                    },
                    targetNum = 60,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 25,
                    },
                    targetNum = 60,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 60,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 60,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 75,
                    },
                    targetNum = 60,
                },
            },
            [6] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 75,
                    },
                    targetNum = 60,
                },
            },
            [7] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 60,
                },
            },
            [8] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 60,
                },
            },
            [9] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 150,
                    },
                    targetNum = 60,
                },
            },
            [10] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 150,
                    },
                    targetNum = 60,
                },
            },
        },
        item_id = 500004,
        id = 4,
        maxRecoverCount = 0,
        cooldown = 0,
        sendNum = 0,
    },
    [5] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 10,
                    },
                    targetNum = 10000,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 15,
                    },
                    targetNum = 12000,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 20,
                    },
                    targetNum = 12000,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 30,
                    },
                    targetNum = 14000,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 40,
                    },
                    targetNum = 14000,
                },
            },
            [6] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 16000,
                },
            },
            [7] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 16000,
                },
            },
            [8] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 16000,
                },
            },
            [9] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 16000,
                },
            },
            [10] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 18000,
                },
            },
            [11] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 18000,
                },
            },
            [12] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 18000,
                },
            },
            [13] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 18000,
                },
            },
            [14] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 18000,
                },
            },
            [15] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 20000,
                },
            },
            [16] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 20000,
                },
            },
            [17] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 20000,
                },
            },
            [18] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 20000,
                },
            },
            [19] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 20000,
                },
            },
            [20] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 20000,
                },
            },
        },
        item_id = 500001,
        id = 5,
        maxRecoverCount = 0,
        cooldown = -1,
        sendNum = 0,
    },
    [6] = {
        recover_count = 3,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 10,
                    },
                    targetNum = 1,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 10,
                    },
                    targetNum = 1,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 20,
                    },
                    targetNum = 1,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 20,
                    },
                    targetNum = 1,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
            [6] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
            [7] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
            [8] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
            [9] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
            [10] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
        },
        item_id = 500017,
        id = 6,
        maxRecoverCount = 3,
        cooldown = -1,
        sendNum = 0,
    },
    [7] = {
        recover_count = 1,
        resetBuyCountTime = 0,
        price = {
        },
        item_id = 500024,
        id = 7,
        maxRecoverCount = 0,
        cooldown = 720,
        sendNum = 0,
    },
    [8] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 25,
                    },
                    targetNum = 30,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 25,
                    },
                    targetNum = 30,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 30,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 30,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 75,
                    },
                    targetNum = 30,
                },
            },
            [6] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 75,
                    },
                    targetNum = 30,
                },
            },
            [7] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 30,
                },
            },
            [8] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 30,
                },
            },
            [9] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 150,
                    },
                    targetNum = 30,
                },
            },
            [10] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 150,
                    },
                    targetNum = 30,
                },
            },
        },
        item_id = 500024,
        id = 8,
        maxRecoverCount = 0,
        cooldown = 0,
        sendNum = 0,
    },
    [9] = {
        recover_count = 1,
        resetBuyCountTime = 0,
        price = {
        },
        item_id = 500025,
        id = 9,
        maxRecoverCount = 0,
        cooldown = 120,
        sendNum = 0,
    },
    [10] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 15,
                    },
                    targetNum = 100,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 15,
                    },
                    targetNum = 100,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 25,
                    },
                    targetNum = 100,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 25,
                    },
                    targetNum = 100,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 100,
                },
            },
            [6] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 100,
                },
            },
            [7] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 75,
                    },
                    targetNum = 100,
                },
            },
            [8] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 75,
                    },
                    targetNum = 100,
                },
            },
            [9] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 100,
                },
            },
            [10] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 100,
                },
            },
        },
        item_id = 500025,
        id = 10,
        maxRecoverCount = 0,
        cooldown = 0,
        sendNum = 0,
    },
    [11] = {
        recover_count = 1,
        resetBuyCountTime = 0,
        price = {
        },
        item_id = 500030,
        id = 11,
        maxRecoverCount = 0,
        cooldown = 28800,
        sendNum = 0,
    },
    [12] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
        },
        item_id = 500033,
        id = 12,
        maxRecoverCount = 0,
        cooldown = -1,
        sendNum = 0,
    },
    [13] = {
        recover_count = 1,
        resetBuyCountTime = 0,
        price = {
        },
        item_id = 500056,
        id = 13,
        maxRecoverCount = 0,
        cooldown = 360,
        sendNum = 0,
    },
    [14] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 30,
                    },
                    targetNum = 50,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 50,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 50,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 50,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 120,
                    },
                    targetNum = 50,
                },
            },
            [6] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 150,
                    },
                    targetNum = 50,
                },
            },
            [7] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 180,
                    },
                    targetNum = 50,
                },
            },
            [8] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 210,
                    },
                    targetNum = 50,
                },
            },
            [9] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 250,
                    },
                    targetNum = 50,
                },
            },
            [10] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 300,
                    },
                    targetNum = 50,
                },
            },
        },
        item_id = 500056,
        id = 14,
        maxRecoverCount = 0,
        cooldown = 0,
        sendNum = 0,
    },
    [15] = {
        recover_count = 3,
        resetBuyCountTime = 2,
        price = {
        },
        item_id = 599503,
        id = 15,
        maxRecoverCount = 10,
        cooldown = -1,
        sendNum = 0,
    },
    [16] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 30,
                    },
                    targetNum = 1,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 1,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 120,
                    },
                    targetNum = 1,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 200,
                    },
                    targetNum = 1,
                },
            },
        },
        item_id = 500060,
        id = 16,
        maxRecoverCount = 0,
        cooldown = -1,
        sendNum = 0,
    },
    [17] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 30,
                    },
                    targetNum = 1,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 1,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 1,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 200,
                    },
                    targetNum = 1,
                },
            },
        },
        item_id = 500063,
        id = 17,
        maxRecoverCount = 0,
        cooldown = -1,
        sendNum = 0,
    },
    [18] = {
        recover_count = 1,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 50,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 50,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 75,
                    },
                    targetNum = 50,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 75,
                    },
                    targetNum = 50,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 50,
                },
            },
            [6] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 50,
                },
            },
        },
        item_id = 500071,
        id = 18,
        maxRecoverCount = 0,
        cooldown = 432,
        sendNum = 0,
    },
    [19] = {
        recover_count = 0,
        resetBuyCountTime = 0,
        price = {
        },
        item_id = 598426,
        id = 19,
        maxRecoverCount = 0,
        cooldown = -1,
        sendNum = 1,
    },
    [20] = {
        recover_count = 0,
        resetBuyCountTime = 1,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 1,
                },
            },
        },
        item_id = 500083,
        id = 20,
        maxRecoverCount = 3,
        cooldown = -1,
        sendNum = 1,
    },
    [21] = {
        recover_count = 1,
        resetBuyCountTime = 0,
        price = {
        },
        item_id = 501001,
        id = 21,
        maxRecoverCount = 0,
        cooldown = 300,
        sendNum = 0,
    },
    [22] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 10,
                    },
                    targetNum = 40,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 20,
                    },
                    targetNum = 40,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 40,
                    },
                    targetNum = 40,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 40,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 120,
                    },
                    targetNum = 40,
                },
            },
        },
        item_id = 501001,
        id = 22,
        maxRecoverCount = 0,
        cooldown = 0,
        sendNum = 0,
    },
    [23] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 1,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 1,
                },
            },
        },
        item_id = 500103,
        id = 23,
        maxRecoverCount = 0,
        cooldown = -1,
        sendNum = 0,
    },
    [24] = {
        recover_count = 1,
        resetBuyCountTime = 0,
        price = {
        },
        item_id = 501005,
        id = 24,
        maxRecoverCount = 0,
        cooldown = 600,
        sendNum = 0,
    },
    [25] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 10,
                    },
                    targetNum = 60,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 20,
                    },
                    targetNum = 60,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 40,
                    },
                    targetNum = 60,
                },
            },
            [4] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 80,
                    },
                    targetNum = 60,
                },
            },
            [5] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 120,
                    },
                    targetNum = 60,
                },
            },
        },
        item_id = 501005,
        id = 25,
        maxRecoverCount = 0,
        cooldown = 0,
        sendNum = 0,
    },
    [26] = {
        recover_count = 1,
        resetBuyCountTime = 0,
        price = {
        },
        item_id = 501008,
        id = 26,
        maxRecoverCount = 0,
        cooldown = 600,
        sendNum = 0,
    },
    [27] = {
        recover_count = 0,
        resetBuyCountTime = 2,
        price = {
            [1] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 30,
                    },
                    targetNum = 60,
                },
            },
            [2] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 50,
                    },
                    targetNum = 60,
                },
            },
            [3] = {
                [1] = {
                    [1] = {
                        id = 500002,
                        num = 100,
                    },
                    targetNum = 60,
                },
            },
        },
        item_id = 501008,
        id = 27,
        maxRecoverCount = 0,
        cooldown = 0,
        sendNum = 0,
    },
}