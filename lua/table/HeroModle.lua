return {
    [1113011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11301_1/paintshow_11301_1",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 1113011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -625,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = 0,
        },
        battleEndPosition = {
            y = 30,
            x = 55,
        },
        teamModelPos = {
            y = -305,
            x = 20,
        },
        TeampaintPosition = {
            y = -35,
            x = 10,
        },
        huntingModelPos = {
            y = -325,
            x = 20,
        },
    },
    [1105011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10501/paintshow_10501",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1105011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.48,
        particleEffect = {
        },
        paintPosition = {
            y = -700,
            x = 120,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 0,
            x = -20,
        },
        battleEndPosition = {
            y = -90,
            x = -10,
        },
        teamModelPos = {
            y = -365,
            x = 0,
        },
        TeampaintPosition = {
            y = -40,
            x = 5,
        },
        huntingModelPos = {
            y = -360,
            x = 0,
        },
    },
    [1106021] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10602/paintshow_10602",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1106021,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.45,
        particleEffect = {
        },
        paintPosition = {
            y = -650,
            x = 100,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 20,
            x = 0,
        },
        battleEndPosition = {
            y = -60,
            x = 0,
        },
        teamModelPos = {
            y = -315,
            x = -20,
        },
        TeampaintPosition = {
            y = -20,
            x = 5,
        },
        huntingModelPos = {
            y = -325,
            x = -20,
        },
    },
    [1105014] = {
        teamModelScale = 0.52,
        paint = "modle/hero/paintshow_10505/paintshow_10505",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1105014,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -700,
            x = 120,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 5,
            x = -10,
        },
        battleEndPosition = {
            y = -90,
            x = -10,
        },
        teamModelPos = {
            y = -365,
            x = 0,
        },
        TeampaintPosition = {
            y = -40,
            x = 10,
        },
        huntingModelPos = {
            y = -350,
            x = 5,
        },
    },
    [1120011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11202/paintshow_11202",
        TeampaintSize = 0.32,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 1120011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -665,
            x = 135,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = -40,
        },
        battleEndPosition = {
            y = -60,
            x = 15,
        },
        teamModelPos = {
            y = -315,
            x = -5,
        },
        TeampaintPosition = {
            y = -55,
            x = 1,
        },
        huntingModelPos = {
            y = -345,
            x = -5,
        },
    },
    [1120012] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11207/paintshow_11207",
        TeampaintSize = 0.32,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 1120012,
        paintSize = 0.9,
        particleEffectOffset = {
            [1] = {
                y = 400,
                x = 0,
            },
        },
        paintDetailSize = 0.51,
        particleEffect = {
            [1] = "particles/waiyoulilizi.plist",
        },
        paintPosition = {
            y = -615,
            x = 135,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -60,
            x = -40,
        },
        battleEndPosition = {
            y = 10,
            x = 15,
        },
        teamModelPos = {
            y = -290,
            x = 18,
        },
        TeampaintPosition = {
            y = -15,
            x = 1,
        },
        huntingModelPos = {
            y = -295,
            x = 18,
        },
    },
    [1104011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10401/paintshow_10401",
        TeampaintSize = 0.31,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1104011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -610,
            x = 160,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -20,
            x = -20,
        },
        battleEndPosition = {
            y = 0,
            x = 0,
        },
        teamModelPos = {
            y = -295,
            x = 0,
        },
        TeampaintPosition = {
            y = -15,
            x = 5,
        },
        huntingModelPos = {
            y = -300,
            x = 0,
        },
    },
    [1104013] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10403/paintshow_10403",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1104013,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.49,
        particleEffect = {
        },
        paintPosition = {
            y = -680,
            x = 150,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 5,
            x = 0,
        },
        battleEndPosition = {
            y = -80,
            x = -10,
        },
        teamModelPos = {
            y = -340,
            x = 0,
        },
        TeampaintPosition = {
            y = -20,
            x = -2,
        },
        huntingModelPos = {
            y = -345,
            x = 0,
        },
    },
    [1104015] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10406/paintshow_10406",
        TeampaintSize = 0.31,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1104015,
        paintSize = 0.9,
        particleEffectOffset = {
            [1] = {
                y = 550,
                x = 0,
            },
        },
        paintDetailSize = 0.51,
        particleEffect = {
            [1] = "particles/xueli.plist",
        },
        paintPosition = {
            y = -640,
            x = 145,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -20,
            x = -35,
        },
        battleEndPosition = {
            y = -25,
            x = -5,
        },
        teamModelPos = {
            y = -300,
            x = -5,
        },
        TeampaintPosition = {
            y = -15,
            x = 5,
        },
        huntingModelPos = {
            y = -320,
            x = -5,
        },
    },
    [1103011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10301/paintshow_10301",
        TeampaintSize = 0.32,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1103011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -570,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -40,
            x = -30,
        },
        battleEndPosition = {
            y = 10,
            x = 20,
        },
        teamModelPos = {
            y = -270,
            x = 0,
        },
        TeampaintPosition = {
            y = -40,
            x = 5,
        },
        huntingModelPos = {
            y = -285,
            x = 0,
        },
    },
    [1113031] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11303/paintshow_11303",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 1113031,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -545,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -105,
            x = -10,
        },
        battleEndPosition = {
            y = 30,
            x = 55,
        },
        teamModelPos = {
            y = -285,
            x = 20,
        },
        TeampaintPosition = {
            y = -35,
            x = 10,
        },
        huntingModelPos = {
            y = -305,
            x = 20,
        },
    },
    [1103015] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10305/paintshow_10305",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1103015,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -600,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -40,
            x = -30,
        },
        battleEndPosition = {
            y = 10,
            x = 20,
        },
        teamModelPos = {
            y = -270,
            x = 0,
        },
        TeampaintPosition = {
            y = -30,
            x = 5,
        },
        huntingModelPos = {
            y = -275,
            x = 0,
        },
    },
    [1110011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11001/paintshow_11001",
        TeampaintSize = 0.31,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1110011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -630,
            x = 150,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -10,
            x = -30,
        },
        battleEndPosition = {
            y = -25,
            x = 15,
        },
        teamModelPos = {
            y = -315,
            x = 0,
        },
        TeampaintPosition = {
            y = -20,
            x = 5,
        },
        huntingModelPos = {
            y = -315,
            x = 0,
        },
    },
    [1103019] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10309/paintshow_10309",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1103019,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -550,
            x = 190,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -40,
            x = -30,
        },
        battleEndPosition = {
            y = 10,
            x = 20,
        },
        teamModelPos = {
            y = -270,
            x = 0,
        },
        TeampaintPosition = {
            y = -30,
            x = 5,
        },
        huntingModelPos = {
            y = -275,
            x = 0,
        },
    },
    [1102012] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10202/paintshow_10202",
        TeampaintSize = 0.32,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1102012,
        paintSize = 0.85,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -670,
            x = 190,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -70,
            x = 0,
        },
        battleEndPosition = {
            y = -45,
            x = 55,
        },
        teamModelPos = {
            y = -310,
            x = 30,
        },
        TeampaintPosition = {
            y = -60,
            x = 10,
        },
        huntingModelPos = {
            y = -325,
            x = 30,
        },
    },
    [1103021] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10302/paintshow_10302",
        TeampaintSize = 0.35,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1103021,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -580,
            x = 110,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -90,
            x = -20,
        },
        battleEndPosition = {
            y = 10,
            x = 20,
        },
        teamModelPos = {
            y = -270,
            x = 0,
        },
        TeampaintPosition = {
            y = -80,
            x = 50,
        },
        huntingModelPos = {
            y = -290,
            x = 0,
        },
    },
    [1102081] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10208/paintshow_10208",
        TeampaintSize = 0.32,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1102081,
        paintSize = 0.85,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -720,
            x = 155,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -35,
            x = -20,
        },
        battleEndPosition = {
            y = -90,
            x = 5,
        },
        teamModelPos = {
            y = -340,
            x = 10,
        },
        TeampaintPosition = {
            y = -60,
            x = 10,
        },
        huntingModelPos = {
            y = -345,
            x = 10,
        },
    },
    [1101011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10101/paintshow_10101",
        TeampaintSize = 0.32,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1101011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -680,
            x = 120,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = 0,
        },
        battleEndPosition = {
            y = -50,
            x = -20,
        },
        teamModelPos = {
            y = -315,
            x = 0,
        },
        TeampaintPosition = {
            y = -50,
            x = -2,
        },
        huntingModelPos = {
            y = -345,
            x = 0,
        },
    },
    [1101012] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10101_2/paintshow_10101_2",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1101012,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.48,
        particleEffect = {
        },
        paintPosition = {
            y = -750,
            x = 160,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 15,
            x = -20,
        },
        battleEndPosition = {
            y = -140,
            x = 20,
        },
        teamModelPos = {
            y = -390,
            x = 0,
        },
        TeampaintPosition = {
            y = -50,
            x = 2,
        },
        huntingModelPos = {
            y = -390,
            x = 0,
        },
    },
    [1101013] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10104/paintshow_10104",
        TeampaintSize = 0.34,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1101013,
        paintSize = 1,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -560,
            x = 130,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -35,
            x = -20,
        },
        battleEndPosition = {
            y = 30,
            x = 25,
        },
        teamModelPos = {
            y = -280,
            x = 0,
        },
        TeampaintPosition = {
            y = -30,
            x = 17,
        },
        huntingModelPos = {
            y = -290,
            x = 0,
        },
    },
    [1101014] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10103/paintshow_10103",
        TeampaintSize = 0.27,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1101014,
        paintSize = 0.85,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -840,
            x = 160,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 0,
            x = 0,
        },
        battleEndPosition = {
            y = -185,
            x = 65,
        },
        teamModelPos = {
            y = -420,
            x = 0,
        },
        TeampaintPosition = {
            y = -30,
            x = 0,
        },
        huntingModelPos = {
            y = -430,
            x = 0,
        },
    },
    [1108071] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10807/paintshow_10807",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1108071,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.49,
        particleEffect = {
        },
        paintPosition = {
            y = -680,
            x = 110,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 0,
            x = -40,
        },
        battleEndPosition = {
            y = -75,
            x = 0,
        },
        teamModelPos = {
            y = -320,
            x = -35,
        },
        TeampaintPosition = {
            y = -30,
            x = 0,
        },
        huntingModelPos = {
            y = -340,
            x = -35,
        },
    },
    [1101016] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10111/paintshow_10111",
        TeampaintSize = 0.35,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1101016,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -510,
            x = 130,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -85,
            x = -20,
        },
        battleEndPosition = {
            y = 120,
            x = 25,
        },
        teamModelPos = {
            y = -230,
            x = 0,
        },
        TeampaintPosition = {
            y = -30,
            x = 0,
        },
        huntingModelPos = {
            y = -245,
            x = 0,
        },
    },
    [1108011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10801/paintshow_10801",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1108011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.49,
        particleEffect = {
        },
        paintPosition = {
            y = -660,
            x = 190,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 0,
            x = -40,
        },
        battleEndPosition = {
            y = -40,
            x = 50,
        },
        teamModelPos = {
            y = -320,
            x = 0,
        },
        TeampaintPosition = {
            y = -30,
            x = 0,
        },
        huntingModelPos = {
            y = -330,
            x = 0,
        },
    },
    [1101019] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10109/paintshow_10109",
        TeampaintSize = 0.27,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1101019,
        paintSize = 0.85,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -700,
            x = 160,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -30,
            x = 0,
        },
        battleEndPosition = {
            y = -55,
            x = 65,
        },
        teamModelPos = {
            y = -330,
            x = 0,
        },
        TeampaintPosition = {
            y = -30,
            x = 0,
        },
        huntingModelPos = {
            y = -330,
            x = 0,
        },
    },
    [1102091] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10209/paintshow_10209",
        TeampaintSize = 0.32,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1102091,
        paintSize = 0.85,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -780,
            x = 155,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -30,
            x = -20,
        },
        battleEndPosition = {
            y = -130,
            x = 5,
        },
        teamModelPos = {
            y = -375,
            x = 10,
        },
        TeampaintPosition = {
            y = -60,
            x = 10,
        },
        huntingModelPos = {
            y = -380,
            x = 10,
        },
    },
    [1101015] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10102/paintshow_10102",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1101015,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -675,
            x = 160,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -10,
            x = -50,
        },
        battleEndPosition = {
            y = -30,
            x = 35,
        },
        teamModelPos = {
            y = -333,
            x = 0,
        },
        TeampaintPosition = {
            y = -30,
            x = -5,
        },
        huntingModelPos = {
            y = -343,
            x = 0,
        },
    },
    [1107071] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10707/paintshow_10707",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1107071,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.48,
        particleEffect = {
        },
        paintPosition = {
            y = -620,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -30,
            x = -10,
        },
        battleEndPosition = {
            y = -30,
            x = 40,
        },
        teamModelPos = {
            y = -295,
            x = 0,
        },
        TeampaintPosition = {
            y = -20,
            x = 10,
        },
        huntingModelPos = {
            y = -310,
            x = 0,
        },
    },
    [1110012] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11005/paintshow_11005",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1110012,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -630,
            x = 150,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -10,
            x = -30,
        },
        battleEndPosition = {
            y = -25,
            x = 15,
        },
        teamModelPos = {
            y = -330,
            x = 0,
        },
        TeampaintPosition = {
            y = -25,
            x = 5,
        },
        huntingModelPos = {
            y = -330,
            x = 0,
        },
    },
    [1115011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11501_1/paintshow_11501_1",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 1115011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -625,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = 0,
        },
        battleEndPosition = {
            y = 30,
            x = 55,
        },
        teamModelPos = {
            y = -305,
            x = 20,
        },
        TeampaintPosition = {
            y = -35,
            x = 10,
        },
        huntingModelPos = {
            y = -325,
            x = 20,
        },
    },
    [1107011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10701/paintshow_10701",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1107011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.48,
        particleEffect = {
        },
        paintPosition = {
            y = -700,
            x = 120,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 10,
            x = -10,
        },
        battleEndPosition = {
            y = -80,
            x = 0,
        },
        teamModelPos = {
            y = -340,
            x = -10,
        },
        TeampaintPosition = {
            y = -30,
            x = 10,
        },
        huntingModelPos = {
            y = -355,
            x = -10,
        },
    },
    [1109011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10901/paintshow_10901",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1109011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -690,
            x = 160,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 0,
            x = 0,
        },
        battleEndPosition = {
            y = -70,
            x = 20,
        },
        teamModelPos = {
            y = -345,
            x = 0,
        },
        TeampaintPosition = {
            y = -45,
            x = 5,
        },
        huntingModelPos = {
            y = -345,
            x = 0,
        },
    },
    [1115012] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11501_2/paintshow_11501_2",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 1115012,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -625,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = 0,
        },
        battleEndPosition = {
            y = 30,
            x = 55,
        },
        teamModelPos = {
            y = -305,
            x = 20,
        },
        TeampaintPosition = {
            y = -35,
            x = 10,
        },
        huntingModelPos = {
            y = -325,
            x = 20,
        },
    },
    [1114012] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11401_2/paintshow_11401_2",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 1114012,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -625,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = 0,
        },
        battleEndPosition = {
            y = 30,
            x = 55,
        },
        teamModelPos = {
            y = -305,
            x = 20,
        },
        TeampaintPosition = {
            y = -35,
            x = 10,
        },
        huntingModelPos = {
            y = -325,
            x = 20,
        },
    },
    [1102101] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10210/paintshow_10210",
        TeampaintSize = 0.32,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1102101,
        paintSize = 0.85,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -780,
            x = 155,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -10,
            x = -20,
        },
        battleEndPosition = {
            y = -130,
            x = 5,
        },
        teamModelPos = {
            y = -375,
            x = 10,
        },
        TeampaintPosition = {
            y = -60,
            x = 10,
        },
        huntingModelPos = {
            y = -380,
            x = 10,
        },
    },
    [1102102] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10212/paintshow_10212",
        TeampaintSize = 0.32,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1102102,
        paintSize = 0.85,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -720,
            x = 155,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -40,
            x = -20,
        },
        battleEndPosition = {
            y = -100,
            x = 25,
        },
        teamModelPos = {
            y = -355,
            x = 10,
        },
        TeampaintPosition = {
            y = -50,
            x = 10,
        },
        huntingModelPos = {
            y = -350,
            x = 10,
        },
    },
    [1113012] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11301_2/paintshow_11301_2",
        TeampaintSize = 0.35,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 1113012,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -625,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = 0,
        },
        battleEndPosition = {
            y = -60,
            x = 15,
        },
        teamModelPos = {
            y = -305,
            x = 20,
        },
        TeampaintPosition = {
            y = -80,
            x = 50,
        },
        huntingModelPos = {
            y = -305,
            x = 20,
        },
    },
    [1114011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_11401_1/paintshow_11401_1",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 1114011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -625,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = 0,
        },
        battleEndPosition = {
            y = 30,
            x = 55,
        },
        teamModelPos = {
            y = -305,
            x = 20,
        },
        TeampaintPosition = {
            y = -35,
            x = 10,
        },
        huntingModelPos = {
            y = -325,
            x = 20,
        },
    },
    [1106011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10601/paintshow_10601",
        TeampaintSize = 0.32,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1106011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -570,
            x = 190,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -30,
            x = -10,
        },
        battleEndPosition = {
            y = 45,
            x = 65,
        },
        teamModelPos = {
            y = -265,
            x = 25,
        },
        TeampaintPosition = {
            y = -20,
            x = -5,
        },
        huntingModelPos = {
            y = -285,
            x = 25,
        },
    },
    [1106012] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10602_3/paintshow_10602_3",
        TeampaintSize = 0.31,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1106012,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.48,
        particleEffect = {
        },
        paintPosition = {
            y = -660,
            x = 140,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 25,
            x = -30,
        },
        battleEndPosition = {
            y = -40,
            x = 0,
        },
        teamModelPos = {
            y = -325,
            x = -10,
        },
        TeampaintPosition = {
            y = -25,
            x = -5,
        },
        huntingModelPos = {
            y = -335,
            x = -10,
        },
    },
    [2101012] = {
        teamModelScale = 0.33,
        paint = "effect/fight_25302/fight_25302",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 2101012,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -625,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = 0,
        },
        battleEndPosition = {
            y = 30,
            x = 55,
        },
        teamModelPos = {
            y = -175,
            x = 20,
        },
        TeampaintPosition = {
            y = -35,
            x = 10,
        },
        huntingModelPos = {
            y = -325,
            x = 20,
        },
    },
    [2101011] = {
        teamModelScale = 0.33,
        paint = "effect/fight_25301/fight_25301",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 2101011,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -625,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = 0,
        },
        battleEndPosition = {
            y = 30,
            x = 55,
        },
        teamModelPos = {
            y = -175,
            x = 20,
        },
        TeampaintPosition = {
            y = -35,
            x = 10,
        },
        huntingModelPos = {
            y = -325,
            x = 20,
        },
    },
    [2101013] = {
        teamModelScale = 0.33,
        paint = "effect/fight_25303/fight_25303",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 30,
            x = 40,
        },
        id = 2101013,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -625,
            x = 170,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -25,
            x = 0,
        },
        battleEndPosition = {
            y = 30,
            x = 55,
        },
        teamModelPos = {
            y = -175,
            x = 20,
        },
        TeampaintPosition = {
            y = -35,
            x = 10,
        },
        huntingModelPos = {
            y = -325,
            x = 20,
        },
    },
    [1102011] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10201/paintshow_10201",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1102011,
        paintSize = 0.85,
        particleEffectOffset = {
        },
        paintDetailSize = 0.5,
        particleEffect = {
        },
        paintPosition = {
            y = -760,
            x = 150,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -10,
            x = 0,
        },
        battleEndPosition = {
            y = -120,
            x = 10,
        },
        teamModelPos = {
            y = -365,
            x = 0,
        },
        TeampaintPosition = {
            y = -60,
            x = 20,
        },
        huntingModelPos = {
            y = -375,
            x = 0,
        },
    },
    [1105013] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10506/paintshow_10506",
        TeampaintSize = 0.3,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1105013,
        paintSize = 0.9,
        particleEffectOffset = {
            [1] = {
                y = 630,
                x = 0,
            },
        },
        paintDetailSize = 0.5,
        particleEffect = {
            [1] = "particles/xueli.plist",
        },
        paintPosition = {
            y = -700,
            x = 160,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = 5,
            x = -30,
        },
        battleEndPosition = {
            y = -85,
            x = 20,
        },
        teamModelPos = {
            y = -340,
            x = 5,
        },
        TeampaintPosition = {
            y = -40,
            x = 10,
        },
        huntingModelPos = {
            y = -350,
            x = 5,
        },
    },
    [1104023] = {
        teamModelScale = 0.5,
        paint = "modle/hero/paintshow_10413/paintshow_10413",
        TeampaintSize = 0.31,
        battleEndYingziPos = {
            y = 0,
            x = 0,
        },
        id = 1104023,
        paintSize = 0.9,
        particleEffectOffset = {
        },
        paintDetailSize = 0.51,
        particleEffect = {
        },
        paintPosition = {
            y = -640,
            x = 145,
        },
        battleEndSize = 1.1,
        paintDetailPosition = {
            y = -20,
            x = -35,
        },
        battleEndPosition = {
            y = -25,
            x = -5,
        },
        teamModelPos = {
            y = -300,
            x = -5,
        },
        TeampaintPosition = {
            y = -15,
            x = 5,
        },
        huntingModelPos = {
            y = -320,
            x = -5,
        },
    },
}