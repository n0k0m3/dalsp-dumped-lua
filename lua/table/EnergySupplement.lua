return {
    [1] = {
        time = {
            [43200] = 50400,
        },
        adIcon = "scene/summon/008.png",
        gifts = {
            [500004] = 30,
        },
        name = "1800002",
        icon = "ui/fuli/icon2.png",
        id = 1,
    },
    [2] = {
        time = {
            [64800] = 72000,
        },
        adIcon = "",
        gifts = {
            [500004] = 30,
        },
        name = "",
        icon = "",
        id = 2,
    },
}