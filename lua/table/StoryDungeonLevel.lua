return {
    [330101101] = {
        chapter = 501,
        group = 4,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110018,
                },
            },
        },
        groupCenter = 1,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000085",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 330101101,
        lineSite1 = {
        },
        lineSite = {
            [1] = "29",
        },
        sortShow = 25,
        levelSite = "26",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [240013] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predungeonResult = {
                    [1] = {
                        id = 240010,
                        star = 4,
                    },
                },
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 13,
        lock = {
        },
        storydungeonName = "301152",
        icon = "icon/fuben/levelIcon/Dungeon/2_jiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240013,
        lineSite1 = {
            [1] = "416",
        },
        lineSite = {
            [1] = "414",
        },
        sortShow = 13,
        levelSite = "413",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [262007] = {
        chapter = 2003,
        group = 3,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 262005,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020020",
        icon = "icon/fuben/levelIcon/Dungeon/3_yewanjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 262007,
        lineSite1 = {
        },
        lineSite = {
            [1] = "308",
        },
        sortShow = 7,
        levelSite = "310",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [275003] = {
        chapter = 0,
        group = 0,
        endShow = {
            [1] = 110401,
            [2] = 1104011,
        },
        unlock = {
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 275003,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "",
        activeIf = {
        },
        storydungeonType = 0,
        isHint = false,
    },
    [110015] = {
        chapter = 501,
        group = 3,
        endShow = {
            [1] = 110701,
            [2] = 1107011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110010,
                },
            },
        },
        groupCenter = 0,
        sort = 21,
        lock = {
        },
        storydungeonName = "12000015",
        icon = "icon/fuben/levelIcon/Dating/110015.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110015,
        lineSite1 = {
        },
        lineSite = {
            [1] = "21",
        },
        sortShow = 21,
        levelSite = "20",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [230005] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                    [1] = 80201,
                },
                predungeon = {
                    [1] = 230002,
                },
            },
        },
        groupCenter = 0,
        sort = 5,
        lock = {
        },
        storydungeonName = "301133",
        icon = "icon/fuben/levelIcon/Dungeon/2_shenshe.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 230005,
        lineSite1 = {
        },
        lineSite = {
            [1] = "305",
        },
        sortShow = 5,
        levelSite = "305",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [120008] = {
        chapter = 502,
        group = 2,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120007,
                },
            },
        },
        groupCenter = 0,
        sort = 43,
        lock = {
        },
        storydungeonName = "12000033",
        icon = "icon/fuben/levelIcon/Dungeon/3_yewanshatan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120008,
        lineSite1 = {
        },
        lineSite = {
            [1] = "11",
        },
        sortShow = 43,
        levelSite = "12",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [120024] = {
        chapter = 502,
        group = 5,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120022,
                },
            },
        },
        groupCenter = 0,
        sort = 63,
        lock = {
        },
        storydungeonName = "12000049",
        icon = "icon/fuben/levelIcon/Dungeon/5_yewanshatan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120024,
        lineSite1 = {
        },
        lineSite = {
            [1] = "32",
        },
        sortShow = 63,
        levelSite = "34",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [262008] = {
        chapter = 2003,
        group = 3,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 262007,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020021",
        icon = "icon/fuben/levelIcon/Dungeon/boss_ditie.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 262008,
        lineSite1 = {
            [1] = "503",
        },
        lineSite = {
            [1] = "309",
        },
        sortShow = 8,
        levelSite = "399",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = false,
    },
    [130001] = {
        chapter = 503,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120026,
                },
            },
        },
        groupCenter = 1,
        sort = 66,
        lock = {
        },
        storydungeonName = "12000052",
        icon = "icon/fuben/levelIcon/Dating/130001.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 66,
        levelSite = "1",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [130017] = {
        chapter = 503,
        group = 3,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130016,
                },
            },
        },
        groupCenter = 0,
        sort = 87,
        lock = {
        },
        storydungeonName = "12000068",
        icon = "icon/fuben/levelIcon/Dungeon/5_ditiezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130017,
        lineSite1 = {
        },
        lineSite = {
            [1] = "24",
        },
        sortShow = 87,
        levelSite = "18",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [321101401] = {
        chapter = 501,
        group = 2,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110006,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000082",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 321101401,
        lineSite1 = {
        },
        lineSite = {
            [1] = "9",
        },
        sortShow = 10,
        levelSite = "11",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [230006] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                    [1] = 80101,
                },
                predungeon = {
                    [1] = 230002,
                },
            },
        },
        groupCenter = 0,
        sort = 6,
        lock = {
        },
        storydungeonName = "301134",
        icon = "icon/fuben/levelIcon/Dungeon/5_ditiezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 230006,
        lineSite1 = {
        },
        lineSite = {
            [1] = "304",
        },
        sortShow = 6,
        levelSite = "306",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [80101] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 230002,
                },
            },
        },
        groupCenter = 0,
        sort = 4,
        lock = {
        },
        storydungeonName = "301132",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 80101,
        lineSite1 = {
        },
        lineSite = {
            [1] = "303",
        },
        sortShow = 4,
        levelSite = "304",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [260001] = {
        chapter = 2003,
        group = 1,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 250005,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020001",
        icon = "icon/fuben/levelIcon/Dungeon/2_xuedichezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 260001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 1,
        levelSite = "101",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [110016] = {
        chapter = 501,
        group = 3,
        endShow = {
            [1] = 110701,
            [2] = 1107011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110015,
                },
            },
        },
        groupCenter = 0,
        sort = 22,
        lock = {
        },
        storydungeonName = "12000016",
        icon = "icon/fuben/levelIcon/Dating/110016.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110016,
        lineSite1 = {
        },
        lineSite = {
            [1] = "23",
        },
        sortShow = 22,
        levelSite = "22",
        activeIf = {
        },
        storydungeonType = 4,
        isHint = false,
    },
    [230007] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 230005,
                    [2] = 230099,
                },
            },
        },
        groupCenter = 0,
        sort = 7,
        lock = {
        },
        storydungeonName = "301135",
        icon = "icon/fuben/levelIcon/Dungeon/2_jiaoshi.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 230007,
        lineSite1 = {
        },
        lineSite = {
            [1] = "310",
        },
        sortShow = 7,
        levelSite = "307",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [120009] = {
        chapter = 502,
        group = 2,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120005,
                },
            },
        },
        groupCenter = 0,
        sort = 44,
        lock = {
        },
        storydungeonName = "12000034",
        icon = "icon/fuben/levelIcon/Dungeon/3_yewanjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120009,
        lineSite1 = {
        },
        lineSite = {
            [1] = "8",
        },
        sortShow = 44,
        levelSite = "10",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [120025] = {
        chapter = 502,
        group = 4,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120023,
                },
            },
        },
        groupCenter = 0,
        sort = 64,
        lock = {
        },
        storydungeonName = "12000050",
        icon = "icon/fuben/levelIcon/Dungeon/5_putongjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120025,
        lineSite1 = {
        },
        lineSite = {
            [1] = "28",
        },
        sortShow = 64,
        levelSite = "25",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [351101401] = {
        chapter = 502,
        group = 5,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120023,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000092",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 351101401,
        lineSite1 = {
        },
        lineSite = {
            [1] = "29",
        },
        sortShow = 62,
        levelSite = "27",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [130018] = {
        chapter = 503,
        group = 4,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130014,
                },
            },
        },
        groupCenter = 0,
        sort = 88,
        lock = {
        },
        storydungeonName = "12000069",
        icon = "icon/fuben/levelIcon/Dungeon/3_louding2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130018,
        lineSite1 = {
        },
        lineSite = {
            [1] = "22",
        },
        sortShow = 88,
        levelSite = "22",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [230008] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 230006,
                },
            },
            [2] = {
                predating = {
                },
                predungeon = {
                    [1] = 230005,
                },
            },
        },
        groupCenter = 0,
        sort = 10,
        lock = {
        },
        storydungeonName = "301138",
        icon = "icon/fuben/levelIcon/Dungeon/3_yewanjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 230008,
        lineSite1 = {
        },
        lineSite = {
            [1] = "307",
            [2] = "311",
        },
        sortShow = 10,
        levelSite = "308",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [260003] = {
        chapter = 2003,
        group = 1,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 260001,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020003",
        icon = "icon/fuben/levelIcon/Dungeon/2_huiyishi.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 260003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "102",
        },
        sortShow = 3,
        levelSite = "105",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [110001] = {
        chapter = 501,
        group = 1,
        endShow = {
            [1] = 110501,
            [2] = 1105011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 1,
        sort = 1,
        lock = {
        },
        storydungeonName = "12000001",
        icon = "icon/fuben/levelIcon/Dating/110001.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 1,
        levelSite = "1",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [110017] = {
        chapter = 501,
        group = 3,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110014,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110015,
                },
            },
        },
        groupCenter = 0,
        sort = 23,
        lock = {
        },
        storydungeonName = "12000017",
        icon = "icon/fuben/levelIcon/Dungeon/3_louding2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110017,
        lineSite1 = {
        },
        lineSite = {
            [1] = "26",
            [2] = "24",
        },
        sortShow = 23,
        levelSite = "24",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [230009] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 230006,
                    [2] = 230099,
                },
            },
        },
        groupCenter = 0,
        sort = 9,
        lock = {
        },
        storydungeonName = "301137",
        icon = "icon/fuben/levelIcon/Dungeon/5_phxiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 230009,
        lineSite1 = {
        },
        lineSite = {
            [1] = "306",
        },
        sortShow = 9,
        levelSite = "309",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [120010] = {
        chapter = 502,
        group = 2,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120006,
                },
            },
        },
        groupCenter = 0,
        sort = 45,
        lock = {
        },
        storydungeonName = "12000035",
        icon = "icon/fuben/levelIcon/Dungeon/5_zcxiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120010,
        lineSite1 = {
        },
        lineSite = {
            [1] = "12",
        },
        sortShow = 45,
        levelSite = "9",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [120026] = {
        chapter = 502,
        group = 5,
        endShow = {
            [1] = 110501,
            [2] = 1105011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 351101401,
                },
                predungeon = {
                    [1] = 120023,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120022,
                },
            },
        },
        groupCenter = 1,
        sort = 65,
        lock = {
        },
        storydungeonName = "12000051",
        icon = "icon/fuben/levelIcon/Dating/120026.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120026,
        lineSite1 = {
        },
        lineSite = {
            [1] = "34",
            [2] = "33",
        },
        sortShow = 65,
        levelSite = "37",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [343101301] = {
        chapter = 502,
        group = 3,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120013,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000090",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 343101301,
        lineSite1 = {
        },
        lineSite = {
            [1] = "15",
        },
        sortShow = 50,
        levelSite = "16",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [130019] = {
        chapter = 503,
        group = 4,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130016,
                },
            },
        },
        groupCenter = 0,
        sort = 89,
        lock = {
        },
        storydungeonName = "12000070",
        icon = "icon/fuben/levelIcon/Dungeon/3_feichuan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130019,
        lineSite1 = {
        },
        lineSite = {
            [1] = "26",
        },
        sortShow = 89,
        levelSite = "25",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [308419] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308418,
                },
            },
        },
        groupCenter = 0,
        sort = 119,
        lock = {
        },
        storydungeonName = "12010079",
        icon = "icon/fuben/levelIcon/Dungeon/3_wanyouli.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308419,
        lineSite1 = {
        },
        lineSite = {
            [1] = "19",
        },
        sortShow = 0,
        levelSite = "19",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [308101] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130020,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130021,
                },
            },
        },
        groupCenter = 0,
        sort = 101,
        lock = {
        },
        storydungeonName = "12010061",
        icon = "icon/fuben/levelIcon/Dungeon/3_louding2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308101,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "1",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [110002] = {
        chapter = 501,
        group = 1,
        endShow = {
            [1] = 110501,
            [2] = 1105011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110001,
                },
            },
        },
        groupCenter = 0,
        sort = 2,
        lock = {
        },
        storydungeonName = "12000002",
        icon = "icon/cg/wanyouli_1_1.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "1",
        },
        sortShow = 2,
        levelSite = "2",
        activeIf = {
        },
        storydungeonType = 7,
        isHint = false,
    },
    [110018] = {
        chapter = 501,
        group = 4,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110017,
                },
            },
        },
        groupCenter = 0,
        sort = 24,
        lock = {
        },
        storydungeonName = "12000018",
        icon = "icon/cg/wanyouli_1_2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110018,
        lineSite1 = {
        },
        lineSite = {
            [1] = "25",
        },
        sortShow = 24,
        levelSite = "23",
        activeIf = {
        },
        storydungeonType = 7,
        isHint = false,
    },
    [308102] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308101,
                },
            },
        },
        groupCenter = 0,
        sort = 102,
        lock = {
        },
        storydungeonName = "12010062",
        icon = "icon/fuben/levelIcon/Dungeon/3_putongjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308102,
        lineSite1 = {
        },
        lineSite = {
            [1] = "1",
        },
        sortShow = 0,
        levelSite = "2",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [120011] = {
        chapter = 502,
        group = 2,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120007,
                },
            },
        },
        groupCenter = 0,
        sort = 46,
        lock = {
        },
        storydungeonName = "12000036",
        icon = "icon/fuben/levelIcon/Dating/120011.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120011,
        lineSite1 = {
        },
        lineSite = {
            [1] = "10",
        },
        sortShow = 46,
        levelSite = "13",
        activeIf = {
        },
        storydungeonType = 4,
        isHint = false,
    },
    [130004] = {
        chapter = 503,
        group = 2,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130002,
                },
            },
        },
        groupCenter = 0,
        sort = 73,
        lock = {
        },
        storydungeonName = "12000055",
        icon = "icon/fuben/levelIcon/Dungeon/3_juchangwai2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "5",
        },
        sortShow = 73,
        levelSite = "8",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [130020] = {
        chapter = 503,
        group = 4,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130018,
                },
            },
        },
        groupCenter = 1,
        sort = 90,
        lock = {
        },
        storydungeonName = "12000071",
        icon = "icon/cg/wanyouli_3_2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130020,
        lineSite1 = {
        },
        lineSite = {
            [1] = "25",
        },
        sortShow = 90,
        levelSite = "27",
        activeIf = {
        },
        storydungeonType = 7,
        isHint = false,
    },
    [353101401] = {
        chapter = 503,
        group = 1,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130001,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000094",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 353101401,
        lineSite1 = {
        },
        lineSite = {
            [1] = "2",
        },
        sortShow = 68,
        levelSite = "3",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [308105] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308104,
                },
            },
        },
        groupCenter = 0,
        sort = 105,
        lock = {
        },
        storydungeonName = "12010065",
        icon = "icon/fuben/levelIcon/Dungeon/3_louding2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308105,
        lineSite1 = {
        },
        lineSite = {
            [1] = "4",
        },
        sortShow = 0,
        levelSite = "6",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [110003] = {
        chapter = 501,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 319101301,
                },
                predungeon = {
                    [1] = 110002,
                },
            },
        },
        groupCenter = 0,
        sort = 5,
        lock = {
        },
        storydungeonName = "12000003",
        icon = "icon/fuben/levelIcon/Dungeon/3_ditiezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "4",
        },
        sortShow = 5,
        levelSite = "5",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [110019] = {
        chapter = 501,
        group = 4,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 325101401,
                },
                predungeon = {
                    [1] = 110011,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                    [1] = 330101101,
                },
                predungeon = {
                    [1] = 110018,
                },
            },
        },
        groupCenter = 0,
        sort = 27,
        lock = {
        },
        storydungeonName = "12000019",
        icon = "icon/fuben/levelIcon/Dating/110019.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110019,
        lineSite1 = {
        },
        lineSite = {
            [1] = "27",
            [2] = "31",
        },
        sortShow = 27,
        levelSite = "28",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [120012] = {
        chapter = 502,
        group = 3,
        endShow = {
            [1] = 110801,
            [2] = 1108011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120008,
                },
            },
        },
        groupCenter = 0,
        sort = 47,
        lock = {
        },
        storydungeonName = "12000037",
        icon = "icon/fuben/levelIcon/Dating/120012.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120012,
        lineSite1 = {
        },
        lineSite = {
            [1] = "14",
        },
        sortShow = 47,
        levelSite = "15",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [130005] = {
        chapter = 503,
        group = 2,
        endShow = {
            [1] = 110201,
            [2] = 1102011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130004,
                },
            },
        },
        groupCenter = 1,
        sort = 74,
        lock = {
        },
        storydungeonName = "12000056",
        icon = "icon/fuben/levelIcon/Dating/130005.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130005,
        lineSite1 = {
        },
        lineSite = {
            [1] = "8",
        },
        sortShow = 74,
        levelSite = "9",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [130021] = {
        chapter = 503,
        group = 4,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130019,
                },
            },
        },
        groupCenter = 0,
        sort = 91,
        lock = {
        },
        storydungeonName = "12000072",
        icon = "icon/fuben/levelIcon/Dating/130021.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130021,
        lineSite1 = {
        },
        lineSite = {
            [1] = "27",
        },
        sortShow = 91,
        levelSite = "35",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [250000] = {
        chapter = 2001,
        group = 1,
        endShow = {
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 1,
        sort = 99,
        lock = {
            [1] = {
                notPass = {
                    [1] = 210099,
                },
            },
            [2] = {
                notPass = {
                    [1] = 220099,
                },
            },
            [3] = {
                notPass = {
                    [1] = 230099,
                },
            },
            [4] = {
                notPass = {
                    [1] = 240099,
                },
            },
        },
        storydungeonName = "301155",
        icon = "icon/fuben/levelIcon/Dungeon/boss_zhong.png",
        speTypeParam = {
            chapterId = 2002,
        },
        previewAni = {
        },
        id = 250000,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 99,
        levelSite = "501",
        activeIf = {
        },
        storydungeonType = 11,
        isHint = false,
    },
    [325101401] = {
        chapter = 501,
        group = 3,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110011,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000084",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 325101401,
        lineSite1 = {
        },
        lineSite = {
            [1] = "18",
        },
        sortShow = 17,
        levelSite = "18",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [110004] = {
        chapter = 501,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 319101401,
                },
                predungeon = {
                    [1] = 110002,
                },
            },
        },
        groupCenter = 0,
        sort = 6,
        lock = {
        },
        storydungeonName = "12000004",
        icon = "icon/fuben/levelIcon/Dungeon/3_feixing.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "5",
        },
        sortShow = 6,
        levelSite = "6",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [110020] = {
        chapter = 501,
        group = 5,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110019,
                },
            },
        },
        groupCenter = 0,
        sort = 28,
        lock = {
        },
        storydungeonName = "12000020",
        icon = "icon/fuben/levelIcon/Dating/110020.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110020,
        lineSite1 = {
        },
        lineSite = {
            [1] = "32",
        },
        sortShow = 28,
        levelSite = "33",
        activeIf = {
        },
        storydungeonType = 4,
        isHint = false,
    },
    [80201] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 230002,
                },
            },
        },
        groupCenter = 0,
        sort = 3,
        lock = {
        },
        storydungeonName = "301131",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 80201,
        lineSite1 = {
        },
        lineSite = {
            [1] = "302",
        },
        sortShow = 3,
        levelSite = "303",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [120013] = {
        chapter = 502,
        group = 3,
        endShow = {
            [1] = 110801,
            [2] = 1108011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120009,
                },
            },
        },
        groupCenter = 0,
        sort = 48,
        lock = {
        },
        storydungeonName = "12000038",
        icon = "icon/fuben/levelIcon/Dating/120013.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120013,
        lineSite1 = {
        },
        lineSite = {
            [1] = "13",
        },
        sortShow = 48,
        levelSite = "14",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [250001] = {
        chapter = 2002,
        group = 1,
        endShow = {
            [1] = 110302,
            [2] = 1103021,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 240099,
                    [2] = 230099,
                    [3] = 220099,
                    [4] = 210099,
                },
            },
        },
        groupCenter = 0,
        sort = 1,
        lock = {
        },
        storydungeonName = "301156",
        icon = "icon/fuben/levelIcon/Dungeon/boss_kabala.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 250001,
        lineSite1 = {
        },
        lineSite = {
            [1] = "101",
        },
        sortShow = 1,
        levelSite = "101",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = false,
    },
    [130006] = {
        chapter = 503,
        group = 2,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130005,
                },
            },
        },
        groupCenter = 0,
        sort = 75,
        lock = {
        },
        storydungeonName = "12000057",
        icon = "icon/fuben/levelIcon/Dungeon/3_jiaowai2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130006,
        lineSite1 = {
        },
        lineSite = {
            [1] = "9",
        },
        sortShow = 75,
        levelSite = "10",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [220007] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 220004,
                },
            },
        },
        groupCenter = 0,
        sort = 7,
        lock = {
        },
        storydungeonName = "301126",
        icon = "icon/fuben/levelIcon/Dungeon/5_ditiezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 220007,
        lineSite1 = {
        },
        lineSite = {
            [1] = "206",
        },
        sortShow = 7,
        levelSite = "207",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [140001] = {
        chapter = 504,
        group = 1,
        endShow = {
            [1] = 112001,
            [2] = 1120011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130020,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130021,
                },
            },
        },
        groupCenter = 1,
        sort = 92,
        lock = {
        },
        storydungeonName = "12000073",
        icon = "icon/fuben/levelIcon/Dating/140001.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 140001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 92,
        levelSite = "1",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [279112] = {
        chapter = 0,
        group = 0,
        endShow = {
            [1] = 110401,
            [2] = 1104011,
        },
        unlock = {
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 279112,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "",
        activeIf = {
        },
        storydungeonType = 0,
        isHint = false,
    },
    [355101401] = {
        chapter = 503,
        group = 2,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130003,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000095",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 355101401,
        lineSite1 = {
        },
        lineSite = {
            [1] = "6",
        },
        sortShow = 71,
        levelSite = "7",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [273007] = {
        chapter = 0,
        group = 0,
        endShow = {
            [1] = 111501,
            [2] = 1115011,
        },
        unlock = {
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 273007,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "",
        activeIf = {
        },
        storydungeonType = 0,
        isHint = false,
    },
    [275002] = {
        chapter = 0,
        group = 0,
        endShow = {
            [1] = 111501,
            [2] = 1115011,
        },
        unlock = {
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 275002,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "",
        activeIf = {
        },
        storydungeonType = 0,
        isHint = false,
    },
    [110005] = {
        chapter = 501,
        group = 1,
        endShow = {
            [1] = 110501,
            [2] = 1105011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110003,
                },
            },
        },
        groupCenter = 0,
        sort = 7,
        lock = {
        },
        storydungeonName = "12000005",
        icon = "icon/fuben/levelIcon/Dating/110005.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110005,
        lineSite1 = {
        },
        lineSite = {
            [1] = "6",
        },
        sortShow = 7,
        levelSite = "7",
        activeIf = {
        },
        storydungeonType = 4,
        isHint = false,
    },
    [110021] = {
        chapter = 501,
        group = 5,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 330101201,
                },
                predungeon = {
                    [1] = 110018,
                },
            },
        },
        groupCenter = 0,
        sort = 29,
        lock = {
        },
        storydungeonName = "12000021",
        icon = "icon/fuben/levelIcon/Dating/110021.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110021,
        lineSite1 = {
        },
        lineSite = {
            [1] = "30",
        },
        sortShow = 29,
        levelSite = "27",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [140002] = {
        chapter = 504,
        group = 1,
        endShow = {
            [1] = 112001,
            [2] = 1120011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 140001,
                },
            },
        },
        groupCenter = 0,
        sort = 93,
        lock = {
        },
        storydungeonName = "12000074",
        icon = "icon/fuben/levelIcon/Dating/140002.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 140002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "1",
        },
        sortShow = 93,
        levelSite = "2",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [275001] = {
        chapter = 0,
        group = 0,
        endShow = {
            [1] = 111401,
            [2] = 1114011,
        },
        unlock = {
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 275001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "",
        activeIf = {
        },
        storydungeonType = 0,
        isHint = false,
    },
    [120014] = {
        chapter = 502,
        group = 3,
        endShow = {
            [1] = 110801,
            [2] = 1108011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 343101401,
                },
                predungeon = {
                    [1] = 120013,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120012,
                },
            },
        },
        groupCenter = 0,
        sort = 51,
        lock = {
        },
        storydungeonName = "12000039",
        icon = "icon/fuben/levelIcon/Dating/120014.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120014,
        lineSite1 = {
        },
        lineSite = {
            [1] = "18",
            [2] = "17",
        },
        sortShow = 51,
        levelSite = "19",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [250003] = {
        chapter = 2002,
        group = 1,
        endShow = {
            [1] = 110302,
            [2] = 1103021,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 240099,
                    [2] = 230099,
                    [3] = 220099,
                    [4] = 210099,
                },
            },
        },
        groupCenter = 0,
        sort = 1,
        lock = {
        },
        storydungeonName = "301158",
        icon = "icon/fuben/levelIcon/Dungeon/boss_ditie.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 250003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "301",
        },
        sortShow = 3,
        levelSite = "301",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = false,
    },
    [274001] = {
        chapter = 0,
        group = 0,
        endShow = {
            [1] = 111501,
            [2] = 1115011,
        },
        unlock = {
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 274001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "",
        activeIf = {
        },
        storydungeonType = 0,
        isHint = false,
    },
    [130007] = {
        chapter = 503,
        group = 2,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 355101401,
                },
                predungeon = {
                    [1] = 130003,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130006,
                },
            },
        },
        groupCenter = 0,
        sort = 76,
        lock = {
        },
        storydungeonName = "12000058",
        icon = "icon/fuben/levelIcon/Dating/130007.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130007,
        lineSite1 = {
        },
        lineSite = {
            [1] = "10",
            [2] = "12",
        },
        sortShow = 76,
        levelSite = "14",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [279109] = {
        chapter = 0,
        group = 0,
        endShow = {
            [1] = 110401,
            [2] = 1104011,
        },
        unlock = {
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 279109,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "",
        activeIf = {
        },
        storydungeonType = 0,
        isHint = false,
    },
    [272004] = {
        chapter = 0,
        group = 0,
        endShow = {
            [1] = 111501,
            [2] = 1115011,
        },
        unlock = {
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 272004,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "",
        activeIf = {
        },
        storydungeonType = 0,
        isHint = false,
    },
    [140003] = {
        chapter = 504,
        group = 1,
        endShow = {
            [1] = 112001,
            [2] = 1120011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 140002,
                },
            },
        },
        groupCenter = 0,
        sort = 94,
        lock = {
        },
        storydungeonName = "12000075",
        icon = "icon/fuben/levelIcon/Dating/140003.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 140003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "2",
        },
        sortShow = 94,
        levelSite = "3",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [264001] = {
        chapter = 2003,
        group = 5,
        endShow = {
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 250005,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
            [1] = {
                notPass = {
                    [1] = 260007,
                },
            },
            [2] = {
                notPass = {
                    [1] = 261006,
                },
            },
            [3] = {
                notPass = {
                    [1] = 262008,
                },
            },
            [4] = {
                notPass = {
                    [1] = 263008,
                },
            },
            [5] = {
                notPass = {
                    [1] = 260007,
                },
            },
        },
        storydungeonName = "12020030",
        icon = "icon/fuben/levelIcon/Dungeon/boss_zhong.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 264001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 1,
        levelSite = "501",
        activeIf = {
        },
        storydungeonType = 13,
        isHint = false,
    },
    [271011] = {
        chapter = 0,
        group = 0,
        endShow = {
            [1] = 111401,
            [2] = 1114011,
        },
        unlock = {
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 271011,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "",
        activeIf = {
        },
        storydungeonType = 0,
        isHint = false,
    },
    [250004] = {
        chapter = 2002,
        group = 1,
        endShow = {
            [1] = 110302,
            [2] = 1103021,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 240099,
                    [2] = 230099,
                    [3] = 220099,
                    [4] = 210099,
                },
            },
        },
        groupCenter = 0,
        sort = 1,
        lock = {
        },
        storydungeonName = "301159",
        icon = "icon/fuben/levelIcon/Dungeon/boss_haibian.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 250004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "401",
        },
        sortShow = 4,
        levelSite = "401",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = false,
    },
    [210001] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 1,
        lock = {
            [1] = {
                predungeon = {
                    [1] = 220001,
                },
                notPass = {
                    [1] = 220099,
                },
            },
            [2] = {
                predungeon = {
                    [1] = 230001,
                },
                notPass = {
                    [1] = 230099,
                },
            },
            [3] = {
                predungeon = {
                    [1] = 240001,
                },
                notPass = {
                    [1] = 240099,
                },
            },
        },
        storydungeonName = "301108",
        icon = "icon/fuben/levelIcon/Dungeon/2_kabala3.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 1,
        levelSite = "101",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = true,
    },
    [271006] = {
        chapter = 0,
        group = 0,
        endShow = {
            [1] = 111401,
            [2] = 1114011,
        },
        unlock = {
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 271006,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 0,
        levelSite = "",
        activeIf = {
        },
        storydungeonType = 0,
        isHint = false,
    },
    [110006] = {
        chapter = 501,
        group = 2,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110003,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110004,
                },
            },
        },
        groupCenter = 1,
        sort = 8,
        lock = {
        },
        storydungeonName = "12000006",
        icon = "icon/fuben/levelIcon/Dating/110006.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110006,
        lineSite1 = {
        },
        lineSite = {
            [1] = "7",
            [2] = "8",
        },
        sortShow = 8,
        levelSite = "8",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [110022] = {
        chapter = 501,
        group = 5,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110021,
                },
            },
        },
        groupCenter = 0,
        sort = 30,
        lock = {
        },
        storydungeonName = "12000022",
        icon = "icon/fuben/levelIcon/Dungeon/3_phxiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110022,
        lineSite1 = {
        },
        lineSite = {
            [1] = "36",
        },
        sortShow = 30,
        levelSite = "35",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [140004] = {
        chapter = 504,
        group = 1,
        endShow = {
            [1] = 112001,
            [2] = 1120011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 140003,
                },
            },
        },
        groupCenter = 0,
        sort = 95,
        lock = {
        },
        storydungeonName = "12000076",
        icon = "icon/fuben/levelIcon/Dating/140004.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 140004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "3",
        },
        sortShow = 95,
        levelSite = "4",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [263008] = {
        chapter = 2003,
        group = 4,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 263007,
                },
            },
            [2] = {
                predungeon = {
                    [1] = 263006,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020029",
        icon = "icon/fuben/levelIcon/Dungeon/boss_haibian.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 263008,
        lineSite1 = {
            [1] = "504",
        },
        lineSite = {
        },
        sortShow = 8,
        levelSite = "499",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = false,
    },
    [120015] = {
        chapter = 502,
        group = 3,
        endShow = {
            [1] = 110801,
            [2] = 1108011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120014,
                },
            },
        },
        groupCenter = 0,
        sort = 52,
        lock = {
        },
        storydungeonName = "12000040",
        icon = "icon/fuben/levelIcon/Dating/120015.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120015,
        lineSite1 = {
        },
        lineSite = {
            [1] = "20",
        },
        sortShow = 52,
        levelSite = "18",
        activeIf = {
        },
        storydungeonType = 4,
        isHint = false,
    },
    [250005] = {
        chapter = 2002,
        group = 1,
        endShow = {
            [1] = 110302,
            [2] = 1103021,
        },
        unlock = {
        },
        groupCenter = 1,
        sort = 1,
        lock = {
            [1] = {
                notPass = {
                    [1] = 250001,
                },
            },
            [2] = {
                notPass = {
                    [1] = 250002,
                },
            },
            [3] = {
                notPass = {
                    [1] = 250003,
                },
            },
            [4] = {
                notPass = {
                    [1] = 250004,
                },
            },
        },
        storydungeonName = "301160",
        icon = "icon/fuben/levelIcon/Dungeon/boss_no9.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 250005,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 6,
        levelSite = "501",
        activeIf = {
        },
        storydungeonType = 12,
        isHint = false,
    },
    [210002] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 210001,
                },
            },
        },
        groupCenter = 0,
        sort = 2,
        lock = {
        },
        storydungeonName = "301109",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "101",
        },
        sortShow = 2,
        levelSite = "102",
        activeIf = {
            predungeonResult = {
                [1] = {
                    id = 210001,
                    star = 4,
                },
            },
        },
        storydungeonType = 10,
        isHint = false,
    },
    [130008] = {
        chapter = 503,
        group = 2,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 355101501,
                },
                predungeon = {
                    [1] = 130003,
                },
            },
        },
        groupCenter = 0,
        sort = 77,
        lock = {
        },
        storydungeonName = "12000059",
        icon = "icon/fuben/levelIcon/Dating/130008.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130008,
        lineSite1 = {
        },
        lineSite = {
            [1] = "11",
        },
        sortShow = 77,
        levelSite = "12",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [263007] = {
        chapter = 2003,
        group = 4,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 263005,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020028",
        icon = "icon/fuben/levelIcon/Dungeon/3_wanyouli.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 263007,
        lineSite1 = {
            [1] = "417",
        },
        lineSite = {
            [1] = "413",
        },
        sortShow = 7,
        levelSite = "414",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [210099] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 210008,
                    [2] = 210010,
                },
            },
        },
        groupCenter = 0,
        sort = 12,
        lock = {
        },
        storydungeonName = "301119",
        icon = "icon/fuben/levelIcon/Dungeon/boss_icestreet.png",
        speTypeParam = {
        },
        previewAni = {
            [1] = "ActionPre_99",
            [2] = "ActionAfter_99",
        },
        id = 210099,
        lineSite1 = {
            [1] = "501",
        },
        lineSite = {
            [1] = "199",
        },
        sortShow = 12,
        levelSite = "199",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = true,
    },
    [140005] = {
        chapter = 504,
        group = 1,
        endShow = {
            [1] = 112001,
            [2] = 1120011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 140004,
                },
            },
        },
        groupCenter = 0,
        sort = 96,
        lock = {
        },
        storydungeonName = "12000077",
        icon = "icon/fuben/levelIcon/Dating/140005.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 140005,
        lineSite1 = {
        },
        lineSite = {
            [1] = "4",
        },
        sortShow = 96,
        levelSite = "6",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [263005] = {
        chapter = 2003,
        group = 4,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 263003,
                },
            },
            [2] = {
                predungeon = {
                    [1] = 263004,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020026",
        icon = "icon/fuben/levelIcon/Dungeon/2_louding.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 263005,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 5,
        levelSite = "410",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [263004] = {
        chapter = 2003,
        group = 4,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 263001,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020025",
        icon = "icon/fuben/levelIcon/Dungeon/2_jiaoshi.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 263004,
        lineSite1 = {
            [1] = "410",
        },
        lineSite = {
            [1] = "403",
        },
        sortShow = 4,
        levelSite = "407",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [263003] = {
        chapter = 2003,
        group = 4,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 263001,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020024",
        icon = "icon/fuben/levelIcon/Dungeon/5_zcxiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 263003,
        lineSite1 = {
            [1] = "409",
        },
        lineSite = {
            [1] = "402",
        },
        sortShow = 3,
        levelSite = "406",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [308312] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308311,
                },
            },
        },
        groupCenter = 0,
        sort = 112,
        lock = {
        },
        storydungeonName = "12010072",
        icon = "icon/fuben/levelIcon/Dungeon/3_caochang.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308312,
        lineSite1 = {
        },
        lineSite = {
            [1] = "11",
        },
        sortShow = 0,
        levelSite = "13",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [230099] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 230008,
                },
            },
        },
        groupCenter = 0,
        sort = 11,
        lock = {
        },
        storydungeonName = "301139",
        icon = "icon/fuben/levelIcon/Dungeon/boss_ditie.png",
        speTypeParam = {
        },
        previewAni = {
            [1] = "ActionPre_99",
            [2] = "ActionAfter_99",
        },
        id = 230099,
        lineSite1 = {
            [1] = "503",
        },
        lineSite = {
            [1] = "309",
        },
        sortShow = 11,
        levelSite = "399",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = true,
    },
    [110007] = {
        chapter = 501,
        group = 2,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 321101301,
                },
                predungeon = {
                    [1] = 110006,
                },
            },
        },
        groupCenter = 0,
        sort = 10,
        lock = {
        },
        storydungeonName = "12000007",
        icon = "icon/fuben/levelIcon/Dating/110007.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110007,
        lineSite1 = {
        },
        lineSite = {
            [1] = "11",
        },
        sortShow = 11,
        levelSite = "9",
        activeIf = {
        },
        storydungeonType = 4,
        isHint = false,
    },
    [319101301] = {
        chapter = 501,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110002,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000079",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 319101301,
        lineSite1 = {
        },
        lineSite = {
            [1] = "2",
        },
        sortShow = 3,
        levelSite = "3",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [140006] = {
        chapter = 504,
        group = 2,
        endShow = {
            [1] = 112001,
            [2] = 1120011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 140005,
                },
            },
        },
        groupCenter = 0,
        sort = 97,
        lock = {
        },
        storydungeonName = "12000078",
        icon = "icon/fuben/levelIcon/Dating/140006.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 140006,
        lineSite1 = {
        },
        lineSite = {
            [1] = "5",
        },
        sortShow = 97,
        levelSite = "5",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [263001] = {
        chapter = 2003,
        group = 4,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 250005,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020022",
        icon = "icon/fuben/levelIcon/Dungeon/5_caochang.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 263001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 1,
        levelSite = "401",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [120016] = {
        chapter = 502,
        group = 3,
        endShow = {
            [1] = 110801,
            [2] = 1108011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 343101301,
                },
                predungeon = {
                    [1] = 120013,
                },
            },
        },
        groupCenter = 1,
        sort = 53,
        lock = {
        },
        storydungeonName = "12000041",
        icon = "icon/fuben/levelIcon/Dating/120016.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120016,
        lineSite1 = {
        },
        lineSite = {
            [1] = "19",
        },
        sortShow = 53,
        levelSite = "17",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [140007] = {
        chapter = 504,
        group = 2,
        endShow = {
            [1] = 112001,
            [2] = 1120011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 140006,
                },
            },
        },
        groupCenter = 0,
        sort = 98,
        lock = {
        },
        storydungeonName = "12000146",
        icon = "icon/fuben/levelIcon/Dating/140007.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 140007,
        lineSite1 = {
        },
        lineSite = {
            [1] = "6",
        },
        sortShow = 98,
        levelSite = "7",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [308314] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308313,
                },
            },
        },
        groupCenter = 0,
        sort = 114,
        lock = {
        },
        storydungeonName = "12010074",
        icon = "icon/fuben/levelIcon/Dungeon/3_wanyouli.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308314,
        lineSite1 = {
        },
        lineSite = {
            [1] = "13",
        },
        sortShow = 0,
        levelSite = "14",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [130009] = {
        chapter = 503,
        group = 2,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130008,
                },
            },
        },
        groupCenter = 0,
        sort = 78,
        lock = {
        },
        storydungeonName = "12000060",
        icon = "icon/fuben/levelIcon/Dungeon/3_louding2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130009,
        lineSite1 = {
        },
        lineSite = {
            [1] = "13",
        },
        sortShow = 78,
        levelSite = "15",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [308311] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308210,
                },
            },
        },
        groupCenter = 0,
        sort = 111,
        lock = {
        },
        storydungeonName = "12010071",
        icon = "icon/fuben/levelIcon/Dungeon/3_ditiezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308311,
        lineSite1 = {
        },
        lineSite = {
            [1] = "10",
        },
        sortShow = 0,
        levelSite = "11",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [308418] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308417,
                },
            },
        },
        groupCenter = 0,
        sort = 118,
        lock = {
        },
        storydungeonName = "12010078",
        icon = "icon/fuben/levelIcon/Dungeon/3_yewanjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308418,
        lineSite1 = {
        },
        lineSite = {
            [1] = "18",
        },
        sortShow = 0,
        levelSite = "17",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [343101401] = {
        chapter = 502,
        group = 3,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120013,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000089",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 343101401,
        lineSite1 = {
        },
        lineSite = {
            [1] = "16",
        },
        sortShow = 49,
        levelSite = "21",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [210003] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 210001,
                },
            },
        },
        groupCenter = 0,
        sort = 3,
        lock = {
        },
        storydungeonName = "301110",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "102",
        },
        sortShow = 3,
        levelSite = "103",
        activeIf = {
            predungeonResult = {
                [1] = {
                    id = 210001,
                    star = 5,
                },
            },
        },
        storydungeonType = 10,
        isHint = false,
    },
    [262002] = {
        chapter = 2003,
        group = 3,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 262001,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020015",
        icon = "icon/fuben/levelIcon/Dungeon/2_ditie.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 262002,
        lineSite1 = {
            [1] = "311",
        },
        lineSite = {
            [1] = "302",
        },
        sortShow = 2,
        levelSite = "305",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [220002] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 220001,
                },
            },
        },
        groupCenter = 0,
        sort = 2,
        lock = {
        },
        storydungeonName = "301121",
        icon = "icon/fuben/levelIcon/Dungeon/2_louding.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 220002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "201",
        },
        sortShow = 2,
        levelSite = "202",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [210005] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predungeonResult = {
                    [1] = {
                        id = 210001,
                        star = 5,
                    },
                },
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 5,
        lock = {
        },
        storydungeonName = "301112",
        icon = "icon/fuben/levelIcon/Dungeon/2_huiyishi.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210005,
        lineSite1 = {
        },
        lineSite = {
            [1] = "105",
        },
        sortShow = 5,
        levelSite = "105",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [261006] = {
        chapter = 2003,
        group = 2,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 261005,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020013",
        icon = "icon/fuben/levelIcon/Dungeon/boss_wanyouli.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 261006,
        lineSite1 = {
            [1] = "502",
        },
        lineSite = {
            [1] = "299",
        },
        sortShow = 6,
        levelSite = "299",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = false,
    },
    [110008] = {
        chapter = 501,
        group = 2,
        endShow = {
            [1] = 110201,
            [2] = 1102011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 321101401,
                },
                predungeon = {
                    [1] = 110006,
                },
            },
        },
        groupCenter = 0,
        sort = 12,
        lock = {
        },
        storydungeonName = "12000008",
        icon = "icon/fuben/levelIcon/Dating/110008.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110008,
        lineSite1 = {
        },
        lineSite = {
            [1] = "12",
        },
        sortShow = 12,
        levelSite = "12",
        activeIf = {
        },
        storydungeonType = 4,
        isHint = false,
    },
    [110024] = {
        chapter = 501,
        group = 5,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110020,
                },
            },
        },
        groupCenter = 0,
        sort = 32,
        lock = {
        },
        storydungeonName = "12000024",
        icon = "icon/fuben/levelIcon/Dungeon/5_caochang.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110024,
        lineSite1 = {
        },
        lineSite = {
            [1] = "33",
        },
        sortShow = 32,
        levelSite = "36",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [140008] = {
        chapter = 504,
        group = 2,
        endShow = {
            [1] = 112001,
            [2] = 1120011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 140007,
                },
            },
        },
        groupCenter = 0,
        sort = 99,
        lock = {
        },
        storydungeonName = "12000147",
        icon = "icon/fuben/levelIcon/Dating/140008.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 140008,
        lineSite1 = {
        },
        lineSite = {
            [1] = "7",
        },
        sortShow = 99,
        levelSite = "8",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [120001] = {
        chapter = 502,
        group = 1,
        endShow = {
            [1] = 110501,
            [2] = 1105011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110025,
                },
            },
        },
        groupCenter = 1,
        sort = 34,
        lock = {
        },
        storydungeonName = "12000026",
        icon = "icon/fuben/levelIcon/Dating/120001.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 34,
        levelSite = "1",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [120017] = {
        chapter = 502,
        group = 3,
        endShow = {
            [1] = 110801,
            [2] = 1108011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120016,
                },
            },
        },
        groupCenter = 0,
        sort = 54,
        lock = {
        },
        storydungeonName = "12000042",
        icon = "icon/fuben/levelIcon/Dating/120017.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120017,
        lineSite1 = {
        },
        lineSite = {
            [1] = "22",
        },
        sortShow = 54,
        levelSite = "20",
        activeIf = {
        },
        storydungeonType = 4,
        isHint = false,
    },
    [261005] = {
        chapter = 2003,
        group = 2,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 261004,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020012",
        icon = "icon/fuben/levelIcon/Dungeon/3_wanyouli.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 261005,
        lineSite1 = {
        },
        lineSite = {
            [1] = "207",
        },
        sortShow = 5,
        levelSite = "208",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [210006] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 210004,
                },
            },
        },
        groupCenter = 0,
        sort = 6,
        lock = {
        },
        storydungeonName = "301113",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210006,
        lineSite1 = {
        },
        lineSite = {
            [1] = "106",
        },
        sortShow = 6,
        levelSite = "106",
        activeIf = {
            predungeonResult = {
                [1] = {
                    id = 210004,
                    star = 4,
                },
            },
        },
        storydungeonType = 10,
        isHint = false,
    },
    [130010] = {
        chapter = 503,
        group = 3,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130007,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130009,
                },
            },
        },
        groupCenter = 0,
        sort = 79,
        lock = {
        },
        storydungeonName = "12000061",
        icon = "icon/fuben/levelIcon/Dating/130010.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130010,
        lineSite1 = {
        },
        lineSite = {
            [1] = "15",
            [2] = "14",
        },
        sortShow = 79,
        levelSite = "16",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [261004] = {
        chapter = 2003,
        group = 2,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 261003,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020011",
        icon = "icon/fuben/levelIcon/Dungeon/2_louding.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 261004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "203",
        },
        sortShow = 4,
        levelSite = "207",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [261003] = {
        chapter = 2003,
        group = 2,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 261002,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020010",
        icon = "icon/fuben/levelIcon/Dungeon/5_ditiezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 261003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "202",
        },
        sortShow = 3,
        levelSite = "203",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [140009] = {
        chapter = 504,
        group = 2,
        endShow = {
            [1] = 112001,
            [2] = 1120011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 140008,
                },
            },
        },
        groupCenter = 1,
        sort = 100,
        lock = {
        },
        storydungeonName = "12000148",
        icon = "icon/fuben/levelIcon/Dating/140009.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 140009,
        lineSite1 = {
        },
        lineSite = {
            [1] = "8",
        },
        sortShow = 100,
        levelSite = "9",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [240001] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 1,
        lock = {
            [1] = {
                predungeon = {
                    [1] = 210001,
                },
                notPass = {
                    [1] = 210099,
                },
            },
            [2] = {
                predungeon = {
                    [1] = 220001,
                },
                notPass = {
                    [1] = 220099,
                },
            },
            [3] = {
                predungeon = {
                    [1] = 230001,
                },
                notPass = {
                    [1] = 230099,
                },
            },
        },
        storydungeonName = "301140",
        icon = "icon/fuben/levelIcon/Dungeon/2_jiaoshi.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 1,
        levelSite = "401",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = true,
    },
    [261002] = {
        chapter = 2003,
        group = 2,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 261001,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020009",
        icon = "icon/fuben/levelIcon/Dungeon/5_putongjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 261002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "201",
        },
        sortShow = 2,
        levelSite = "202",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [261001] = {
        chapter = 2003,
        group = 2,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 250005,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020008",
        icon = "icon/fuben/levelIcon/Dungeon/2_shenshe.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 261001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 1,
        levelSite = "201",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [210007] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 210004,
                },
            },
        },
        groupCenter = 0,
        sort = 7,
        lock = {
        },
        storydungeonName = "301114",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210007,
        lineSite1 = {
        },
        lineSite = {
            [1] = "107",
        },
        sortShow = 7,
        levelSite = "107",
        activeIf = {
            predungeonResult = {
                [1] = {
                    id = 210004,
                    star = 5,
                },
            },
        },
        storydungeonType = 10,
        isHint = false,
    },
    [260007] = {
        chapter = 2003,
        group = 1,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 260006,
                },
            },
            [2] = {
                predungeon = {
                    [1] = 260005,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020007",
        icon = "icon/fuben/levelIcon/Dungeon/boss_kabala.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 260007,
        lineSite1 = {
            [1] = "501",
        },
        lineSite = {
        },
        sortShow = 7,
        levelSite = "199",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = false,
    },
    [110009] = {
        chapter = 501,
        group = 2,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110008,
                },
            },
        },
        groupCenter = 0,
        sort = 13,
        lock = {
        },
        storydungeonName = "12000009",
        icon = "icon/fuben/levelIcon/Dungeon/5_yewanshatan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110009,
        lineSite1 = {
        },
        lineSite = {
            [1] = "13",
        },
        sortShow = 13,
        levelSite = "13",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [110025] = {
        chapter = 501,
        group = 5,
        endShow = {
            [1] = 110801,
            [2] = 1108011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110022,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110023,
                },
            },
        },
        groupCenter = 1,
        sort = 33,
        lock = {
        },
        storydungeonName = "12000025",
        icon = "icon/fuben/levelIcon/Dating/110025.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110025,
        lineSite1 = {
        },
        lineSite = {
            [1] = "34",
            [2] = "35",
        },
        sortShow = 33,
        levelSite = "37",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [260006] = {
        chapter = 2003,
        group = 1,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 260003,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020006",
        icon = "icon/fuben/levelIcon/Dungeon/2_icestreet.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 260006,
        lineSite1 = {
            [1] = "111",
        },
        lineSite = {
            [1] = "108",
        },
        sortShow = 6,
        levelSite = "108",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [321101301] = {
        chapter = 501,
        group = 2,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110006,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000081",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 321101301,
        lineSite1 = {
        },
        lineSite = {
            [1] = "10",
        },
        sortShow = 9,
        levelSite = "10",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [120018] = {
        chapter = 502,
        group = 3,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120014,
                },
            },
        },
        groupCenter = 0,
        sort = 55,
        lock = {
        },
        storydungeonName = "12000043",
        icon = "icon/fuben/levelIcon/Dungeon/3_louding2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120018,
        lineSite1 = {
        },
        lineSite = {
            [1] = "21",
        },
        sortShow = 55,
        levelSite = "32",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [260005] = {
        chapter = 2003,
        group = 1,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 260002,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020005",
        icon = "icon/fuben/levelIcon/Dungeon/2_kabala2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 260005,
        lineSite1 = {
            [1] = "112",
        },
        lineSite = {
            [1] = "107",
        },
        sortShow = 5,
        levelSite = "110",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [210008] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 210005,
                },
            },
        },
        groupCenter = 0,
        sort = 8,
        lock = {
        },
        storydungeonName = "301115",
        icon = "icon/fuben/levelIcon/Dungeon/2_xuedichezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210008,
        lineSite1 = {
        },
        lineSite = {
            [1] = "108",
        },
        sortShow = 8,
        levelSite = "108",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [130011] = {
        chapter = 503,
        group = 3,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130010,
                },
            },
        },
        groupCenter = 1,
        sort = 80,
        lock = {
        },
        storydungeonName = "12000062",
        icon = "icon/fuben/levelIcon/Dungeon/5_shatan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130011,
        lineSite1 = {
        },
        lineSite = {
            [1] = "16",
        },
        sortShow = 80,
        levelSite = "17",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [260004] = {
        chapter = 2003,
        group = 1,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 260002,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020004",
        icon = "icon/fuben/levelIcon/Dungeon/2_xuedichezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 260004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "106",
        },
        sortShow = 4,
        levelSite = "109",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [260002] = {
        chapter = 2003,
        group = 1,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 260001,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020002",
        icon = "icon/fuben/levelIcon/Dungeon/2_kabala1.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 260002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "101",
        },
        sortShow = 2,
        levelSite = "104",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [250002] = {
        chapter = 2002,
        group = 1,
        endShow = {
            [1] = 110302,
            [2] = 1103021,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 240099,
                    [2] = 230099,
                    [3] = 220099,
                    [4] = 210099,
                },
            },
        },
        groupCenter = 0,
        sort = 1,
        lock = {
        },
        storydungeonName = "301157",
        icon = "icon/fuben/levelIcon/Dungeon/boss_wanyouli.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 250002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "201",
        },
        sortShow = 2,
        levelSite = "201",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = false,
    },
    [240003] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 240001,
                },
            },
        },
        groupCenter = 0,
        sort = 3,
        lock = {
        },
        storydungeonName = "301142",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "402",
        },
        sortShow = 3,
        levelSite = "403",
        activeIf = {
            predungeonResult = {
                [1] = {
                    id = 240001,
                    star = 5,
                },
            },
        },
        storydungeonType = 10,
        isHint = false,
    },
    [240099] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 240013,
                },
            },
            [2] = {
                predating = {
                },
                predungeon = {
                    [1] = 240014,
                },
            },
        },
        groupCenter = 0,
        sort = 15,
        lock = {
        },
        storydungeonName = "301154",
        icon = "icon/fuben/levelIcon/Dungeon/boss_wanyouli.png",
        speTypeParam = {
        },
        previewAni = {
            [1] = "ActionPre_99",
            [2] = "ActionAfter_99",
        },
        id = 240099,
        lineSite1 = {
            [1] = "504",
        },
        lineSite = {
            [1] = "402",
        },
        sortShow = 15,
        levelSite = "499",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = true,
    },
    [240014] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predungeonResult = {
                    [1] = {
                        id = 240010,
                        star = 5,
                    },
                },
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 14,
        lock = {
        },
        storydungeonName = "301153",
        icon = "icon/fuben/levelIcon/Dungeon/2_youleyuan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240014,
        lineSite1 = {
            [1] = "417",
        },
        lineSite = {
            [1] = "415",
        },
        sortShow = 14,
        levelSite = "414",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [210009] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predungeonResult = {
                    [1] = {
                        id = 210004,
                        star = 4,
                    },
                },
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 9,
        lock = {
        },
        storydungeonName = "301116",
        icon = "icon/fuben/levelIcon/Dungeon/2_kabala1.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210009,
        lineSite1 = {
        },
        lineSite = {
            [1] = "109",
        },
        sortShow = 9,
        levelSite = "109",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [240012] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 240010,
                },
            },
        },
        groupCenter = 0,
        sort = 12,
        lock = {
        },
        storydungeonName = "301151",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240012,
        lineSite1 = {
        },
        lineSite = {
            [1] = "413",
        },
        sortShow = 12,
        levelSite = "412",
        activeIf = {
            predungeonResult = {
                [1] = {
                    id = 240010,
                    star = 5,
                },
            },
        },
        storydungeonType = 10,
        isHint = false,
    },
    [110010] = {
        chapter = 501,
        group = 3,
        endShow = {
            [1] = 110701,
            [2] = 1107011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 321101301,
                },
                predungeon = {
                    [1] = 110006,
                },
            },
        },
        groupCenter = 1,
        sort = 14,
        lock = {
        },
        storydungeonName = "12000010",
        icon = "icon/fuben/levelIcon/Dating/110010.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110010,
        lineSite1 = {
        },
        lineSite = {
            [1] = "16",
        },
        sortShow = 14,
        levelSite = "17",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [308313] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308312,
                },
            },
        },
        groupCenter = 0,
        sort = 113,
        lock = {
        },
        storydungeonName = "12010073",
        icon = "icon/fuben/levelIcon/Dungeon/3_louding2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308313,
        lineSite1 = {
        },
        lineSite = {
            [1] = "12",
        },
        sortShow = 0,
        levelSite = "12",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [240010] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 240006,
                },
            },
            [2] = {
                predating = {
                },
                predungeon = {
                    [1] = 240007,
                },
            },
        },
        groupCenter = 0,
        sort = 10,
        lock = {
        },
        storydungeonName = "301149",
        icon = "icon/fuben/levelIcon/Dungeon/3_zcxiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240010,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 10,
        levelSite = "410",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [351101301] = {
        chapter = 502,
        group = 5,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120023,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000091",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 351101301,
        lineSite1 = {
        },
        lineSite = {
            [1] = "30",
        },
        sortShow = 61,
        levelSite = "35",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [120019] = {
        chapter = 502,
        group = 4,
        endShow = {
            [1] = 110501,
            [2] = 1105011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120018,
                },
            },
        },
        groupCenter = 0,
        sort = 56,
        lock = {
        },
        storydungeonName = "12000044",
        icon = "icon/fuben/levelIcon/Dating/120019.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120019,
        lineSite1 = {
        },
        lineSite = {
            [1] = "26",
        },
        sortShow = 56,
        levelSite = "31",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [120003] = {
        chapter = 502,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120001,
                },
            },
        },
        groupCenter = 0,
        sort = 36,
        lock = {
        },
        storydungeonName = "12000028",
        icon = "icon/fuben/levelIcon/Dating/120003.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "2",
        },
        sortShow = 36,
        levelSite = "2",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [210010] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predungeonResult = {
                    [1] = {
                        id = 210004,
                        star = 5,
                    },
                },
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 10,
        lock = {
        },
        storydungeonName = "301117",
        icon = "icon/fuben/levelIcon/Dungeon/2_kabala2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210010,
        lineSite1 = {
        },
        lineSite = {
            [1] = "110",
        },
        sortShow = 10,
        levelSite = "110",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [130012] = {
        chapter = 503,
        group = 3,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130010,
                },
            },
        },
        groupCenter = 0,
        sort = 81,
        lock = {
        },
        storydungeonName = "12000063",
        icon = "icon/fuben/levelIcon/Dungeon/5_jiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130012,
        lineSite1 = {
        },
        lineSite = {
            [1] = "28",
        },
        sortShow = 81,
        levelSite = "20",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [240008] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 240005,
                    [2] = 230009,
                },
            },
        },
        groupCenter = 0,
        sort = 8,
        lock = {
        },
        storydungeonName = "301147",
        icon = "icon/fuben/levelIcon/Dungeon/3_caochang.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240008,
        lineSite1 = {
        },
        lineSite = {
            [1] = "407",
        },
        sortShow = 8,
        levelSite = "408",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [220004] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 220003,
                },
            },
        },
        groupCenter = 0,
        sort = 4,
        lock = {
        },
        storydungeonName = "301123",
        icon = "icon/fuben/levelIcon/Dating/140005.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 220004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "203",
        },
        sortShow = 4,
        levelSite = "204",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [240006] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predungeonResult = {
                    [1] = {
                        id = 240001,
                        star = 5,
                    },
                },
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 6,
        lock = {
        },
        storydungeonName = "301145",
        icon = "icon/fuben/levelIcon/Dungeon/2_xiaoyuancaochang.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240006,
        lineSite1 = {
            [1] = "409",
        },
        lineSite = {
            [1] = "405",
        },
        sortShow = 6,
        levelSite = "406",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [240005] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predungeonResult = {
                    [1] = {
                        id = 240001,
                        star = 4,
                    },
                },
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 5,
        lock = {
        },
        storydungeonName = "301144",
        icon = "icon/fuben/levelIcon/Dungeon/2_louding.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240005,
        lineSite1 = {
        },
        lineSite = {
            [1] = "404",
        },
        sortShow = 5,
        levelSite = "405",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [240004] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 240001,
                },
            },
        },
        groupCenter = 0,
        sort = 4,
        lock = {
        },
        storydungeonName = "301143",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "403",
        },
        sortShow = 4,
        levelSite = "404",
        activeIf = {
            predungeonResult = {
                [1] = {
                    id = 240001,
                    star = 6,
                },
            },
        },
        storydungeonType = 10,
        isHint = false,
    },
    [240002] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 240001,
                },
            },
        },
        groupCenter = 0,
        sort = 2,
        lock = {
        },
        storydungeonName = "301141",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "401",
        },
        sortShow = 2,
        levelSite = "402",
        activeIf = {
            predungeonResult = {
                [1] = {
                    id = 240001,
                    star = 4,
                },
            },
        },
        storydungeonType = 10,
        isHint = false,
    },
    [210011] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 210008,
                },
            },
            [2] = {
                predating = {
                },
                predungeon = {
                    [1] = 210010,
                },
            },
        },
        groupCenter = 0,
        sort = 11,
        lock = {
        },
        storydungeonName = "301118",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210011,
        lineSite1 = {
        },
        lineSite = {
            [1] = "111",
            [2] = "112",
        },
        sortShow = 11,
        levelSite = "111",
        activeIf = {
            predungeon = {
                [1] = 210008,
                [2] = 210010,
            },
        },
        storydungeonType = 10,
        isHint = false,
    },
    [263002] = {
        chapter = 2003,
        group = 4,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 263001,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020023",
        icon = "icon/fuben/levelIcon/Dungeon/2_xiaoyuancaochang.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 263002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "401",
        },
        sortShow = 2,
        levelSite = "405",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [110011] = {
        chapter = 501,
        group = 3,
        endShow = {
            [1] = 110701,
            [2] = 1107011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 321101401,
                },
                predungeon = {
                    [1] = 110006,
                },
            },
        },
        groupCenter = 0,
        sort = 15,
        lock = {
        },
        storydungeonName = "12000011",
        icon = "icon/fuben/levelIcon/Dating/110011.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110011,
        lineSite1 = {
        },
        lineSite = {
            [1] = "15",
        },
        sortShow = 15,
        levelSite = "15",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [230002] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 230001,
                },
            },
        },
        groupCenter = 0,
        sort = 2,
        lock = {
        },
        storydungeonName = "301130",
        icon = "icon/fuben/levelIcon/Dating/140005.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 230002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "301",
        },
        sortShow = 2,
        levelSite = "302",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [355101501] = {
        chapter = 503,
        group = 2,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130003,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000096",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 355101501,
        lineSite1 = {
        },
        lineSite = {
            [1] = "7",
        },
        sortShow = 72,
        levelSite = "6",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [120004] = {
        chapter = 502,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 336101101,
                },
                predungeon = {
                    [1] = 120003,
                },
            },
        },
        groupCenter = 0,
        sort = 39,
        lock = {
        },
        storydungeonName = "12000029",
        icon = "icon/fuben/levelIcon/Dating/120004.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "6",
        },
        sortShow = 39,
        levelSite = "6",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [120020] = {
        chapter = 502,
        group = 4,
        endShow = {
            [1] = 110501,
            [2] = 1105011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120016,
                },
            },
        },
        groupCenter = 1,
        sort = 57,
        lock = {
        },
        storydungeonName = "12000045",
        icon = "icon/fuben/levelIcon/Dating/120020.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120020,
        lineSite1 = {
        },
        lineSite = {
            [1] = "24",
        },
        sortShow = 57,
        levelSite = "23",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [308420] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308419,
                },
            },
        },
        groupCenter = 0,
        sort = 120,
        lock = {
        },
        storydungeonName = "12010080",
        icon = "icon/fuben/levelIcon/Dungeon/3_feichuan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308420,
        lineSite1 = {
        },
        lineSite = {
            [1] = "20",
        },
        sortShow = 0,
        levelSite = "20",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [220099] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 220008,
                },
            },
        },
        groupCenter = 0,
        sort = 9,
        lock = {
        },
        storydungeonName = "301128",
        icon = "icon/fuben/levelIcon/Dungeon/boss_shenshe.png",
        speTypeParam = {
        },
        previewAni = {
            [1] = "ActionPre_99",
            [2] = "ActionAfter_99",
        },
        id = 220099,
        lineSite1 = {
            [1] = "502",
        },
        lineSite = {
            [1] = "299",
        },
        sortShow = 9,
        levelSite = "299",
        activeIf = {
        },
        storydungeonType = 9,
        isHint = true,
    },
    [336101201] = {
        chapter = 502,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120003,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000088",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 336101201,
        lineSite1 = {
        },
        lineSite = {
            [1] = "3",
        },
        sortShow = 38,
        levelSite = "5",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [220008] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 220007,
                },
            },
        },
        groupCenter = 0,
        sort = 8,
        lock = {
        },
        storydungeonName = "301127",
        icon = "icon/fuben/levelIcon/Dungeon/3_wanyouli.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 220008,
        lineSite1 = {
        },
        lineSite = {
            [1] = "207",
        },
        sortShow = 8,
        levelSite = "208",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [220006] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 220005,
                },
            },
        },
        groupCenter = 0,
        sort = 6,
        lock = {
        },
        storydungeonName = "301125",
        icon = "icon/fuben/levelIcon/Dating/120011.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 220006,
        lineSite1 = {
        },
        lineSite = {
            [1] = "205",
        },
        sortShow = 6,
        levelSite = "206",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [220005] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 220004,
                    [2] = 210005,
                    [3] = 210099,
                    [4] = 220099,
                },
            },
        },
        groupCenter = 0,
        sort = 5,
        lock = {
        },
        storydungeonName = "301124",
        icon = "icon/fuben/levelIcon/Dungeon/3_yewanjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 220005,
        lineSite1 = {
        },
        lineSite = {
            [1] = "204",
        },
        sortShow = 5,
        levelSite = "205",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [240007] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predungeonResult = {
                    [1] = {
                        id = 240001,
                        star = 6,
                    },
                },
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 7,
        lock = {
        },
        storydungeonName = "301146",
        icon = "icon/fuben/levelIcon/Dungeon/2_juyuan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240007,
        lineSite1 = {
            [1] = "410",
        },
        lineSite = {
            [1] = "406",
        },
        sortShow = 7,
        levelSite = "407",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [220003] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 220002,
                },
            },
        },
        groupCenter = 0,
        sort = 3,
        lock = {
        },
        storydungeonName = "301122",
        icon = "icon/fuben/levelIcon/Dungeon/5_phxiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 220003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "202",
        },
        sortShow = 3,
        levelSite = "203",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [262001] = {
        chapter = 2003,
        group = 3,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 250005,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020014",
        icon = "icon/fuben/levelIcon/Dungeon/3_yewanjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 262001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 1,
        levelSite = "301",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [220001] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 1,
        lock = {
            [1] = {
                predungeon = {
                    [1] = 210001,
                },
                notPass = {
                    [1] = 210099,
                },
            },
            [2] = {
                predungeon = {
                    [1] = 230001,
                },
                notPass = {
                    [1] = 230099,
                },
            },
            [3] = {
                predungeon = {
                    [1] = 240001,
                },
                notPass = {
                    [1] = 240099,
                },
            },
        },
        storydungeonName = "301120",
        icon = "icon/fuben/levelIcon/Dungeon/3_putongjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 220001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 1,
        levelSite = "201",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = true,
    },
    [263006] = {
        chapter = 2003,
        group = 4,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 263005,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020027",
        icon = "icon/fuben/levelIcon/Dungeon/2_shenshe.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 263006,
        lineSite1 = {
            [1] = "416",
        },
        lineSite = {
            [1] = "412",
        },
        sortShow = 6,
        levelSite = "413",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [110012] = {
        chapter = 501,
        group = 3,
        endShow = {
            [1] = 110701,
            [2] = 1107011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110011,
                },
            },
        },
        groupCenter = 0,
        sort = 18,
        lock = {
        },
        storydungeonName = "12000012",
        icon = "icon/fuben/levelIcon/Dating/110012.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110012,
        lineSite1 = {
        },
        lineSite = {
            [1] = "19",
        },
        sortShow = 18,
        levelSite = "16",
        activeIf = {
        },
        storydungeonType = 4,
        isHint = false,
    },
    [308210] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308209,
                },
            },
        },
        groupCenter = 0,
        sort = 110,
        lock = {
        },
        storydungeonName = "12010070",
        icon = "icon/fuben/levelIcon/Dungeon/3_juchangwai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308210,
        lineSite1 = {
        },
        lineSite = {
            [1] = "9",
        },
        sortShow = 0,
        levelSite = "10",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [308417] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308416,
                },
            },
        },
        groupCenter = 0,
        sort = 117,
        lock = {
        },
        storydungeonName = "12010077",
        icon = "icon/fuben/levelIcon/Dungeon/3_jiaowai2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308417,
        lineSite1 = {
        },
        lineSite = {
            [1] = "17",
        },
        sortShow = 0,
        levelSite = "18",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [120005] = {
        chapter = 502,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 336101201,
                },
                predungeon = {
                    [1] = 120003,
                },
            },
        },
        groupCenter = 0,
        sort = 40,
        lock = {
        },
        storydungeonName = "12000030",
        icon = "icon/fuben/levelIcon/Dating/120005.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120005,
        lineSite1 = {
        },
        lineSite = {
            [1] = "5",
        },
        sortShow = 40,
        levelSite = "7",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [120021] = {
        chapter = 502,
        group = 3,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120017,
                },
            },
        },
        groupCenter = 0,
        sort = 58,
        lock = {
        },
        storydungeonName = "12000046",
        icon = "icon/fuben/levelIcon/Dungeon/5_ditiezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120021,
        lineSite1 = {
        },
        lineSite = {
            [1] = "23",
        },
        sortShow = 58,
        levelSite = "22",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [353101301] = {
        chapter = 503,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130001,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000093",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 353101301,
        lineSite1 = {
        },
        lineSite = {
            [1] = "1",
        },
        sortShow = 67,
        levelSite = "2",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [336101101] = {
        chapter = 502,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120003,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000087",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 336101101,
        lineSite1 = {
        },
        lineSite = {
            [1] = "4",
        },
        sortShow = 37,
        levelSite = "4",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [130014] = {
        chapter = 503,
        group = 3,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130021,
                },
            },
        },
        groupCenter = 0,
        sort = 84,
        lock = {
        },
        storydungeonName = "12000065",
        icon = "icon/cg/wanyouli_3_1.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130014,
        lineSite1 = {
        },
        lineSite = {
            [1] = "20",
        },
        sortShow = 84,
        levelSite = "32",
        activeIf = {
        },
        storydungeonType = 7,
        isHint = false,
    },
    [308207] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308206,
                },
            },
        },
        groupCenter = 0,
        sort = 107,
        lock = {
        },
        storydungeonName = "12010067",
        icon = "icon/fuben/levelIcon/Dungeon/3_phxiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308207,
        lineSite1 = {
        },
        lineSite = {
            [1] = "6",
        },
        sortShow = 0,
        levelSite = "7",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [210004] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predungeonResult = {
                    [1] = {
                        id = 210001,
                        star = 4,
                    },
                },
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 4,
        lock = {
        },
        storydungeonName = "301111",
        icon = "icon/fuben/levelIcon/Dungeon/2_juyuan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 210004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "104",
        },
        sortShow = 4,
        levelSite = "104",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [319101401] = {
        chapter = 501,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110002,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000080",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 319101401,
        lineSite1 = {
        },
        lineSite = {
            [1] = "3",
        },
        sortShow = 4,
        levelSite = "4",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [240009] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 240008,
                },
            },
        },
        groupCenter = 0,
        sort = 9,
        lock = {
        },
        storydungeonName = "301148",
        icon = "icon/fuben/levelIcon/Dating/tiantaiyewan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240009,
        lineSite1 = {
        },
        lineSite = {
            [1] = "411",
        },
        sortShow = 9,
        levelSite = "409",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [308208] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308207,
                },
            },
        },
        groupCenter = 0,
        sort = 108,
        lock = {
        },
        storydungeonName = "12010068",
        icon = "icon/fuben/levelIcon/Dungeon/5_wanyouli.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308208,
        lineSite1 = {
        },
        lineSite = {
            [1] = "7",
        },
        sortShow = 0,
        levelSite = "8",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [262003] = {
        chapter = 2003,
        group = 3,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 262001,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020016",
        icon = "icon/fuben/levelIcon/Dungeon/2_shenshe.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 262003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "303",
        },
        sortShow = 3,
        levelSite = "306",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [308209] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308208,
                },
            },
        },
        groupCenter = 0,
        sort = 109,
        lock = {
        },
        storydungeonName = "12010069",
        icon = "icon/fuben/levelIcon/Dungeon/3_juchangwai2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308209,
        lineSite1 = {
        },
        lineSite = {
            [1] = "8",
        },
        sortShow = 0,
        levelSite = "9",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [308104] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308103,
                },
            },
        },
        groupCenter = 0,
        sort = 104,
        lock = {
        },
        storydungeonName = "12010064",
        icon = "icon/fuben/levelIcon/Dungeon/3_caochang.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308104,
        lineSite1 = {
        },
        lineSite = {
            [1] = "3",
        },
        sortShow = 0,
        levelSite = "4",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [110013] = {
        chapter = 501,
        group = 3,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110012,
                },
            },
        },
        groupCenter = 0,
        sort = 19,
        lock = {
        },
        storydungeonName = "12000013",
        icon = "icon/fuben/levelIcon/Dungeon/5_jiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110013,
        lineSite1 = {
        },
        lineSite = {
            [1] = "20",
        },
        sortShow = 19,
        levelSite = "21",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [230001] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                },
            },
        },
        groupCenter = 0,
        sort = 1,
        lock = {
            [1] = {
                predungeon = {
                    [1] = 210001,
                },
                notPass = {
                    [1] = 210099,
                },
            },
            [2] = {
                predungeon = {
                    [1] = 220001,
                },
                notPass = {
                    [1] = 220099,
                },
            },
            [3] = {
                predungeon = {
                    [1] = 240001,
                },
                notPass = {
                    [1] = 240099,
                },
            },
        },
        storydungeonName = "301129",
        icon = "icon/fuben/levelIcon/Dungeon/2_demwaibu.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 230001,
        lineSite1 = {
        },
        lineSite = {
        },
        sortShow = 1,
        levelSite = "301",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = true,
    },
    [110023] = {
        chapter = 501,
        group = 5,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110019,
                },
            },
        },
        groupCenter = 0,
        sort = 31,
        lock = {
        },
        storydungeonName = "12000023",
        icon = "icon/fuben/levelIcon/Dungeon/3_jiaowai2.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110023,
        lineSite1 = {
        },
        lineSite = {
            [1] = "37",
        },
        sortShow = 31,
        levelSite = "34",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [120006] = {
        chapter = 502,
        group = 2,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120005,
                },
            },
        },
        groupCenter = 1,
        sort = 41,
        lock = {
        },
        storydungeonName = "12000031",
        icon = "icon/fuben/levelIcon/Dating/120006.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120006,
        lineSite1 = {
        },
        lineSite = {
            [1] = "7",
        },
        sortShow = 41,
        levelSite = "8",
        activeIf = {
        },
        storydungeonType = 4,
        isHint = false,
    },
    [120022] = {
        chapter = 502,
        group = 5,
        endShow = {
            [1] = 110501,
            [2] = 1105011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 351101301,
                },
                predungeon = {
                    [1] = 120023,
                },
            },
            [2] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120019,
                },
            },
        },
        groupCenter = 0,
        sort = 59,
        lock = {
        },
        storydungeonName = "12000047",
        icon = "icon/cg/wanyouli_2_1.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120022,
        lineSite1 = {
        },
        lineSite = {
            [1] = "31",
            [2] = "27",
        },
        sortShow = 59,
        levelSite = "33",
        activeIf = {
        },
        storydungeonType = 7,
        isHint = false,
    },
    [262004] = {
        chapter = 2003,
        group = 3,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 262002,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020017",
        icon = "icon/fuben/levelIcon/Dungeon/2_jiaoshi.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 262004,
        lineSite1 = {
        },
        lineSite = {
            [1] = "310",
        },
        sortShow = 4,
        levelSite = "307",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [308416] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308315,
                },
            },
        },
        groupCenter = 0,
        sort = 116,
        lock = {
        },
        storydungeonName = "12010076",
        icon = "icon/fuben/levelIcon/Dungeon/3_ditiezhan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308416,
        lineSite1 = {
        },
        lineSite = {
            [1] = "16",
        },
        sortShow = 0,
        levelSite = "16",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [130015] = {
        chapter = 503,
        group = 4,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130014,
                },
            },
        },
        groupCenter = 0,
        sort = 85,
        lock = {
        },
        storydungeonName = "12000066",
        icon = "icon/fuben/levelIcon/Dungeon/5_caochang.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130015,
        lineSite1 = {
        },
        lineSite = {
            [1] = "23",
        },
        sortShow = 85,
        levelSite = "23",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [308315] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308314,
                },
            },
        },
        groupCenter = 0,
        sort = 115,
        lock = {
        },
        storydungeonName = "12010075",
        icon = "icon/fuben/levelIcon/Dungeon/3_yewanjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308315,
        lineSite1 = {
        },
        lineSite = {
            [1] = "15",
        },
        sortShow = 0,
        levelSite = "15",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [325101301] = {
        chapter = 501,
        group = 3,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110011,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000083",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 325101301,
        lineSite1 = {
        },
        lineSite = {
            [1] = "17",
        },
        sortShow = 16,
        levelSite = "19",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [130002] = {
        chapter = 503,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 353101301,
                },
                predungeon = {
                    [1] = 130001,
                },
            },
        },
        groupCenter = 0,
        sort = 69,
        lock = {
        },
        storydungeonName = "12000053",
        icon = "icon/fuben/levelIcon/Dating/130002.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "3",
        },
        sortShow = 69,
        levelSite = "5",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [240011] = {
        chapter = 2001,
        group = 1,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                predating = {
                },
                predungeon = {
                    [1] = 240010,
                },
            },
        },
        groupCenter = 0,
        sort = 11,
        lock = {
        },
        storydungeonName = "301150",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 240011,
        lineSite1 = {
        },
        lineSite = {
            [1] = "412",
        },
        sortShow = 11,
        levelSite = "411",
        activeIf = {
            predungeonResult = {
                [1] = {
                    id = 240010,
                    star = 4,
                },
            },
        },
        storydungeonType = 10,
        isHint = false,
    },
    [330101201] = {
        chapter = 501,
        group = 4,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 110018,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12000086",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 330101201,
        lineSite1 = {
        },
        lineSite = {
            [1] = "28",
        },
        sortShow = 26,
        levelSite = "25",
        activeIf = {
        },
        storydungeonType = 6,
        isHint = false,
    },
    [262005] = {
        chapter = 2003,
        group = 3,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 262003,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020018",
        icon = "icon/fuben/levelIcon/Dungeon/3_phxiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 262005,
        lineSite1 = {
            [1] = "308",
        },
        lineSite = {
            [1] = "307",
        },
        sortShow = 5,
        levelSite = "308",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [130003] = {
        chapter = 503,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 353101401,
                },
                predungeon = {
                    [1] = 130001,
                },
            },
        },
        groupCenter = 0,
        sort = 70,
        lock = {
        },
        storydungeonName = "12000054",
        icon = "icon/fuben/levelIcon/Dating/130003.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130003,
        lineSite1 = {
        },
        lineSite = {
            [1] = "4",
        },
        sortShow = 70,
        levelSite = "4",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [120002] = {
        chapter = 502,
        group = 1,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120001,
                },
            },
        },
        groupCenter = 0,
        sort = 35,
        lock = {
        },
        storydungeonName = "12000027",
        icon = "icon/fuben/levelIcon/Dungeon/5_yewanjiedao.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120002,
        lineSite1 = {
        },
        lineSite = {
            [1] = "1",
        },
        sortShow = 35,
        levelSite = "3",
        activeIf = {
        },
        storydungeonType = 5,
        isHint = false,
    },
    [110014] = {
        chapter = 501,
        group = 3,
        endShow = {
            [1] = 110701,
            [2] = 1107011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                    [1] = 325101301,
                },
                predungeon = {
                    [1] = 110011,
                },
            },
        },
        groupCenter = 0,
        sort = 20,
        lock = {
        },
        storydungeonName = "12000014",
        icon = "icon/fuben/levelIcon/Dating/110014.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 110014,
        lineSite1 = {
        },
        lineSite = {
            [1] = "22",
        },
        sortShow = 20,
        levelSite = "29",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [308206] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308105,
                },
            },
        },
        groupCenter = 0,
        sort = 106,
        lock = {
        },
        storydungeonName = "12010066",
        icon = "icon/fuben/levelIcon/Dungeon/3_zcxiaowai.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308206,
        lineSite1 = {
        },
        lineSite = {
            [1] = "5",
        },
        sortShow = 0,
        levelSite = "5",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [308103] = {
        chapter = 701,
        group = 0,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 308102,
                },
            },
        },
        groupCenter = 0,
        sort = 103,
        lock = {
        },
        storydungeonName = "12010063",
        icon = "icon/fuben/levelIcon/Dungeon/3_shatan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 308103,
        lineSite1 = {
        },
        lineSite = {
            [1] = "2",
        },
        sortShow = 0,
        levelSite = "3",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [120007] = {
        chapter = 502,
        group = 2,
        endShow = {
            [1] = 110301,
            [2] = 1103011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120004,
                },
            },
        },
        groupCenter = 0,
        sort = 42,
        lock = {
        },
        storydungeonName = "12000032",
        icon = "icon/fuben/levelIcon/Dating/120007.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120007,
        lineSite1 = {
        },
        lineSite = {
            [1] = "9",
        },
        sortShow = 42,
        levelSite = "11",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [120023] = {
        chapter = 502,
        group = 4,
        endShow = {
            [1] = 110501,
            [2] = 1105011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 120020,
                },
            },
        },
        groupCenter = 0,
        sort = 60,
        lock = {
        },
        storydungeonName = "12000048",
        icon = "icon/fuben/levelIcon/Dating/120023.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 120023,
        lineSite1 = {
        },
        lineSite = {
            [1] = "25",
        },
        sortShow = 60,
        levelSite = "26",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [262006] = {
        chapter = 2003,
        group = 3,
        endShow = {
        },
        unlock = {
            [1] = {
                predungeon = {
                    [1] = 262003,
                },
            },
        },
        groupCenter = 0,
        sort = 0,
        lock = {
        },
        storydungeonName = "12020019",
        icon = "icon/fuben/levelIcon/Dungeon/2_youleyuan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 262006,
        lineSite1 = {
        },
        lineSite = {
            [1] = "306",
        },
        sortShow = 6,
        levelSite = "309",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [130013] = {
        chapter = 503,
        group = 3,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130010,
                },
            },
        },
        groupCenter = 0,
        sort = 82,
        lock = {
        },
        storydungeonName = "12000064",
        icon = "icon/fuben/levelIcon/Dungeon/3_feichuan.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130013,
        lineSite1 = {
        },
        lineSite = {
            [1] = "19",
        },
        sortShow = 82,
        levelSite = "21",
        activeIf = {
        },
        storydungeonType = 3,
        isHint = false,
    },
    [130016] = {
        chapter = 503,
        group = 3,
        endShow = {
            [1] = 110101,
            [2] = 1101011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130013,
                },
            },
        },
        groupCenter = 0,
        sort = 86,
        lock = {
        },
        storydungeonName = "12000067",
        icon = "icon/fuben/levelIcon/Dating/130016.png",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 130016,
        lineSite1 = {
        },
        lineSite = {
            [1] = "17",
        },
        sortShow = 86,
        levelSite = "31",
        activeIf = {
        },
        storydungeonType = 2,
        isHint = false,
    },
    [131021] = {
        chapter = 503,
        group = 3,
        endShow = {
            [1] = 111001,
            [2] = 1110011,
        },
        unlock = {
            [1] = {
                spirit = 0,
                predating = {
                },
                predungeon = {
                    [1] = 130021,
                },
            },
        },
        groupCenter = 0,
        sort = 83,
        lock = {
        },
        storydungeonName = "12010060",
        icon = "",
        speTypeParam = {
        },
        previewAni = {
        },
        id = 131021,
        lineSite1 = {
        },
        lineSite = {
            [1] = "21",
        },
        sortShow = 83,
        levelSite = "36",
        activeIf = {
        },
        storydungeonType = 8,
        isHint = false,
    },
}