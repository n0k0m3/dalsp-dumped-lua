return {
    [1] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 1,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279101,
        camp = {
        },
        city = 1,
        sTime = "2019-11-27 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [2] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 2,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279102,
        camp = {
        },
        city = 2,
        sTime = "2019-11-27 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [3] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 3,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279103,
        camp = {
        },
        city = 3,
        sTime = "2019-11-27 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [4] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 4,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279104,
        camp = {
        },
        city = 4,
        sTime = "2019-11-27 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [5] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 5,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279105,
        camp = {
        },
        city = 5,
        sTime = "2019-11-28 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [6] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 6,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279106,
        camp = {
        },
        city = 6,
        sTime = "2019-11-28 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [7] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 7,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279107,
        camp = {
        },
        city = 7,
        sTime = "2019-11-28 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [8] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 8,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279108,
        camp = {
        },
        city = 8,
        sTime = "2019-11-28 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [9] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 9,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279109,
        camp = {
        },
        city = 9,
        sTime = "2019-11-29 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [10] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 10,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279110,
        camp = {
        },
        city = 10,
        sTime = "2019-11-29 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [11] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 11,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279111,
        camp = {
        },
        city = 11,
        sTime = "2019-11-29 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [12] = {
        isInvade = false,
        invadeIcon = {
        },
        devilIcon = "",
        id = 12,
        invadeCamp = {
            [1] = 0,
        },
        displayDetail = 1,
        dungeon = 279112,
        camp = {
        },
        city = 12,
        sTime = "2019-11-29 00:00:00",
        eTime = "",
        stage = 0,
        buffDescribe = {
        },
    },
    [13] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_1.png",
        },
        devilIcon = "",
        id = 13,
        invadeCamp = {
            [1] = 1,
        },
        displayDetail = 1,
        dungeon = 279102,
        camp = {
            [1] = 1,
        },
        city = 2,
        sTime = "2019-11-30 00:00:00",
        eTime = "2019-12-02 23:59:59",
        stage = 2,
        buffDescribe = {
        },
    },
    [14] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_1.png",
        },
        devilIcon = "",
        id = 14,
        invadeCamp = {
            [1] = 1,
        },
        displayDetail = 2,
        dungeon = 279201,
        camp = {
            [1] = 2,
            [2] = 3,
        },
        city = 2,
        sTime = "2019-11-30 00:00:00",
        eTime = "2019-12-02 23:59:59",
        stage = 2,
        buffDescribe = {
            [1] = 12032053,
        },
    },
    [15] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_2.png",
        },
        devilIcon = "",
        id = 15,
        invadeCamp = {
            [1] = 2,
        },
        displayDetail = 1,
        dungeon = 279105,
        camp = {
            [1] = 2,
        },
        city = 5,
        sTime = "2019-11-30 00:00:00",
        eTime = "2019-12-02 23:59:59",
        stage = 2,
        buffDescribe = {
        },
    },
    [16] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_2.png",
        },
        devilIcon = "",
        id = 16,
        invadeCamp = {
            [1] = 2,
        },
        displayDetail = 2,
        dungeon = 279202,
        camp = {
            [1] = 1,
            [2] = 3,
        },
        city = 5,
        sTime = "2019-11-30 00:00:00",
        eTime = "2019-12-02 23:59:59",
        stage = 2,
        buffDescribe = {
            [1] = 12032056,
        },
    },
    [17] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_3.png",
        },
        devilIcon = "",
        id = 17,
        invadeCamp = {
            [1] = 3,
        },
        displayDetail = 1,
        dungeon = 279108,
        camp = {
            [1] = 3,
        },
        city = 8,
        sTime = "2019-11-30 00:00:00",
        eTime = "2019-12-02 23:59:59",
        stage = 2,
        buffDescribe = {
        },
    },
    [18] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_3.png",
        },
        devilIcon = "",
        id = 18,
        invadeCamp = {
            [1] = 3,
        },
        displayDetail = 2,
        dungeon = 279203,
        camp = {
            [1] = 1,
            [2] = 2,
        },
        city = 8,
        sTime = "2019-11-30 00:00:00",
        eTime = "2019-12-02 23:59:59",
        stage = 2,
        buffDescribe = {
            [1] = 12032057,
        },
    },
    [19] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_1.png",
        },
        devilIcon = "",
        id = 19,
        invadeCamp = {
            [1] = 1,
        },
        displayDetail = 1,
        dungeon = 279103,
        camp = {
            [1] = 1,
        },
        city = 3,
        sTime = "2019-12-03 00:00:00",
        eTime = "2019-12-05 23:59:59",
        stage = 2,
        buffDescribe = {
        },
    },
    [20] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_1.png",
        },
        devilIcon = "",
        id = 20,
        invadeCamp = {
            [1] = 1,
        },
        displayDetail = 2,
        dungeon = 279204,
        camp = {
            [1] = 2,
            [2] = 3,
        },
        city = 3,
        sTime = "2019-12-03 00:00:00",
        eTime = "2019-12-05 23:59:59",
        stage = 2,
        buffDescribe = {
            [1] = 12032054,
            [2] = 12032057,
        },
    },
    [21] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_2.png",
        },
        devilIcon = "",
        id = 21,
        invadeCamp = {
            [1] = 2,
        },
        displayDetail = 1,
        dungeon = 279106,
        camp = {
            [1] = 2,
        },
        city = 6,
        sTime = "2019-12-03 00:00:00",
        eTime = "2019-12-05 23:59:59",
        stage = 2,
        buffDescribe = {
        },
    },
    [22] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_2.png",
        },
        devilIcon = "",
        id = 22,
        invadeCamp = {
            [1] = 2,
        },
        displayDetail = 2,
        dungeon = 279205,
        camp = {
            [1] = 1,
            [2] = 3,
        },
        city = 6,
        sTime = "2019-12-03 00:00:00",
        eTime = "2019-12-05 23:59:59",
        stage = 2,
        buffDescribe = {
            [1] = 12032053,
            [2] = 12032056,
        },
    },
    [23] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_3.png",
        },
        devilIcon = "",
        id = 23,
        invadeCamp = {
            [1] = 3,
        },
        displayDetail = 1,
        dungeon = 279110,
        camp = {
            [1] = 3,
        },
        city = 10,
        sTime = "2019-12-03 00:00:00",
        eTime = "2019-12-05 23:59:59",
        stage = 2,
        buffDescribe = {
        },
    },
    [24] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_3.png",
        },
        devilIcon = "",
        id = 24,
        invadeCamp = {
            [1] = 3,
        },
        displayDetail = 2,
        dungeon = 279206,
        camp = {
            [1] = 1,
            [2] = 2,
        },
        city = 10,
        sTime = "2019-12-03 00:00:00",
        eTime = "2019-12-05 23:59:59",
        stage = 2,
        buffDescribe = {
            [1] = 12032053,
            [2] = 12032055,
        },
    },
    [25] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_1.png",
        },
        devilIcon = "",
        id = 25,
        invadeCamp = {
            [1] = 1,
        },
        displayDetail = 1,
        dungeon = 279104,
        camp = {
            [1] = 1,
        },
        city = 4,
        sTime = "2019-12-06 00:00:00",
        eTime = "2019-12-08 23:59:59",
        stage = 2,
        buffDescribe = {
        },
    },
    [26] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_1.png",
        },
        devilIcon = "",
        id = 26,
        invadeCamp = {
            [1] = 1,
        },
        displayDetail = 2,
        dungeon = 279207,
        camp = {
            [1] = 2,
            [2] = 3,
        },
        city = 4,
        sTime = "2019-12-06 00:00:00",
        eTime = "2019-12-08 23:59:59",
        stage = 2,
        buffDescribe = {
            [1] = 12032053,
            [2] = 12032056,
            [3] = 12032057,
        },
    },
    [27] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_2.png",
        },
        devilIcon = "",
        id = 27,
        invadeCamp = {
            [1] = 2,
        },
        displayDetail = 1,
        dungeon = 279107,
        camp = {
            [1] = 2,
        },
        city = 7,
        sTime = "2019-12-06 00:00:00",
        eTime = "2019-12-08 23:59:59",
        stage = 2,
        buffDescribe = {
        },
    },
    [28] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_2.png",
        },
        devilIcon = "",
        id = 28,
        invadeCamp = {
            [1] = 2,
        },
        displayDetail = 2,
        dungeon = 279208,
        camp = {
            [1] = 1,
            [2] = 3,
        },
        city = 7,
        sTime = "2019-12-06 00:00:00",
        eTime = "2019-12-08 23:59:59",
        stage = 2,
        buffDescribe = {
            [1] = 12032053,
            [2] = 12032055,
            [3] = 12032057,
        },
    },
    [29] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_3.png",
        },
        devilIcon = "",
        id = 29,
        invadeCamp = {
            [1] = 3,
        },
        displayDetail = 1,
        dungeon = 279111,
        camp = {
            [1] = 3,
        },
        city = 11,
        sTime = "2019-12-06 00:00:00",
        eTime = "2019-12-08 23:59:59",
        stage = 2,
        buffDescribe = {
        },
    },
    [30] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_3.png",
        },
        devilIcon = "",
        id = 30,
        invadeCamp = {
            [1] = 3,
        },
        displayDetail = 2,
        dungeon = 279209,
        camp = {
            [1] = 1,
            [2] = 2,
        },
        city = 11,
        sTime = "2019-12-06 00:00:00",
        eTime = "2019-12-08 23:59:59",
        stage = 2,
        buffDescribe = {
            [1] = 12032053,
            [2] = 12032054,
            [3] = 12032057,
        },
    },
    [31] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_1.png",
            [2] = "ui/activity/kuangsan_fuben/main/boss_smallHead_2.png",
            [3] = "ui/activity/kuangsan_fuben/main/boss_smallHead_3.png",
        },
        devilIcon = "ui/activity/kuangsan_fuben/main/boss_smallHead_1.png",
        id = 31,
        invadeCamp = {
            [1] = 1,
            [2] = 2,
            [3] = 3,
        },
        displayDetail = 3,
        dungeon = 279301,
        camp = {
            [1] = 1,
        },
        city = 1,
        sTime = "2019-12-09 00:00:00",
        eTime = "2019-12-15 23:59:59",
        stage = 3,
        buffDescribe = {
        },
    },
    [32] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_1.png",
            [2] = "ui/activity/kuangsan_fuben/main/boss_smallHead_2.png",
            [3] = "ui/activity/kuangsan_fuben/main/boss_smallHead_3.png",
        },
        devilIcon = "ui/activity/kuangsan_fuben/main/boss_smallHead_2.png",
        id = 32,
        invadeCamp = {
            [1] = 1,
            [2] = 2,
            [3] = 3,
        },
        displayDetail = 3,
        dungeon = 279302,
        camp = {
            [1] = 2,
        },
        city = 1,
        sTime = "2019-12-09 00:00:00",
        eTime = "2019-12-15 23:59:59",
        stage = 3,
        buffDescribe = {
        },
    },
    [33] = {
        isInvade = true,
        invadeIcon = {
            [1] = "ui/activity/kuangsan_fuben/main/boss_smallHead_1.png",
            [2] = "ui/activity/kuangsan_fuben/main/boss_smallHead_2.png",
            [3] = "ui/activity/kuangsan_fuben/main/boss_smallHead_3.png",
        },
        devilIcon = "ui/activity/kuangsan_fuben/main/boss_smallHead_3.png",
        id = 33,
        invadeCamp = {
            [1] = 1,
            [2] = 2,
            [3] = 3,
        },
        displayDetail = 3,
        dungeon = 279303,
        camp = {
            [1] = 3,
        },
        city = 1,
        sTime = "2019-12-09 00:00:00",
        eTime = "2019-12-15 23:59:59",
        stage = 3,
        buffDescribe = {
        },
    },
}