return {
    [110208] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1012,
        },
        name = 30221,
        excursion = {
            [1] = -24,
            [2] = 0,
        },
        describe = 213112,
        id = 110208,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110210] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1011,
        },
        name = 30231,
        excursion = {
            [1] = 0,
            [2] = 0,
        },
        describe = 213105,
        id = 110210,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110801] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1004,
        },
        name = 30801,
        excursion = {
            [1] = -45,
            [2] = 0,
        },
        describe = 213110,
        id = 110801,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [111301] = {
        restorationCap = 50,
        fetterList = {
        },
        name = 31301,
        excursion = {
            [1] = -12,
            [2] = 0,
        },
        describe = 213115,
        id = 111301,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110501] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1000,
        },
        name = 30501,
        excursion = {
            [1] = -10,
            [2] = 0,
        },
        describe = 213107,
        id = 110501,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110102] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1013,
        },
        name = 30121,
        excursion = {
            [1] = -8,
            [2] = 0,
        },
        describe = 213103,
        id = 110102,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110602] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1006,
        },
        name = 30601,
        excursion = {
            [1] = -15,
            [2] = 0,
        },
        describe = 213116,
        id = 110602,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110201] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1005,
        },
        name = 30201,
        excursion = {
            [1] = 0,
            [2] = 0,
        },
        describe = 213101,
        id = 110201,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [112001] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1002,
        },
        name = 31201,
        excursion = {
            [1] = -3,
            [2] = 0,
        },
        describe = 213102,
        id = 112001,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110701] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1008,
        },
        name = 30701,
        excursion = {
            [1] = 0,
            [2] = 0,
        },
        describe = 213114,
        id = 110701,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110302] = {
        restorationCap = 50,
        fetterList = {
        },
        name = 30321,
        excursion = {
            [1] = 2,
            [2] = 0,
        },
        describe = 213106,
        id = 110302,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110401] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1003,
        },
        name = 30401,
        excursion = {
            [1] = -12,
            [2] = 0,
        },
        describe = 213108,
        id = 110401,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110901] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1016,
        },
        name = 30901,
        excursion = {
            [1] = -12,
            [2] = 0,
        },
        describe = 213109,
        id = 110901,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [111501] = {
        restorationCap = 50,
        fetterList = {
        },
        name = 31501,
        excursion = {
            [1] = -12,
            [2] = 0,
        },
        describe = 213215,
        id = 111501,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [111401] = {
        restorationCap = 50,
        fetterList = {
        },
        name = 31401,
        excursion = {
            [1] = -12,
            [2] = 0,
        },
        describe = 213214,
        id = 111401,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110101] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1001,
        },
        name = 30101,
        excursion = {
            [1] = -1,
            [2] = 0,
        },
        describe = 213118,
        id = 110101,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110103] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1015,
        },
        name = 30130,
        excursion = {
            [1] = -10,
            [2] = 0,
        },
        describe = 213117,
        id = 110103,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110601] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1007,
        },
        name = 30601,
        excursion = {
            [1] = -25,
            [2] = 0,
        },
        describe = 213119,
        id = 110601,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [111001] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1009,
        },
        name = 31001,
        excursion = {
            [1] = -25,
            [2] = 0,
        },
        describe = 213104,
        id = 111001,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110413] = {
        restorationCap = 50,
        fetterList = {
        },
        name = 31601,
        excursion = {
            [1] = -12,
            [2] = 0,
        },
        describe = 213108,
        id = 110413,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110209] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1014,
        },
        name = 30231,
        excursion = {
            [1] = -35,
            [2] = 0,
        },
        describe = 213113,
        id = 110209,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
    [110301] = {
        restorationCap = 50,
        fetterList = {
            [1] = 1010,
        },
        name = 30301,
        excursion = {
            [1] = -28,
            [2] = -10,
        },
        describe = 213111,
        id = 110301,
        coincidenceTime = 10,
        quantityRecovery = 1,
    },
}