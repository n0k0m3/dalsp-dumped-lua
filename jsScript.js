var function_hooked = "luaL_loadbuffer"
do{
    var chat = Module.findExportByName("libTerransForce.so", function_hooked);
} while (chat == null)
console.log(function_hooked, "at address:", chat);

Interceptor.attach(chat, {
    onEnter: function(args) {
        var name = Memory.readUtf8String(args[3]);
        var obj = {}
        obj.size = args[2].toInt32()
        obj.name = name;
        obj.content = Memory.readCString(args[1], obj.size);
        send(obj);
    }
});