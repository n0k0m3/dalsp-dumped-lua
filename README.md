## **Requirements**  
An Android Device (emulators, phones, etc.)  
Date A Live: Spirit Pledge - Global(EN) ([Google Play](https://play.google.com/store/apps/details?id=com.en.datealive.gp)/[APKPure](https://apkpure.com/date-a-live-spirit-pledge-global/com.en.datealive.gp))  
[frida-server](https://github.com/frida/frida/releases) on debugging device and [run as root](https://frida.re/docs/android/)  
Python 3+  
[frida-python](https://github.com/frida/frida-python) (`pip install frida`)  

## **Usage**  
```python dumpluaEN.py```  

Whenever a lua asset is loaded in the game memory it will be dumped using this scripts. Note: if the game didn't load the lua then the scripts will not dump it.