import frida
import sys
import os

process_name = "com.en.datealive.gp"

jsScript = open("jsScript.js","r").read()

def write(path, content):
    print('write:', path)
    folder = os.path.dirname(path)
    if not os.path.exists(folder):
        os.makedirs(folder)
    open(path, 'wb').write(content)

def on_message(message, data):
    # print 'message:',message
    name = message['payload']['name']
    content = message['payload']['content'].encode('utf-8')
    if name.endswith('.lua'):
        write(name, content)

device  = frida.get_usb_device()
pid = device.spawn([process_name])
session = device.attach(pid)
device.resume(pid)

script = session.create_script(jsScript)
script.on('message', on_message)
script.load()
sys.stdin.read()